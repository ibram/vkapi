#pragma once

namespace sonapi
{
	template<typename T>
	struct ObjectDeleter
	{
	public:
		static void FreeMemory(T **ptrToOldObj, T *ptrToNewObj)
		{
			if (ptrToNewObj != *ptrToOldObj)
			{
				delete *ptrToOldObj;
				*ptrToOldObj = ptrToNewObj;
			}
		}
	};
	
	template<typename T>
	struct ArrayDeleter
	{
		static void FreeMemory(T **ptrToOldObj, T *ptrToNewObj)
		{
			if (ptrToNewObj != *ptrToOldObj)
			{
				delete[] *ptrToOldObj;
				*ptrToOldObj = ptrToNewObj;
			}
		}
	};
}
