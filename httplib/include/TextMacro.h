#pragma once

#ifndef TEXT
	#ifdef UNICODE
		#define TEXT(str) L##str
	#else
		#define TEXT(str) #str
	#endif
#endif