#pragma once
#include <cassert>
#include <cstdio>
#include "AutoPointerTraits.h"

namespace sonapi
{
	template<typename T, typename X = ArrayDeleter<T> >
	class AutoPointer
	{
	private:
		T *m_objectPointer;
	
	public:
		explicit AutoPointer(T *objectPointer = NULL)
		{
			m_objectPointer = objectPointer;
		}
		
		~AutoPointer()
		{
			Reset();
		}
		
		T *Get() const
		{
			return m_objectPointer;
		}
		
		T &operator*() const
		{
			assert(m_objectPointer != NULL);
			
			return *m_objectPointer;
		}
		
		T *operator->() const
		{
			assert(m_objectPointer != NULL);

			return m_objectPointer;
		}
		
		AutoPointer &operator=(AutoPointer &autoPtr)
		{
			Reset(autoPtr.Release());
			return *this;
		}
		
		T* Release()
		{
			T *ptrToObj = m_objectPointer;
			m_objectPointer = NULL;
			return ptrToObj;
		}
		
		void Reset(T *objectPointer = NULL)
		{
			if (sizeof(T) > 0)
				X::FreeMemory(&m_objectPointer, objectPointer);
		}
	};
}
