﻿#pragma once
#include "BaseException.h"
#include "DataTraits.h"

namespace sonapi
{
	/**
	 * \brief Исключение, которое выбрасывается при возникновении ошибки инциализации libCURL.
	 */
	class CURLInitException: public BaseException
	{
	public:
		CURLInitException(int, const char*);
	};
}
