#pragma once

namespace sonapi
{
	template<typename T, typename X>
	class Delegate
	{
	private:
		typedef T(*FuncPtr)(X);
		FuncPtr m_function;
	
	public:
		Delegate(FuncPtr func)
		{
			m_function = func;
		}
		
		T operator()(X parameter)
		{
			return m_function(parameter);
		}
		
		bool operator==(Delegate delegate)
		{
			if (m_function == delegate.Get())
				return true;
			
			return false;
		}
		
		FuncPtr Get()
		{
			return m_function;
		}
		
		T Invoke(X parameter)
		{
			return m_function(parameter);
		}
		
		void Set(FuncPtr newFuncPtr)
		{
			m_function = newFuncPtr;
		}
	};
}