#pragma once
#include "DataTraits.h"
using namespace std;

namespace sonapi
{
	template<DataType T> class HTTPConnectionInfo
	{
	private:
		typedef typename DataTraits<T>::String String;
		
		String m_lastUsedURL;
		long m_responseCode;
		double m_downloadSpeed, m_uploadSpeed;
		
	public:
		double GetDownloadSpeed() const;
		const String &GetLastUsedURL() const;
		long GetResponseCode() const;
		double GetUploadSpeed() const;
		void SetDownloadSpeed(double downloadSpeed);
		void SetLastUsedURL(const String &lastUsedURL);
		void SetResponseCode(long responseCode);
		void SetUploadSpeed(double uploadSpeed);
	};
	
	template<DataType T> double HTTPConnectionInfo<T>::GetDownloadSpeed() const
	{
		return m_downloadSpeed;
	}
	
	template <DataType T> const typename HTTPConnectionInfo<T>::String &HTTPConnectionInfo<T>::GetLastUsedURL() const
	{
		return m_lastUsedURL;
	}
	
	template <DataType T> long HTTPConnectionInfo<T>::GetResponseCode() const
	{
		return m_responseCode;
	}
	
	template <DataType T> double HTTPConnectionInfo<T>::GetUploadSpeed() const
	{
		return m_uploadSpeed;
	}
	
	template <DataType T> void HTTPConnectionInfo<T>::SetDownloadSpeed(double downloadSpeed)
	{
		m_downloadSpeed = downloadSpeed;
	}
	
	template <DataType T> void HTTPConnectionInfo<T>::SetLastUsedURL(const String &lastUsedURL)
	{
		m_lastUsedURL = lastUsedURL;
	}
	
	template <DataType T> void HTTPConnectionInfo<T>::SetResponseCode(long responseCode)
	{
		m_responseCode = responseCode;
	}
	
	template <DataType T> void HTTPConnectionInfo<T>::SetUploadSpeed(double uploadSpeed)
	{
		m_uploadSpeed = uploadSpeed;
	}
}
