#pragma once
#include <curl/curl.h>
#include "AutoPointer.h"
#include "Converter.h"
#include "HTTPProxy.h"
#include "HTTPVersion.h"
#include "SSLVersion.h"
#include "TextMacro.h"

namespace sonapi
{
	/**
	 * \brief Настройки соединения.
	 */
	template<DataType T> class HTTPSettings
	{
	private:
		typedef typename DataTraits<T>::String String;
		
		
		String m_userAgent, m_acceptEncoding;
		AutoPointer<char> m_memoryHandler;
		HTTPVersion m_httpVersion;
		SSLVersion m_sslVersion;
		long m_maxConnects, m_maxRedirs, m_bufferSize;
		size_t m_timeout;
		CURL *m_curlHandle;
		HTTPProxy<T> m_proxy;
		curl_slist *m_headers;
		
		bool m_autoFollowing, m_autoRefering;
		bool m_contentDecoding, m_cookiesEnabled;
		bool m_useProxy, m_useSSL, m_useTransferEncoding;
		
	public:
		explicit HTTPSettings(CURL*);
		~HTTPSettings();
		
		void AddHeader(const String&);
		void ClearCookies(bool = false);
		const String &GetAcceptEncoding() const;
		long GetBufferSize() const;
		HTTPVersion GetHTTPVersion() const;
		long GetMaxConnectsCount() const;
		long GetMaxRedirectsCount() const;
		HTTPProxy<T> &GetProxy();
		SSLVersion GetSSLVersion() const;
		size_t GetTimeout() const;
		const String &GetUserAgent() const;
		bool IsAutoFollowingEnabled() const;
		bool IsAutoReferingEnabled() const;
		bool IsContentDecodingEnabled() const;
		bool IsCookiesEnabled() const;
		bool IsProxyUsed() const;
		bool IsSSLUsed() const;
		void SetAcceptEncoding(const String&);
		void SetBufferSize(long);
		void SetCurlHandle(CURL*);
		void SetHTTPVersion(HTTPVersion);
		void SetMaxConnectsCount(long);
		void SetMaxRedirectsCount(long);
		void SetSSLVersion(SSLVersion);
		void SetTimeout(size_t);
		void SetUserAgent(const String&);
		void UseAutoFollowing(bool = true);
		void UseAutoRefering(bool = true);
		void UseContentDecoding(bool = true);
		void UseCookieEngine(bool = true);
		void UseProxy(bool = false);
		void UseSSL(bool = false);
		void UseTransferEncoding(bool = true);
	};
	
	/**
	* \brief Конструктор.
	*/
	template<DataType T> HTTPSettings<T>::HTTPSettings(CURL *curlHandle):
		m_userAgent(TEXT("libCURL Wrapper")), m_proxy(curlHandle, TEXT("localhost"), 3128)
	{
		m_curlHandle = curlHandle;
		m_bufferSize = CURL_MAX_WRITE_SIZE;
		m_sslVersion = SSLVersion_Auto;
		curl_easy_setopt(m_curlHandle, CURLOPT_USERAGENT, "libCURL Wrapper");
		
		m_useSSL = false;
		curl_easy_setopt(m_curlHandle, CURLOPT_USE_SSL, false);

		m_headers = NULL;
		m_autoFollowing = true;
		curl_easy_setopt(m_curlHandle, CURLOPT_FOLLOWLOCATION, true);
		
		m_autoRefering = true;
		curl_easy_setopt(m_curlHandle, CURLOPT_AUTOREFERER, true);
		
		m_contentDecoding = true;
		curl_easy_setopt(m_curlHandle, CURLOPT_HTTP_CONTENT_DECODING, true);
		
		m_cookiesEnabled = true;
		curl_easy_setopt(m_curlHandle, CURLOPT_COOKIELIST, "");
		
		m_maxConnects = 20;
		curl_easy_setopt(m_curlHandle, CURLOPT_MAXCONNECTS, 20);
		
		m_maxRedirs = -1;
		curl_easy_setopt(m_curlHandle, CURLOPT_MAXREDIRS, -1);
		
		m_timeout = 10;
		curl_easy_setopt(m_curlHandle, CURLOPT_CONNECTTIMEOUT, 10);
		
		m_useProxy = false;
		m_proxy.Disable();
		
		m_useTransferEncoding = true;
		curl_easy_setopt(m_curlHandle, CURLOPT_TRANSFER_ENCODING, true);
	}
	
	template<DataType T> HTTPSettings<T>::~HTTPSettings()
	{
		curl_slist_free_all(m_headers);
	}
	
	/**
	 * \brief Добавляет поле НТТР заголовок в запрос.
	 * \param headerData Значение добавляемого поля.
	 */
	template<DataType T> void HTTPSettings<T>::AddHeader(const String& headerData)
	{
		m_memoryHandler.Reset(Converter<T>::StringToCharArray(headerData));
		m_headers = curl_slist_append(m_headers, m_memoryHandler.Get());
		
		curl_easy_setopt(m_curlHandle, CURLOPT_HTTPHEADER, m_headers);
	}
	
	template<DataType T> void HTTPSettings<T>::ClearCookies(bool fullClear)
	{
		if (fullClear)
			curl_easy_setopt(m_curlHandle, CURLOPT_COOKIELIST, "ALL");
		else
			curl_easy_setopt(m_curlHandle, CURLOPT_COOKIELIST, "SESSION");
	}
	
	/**
	 * \brief Получает значение поля Accept Encoding.
	 * \return Значение поля Accept Encoding.
	 */
	template<DataType T> const typename HTTPSettings<T>::String &HTTPSettings<T>::GetAcceptEncoding() const
	{
		return m_acceptEncoding;
	}
	
	template<DataType T> long HTTPSettings<T>::GetBufferSize() const
	{
		return m_bufferSize;
	}
	
	/**
	 * \brief Возвращает версию используемого протокола HTTP.
	 * \return Версия используемого протокола HTTP.
	 */
	template<DataType T> HTTPVersion HTTPSettings<T>::GetHTTPVersion() const
	{
		return m_httpVersion;
	}
	
	/**
	 * \brief Возвращает количество одновременно открытых соединений, которые будут закэшированы.
	 * \details Если это количество будет превышено, libCURL будет закрывать старые соединения.
	 * \return Количество одновременно открытых соединений, которые будут закэшированы.
	 */
	template<DataType T> long HTTPSettings<T>::GetMaxConnectsCount() const
	{
		return m_maxConnects;
	}
	
	/**
	 * \brief Возвращает количество возможных перенаправлений по ссылке.
	 * \details Если это количество будет превышено, любое последующее перенаправление приведет к ошибке.
	 * \return Количество одновременно открытых соединений, которые будут закэшированы.
	 */
	template<DataType T> long HTTPSettings<T>::GetMaxRedirectsCount() const
	{
		return m_maxRedirs;
	}
	
	/**
	 * \brief Возвращает объект прокси, связанный с текущим соединением.
	 * \return Объект прокси, связанный с текущим соединением.
	 */
	template<DataType T> HTTPProxy<T> &HTTPSettings<T>::GetProxy()
	{
		return m_proxy;
	}
	
	template<DataType T> SSLVersion HTTPSettings<T>::GetSSLVersion() const
	{
		return m_sslVersion;
	}
	
	template<DataType T> size_t HTTPSettings<T>::GetTimeout() const
	{
		return m_timeout;
	}
	
	template<DataType T> const typename HTTPSettings<T>::String &HTTPSettings<T>::GetUserAgent() const
	{
		return m_userAgent;
	}
	
	template<DataType T> bool HTTPSettings<T>::IsAutoFollowingEnabled() const
	{
		return m_autoFollowing;
	}
	
	template<DataType T> bool HTTPSettings<T>::IsAutoReferingEnabled() const
	{
		return m_autoRefering;
	}
	
	template<DataType T> bool HTTPSettings<T>::IsContentDecodingEnabled() const
	{
		return m_contentDecoding;
	}
	
	template<DataType T> bool HTTPSettings<T>::IsCookiesEnabled() const
	{
		return m_cookiesEnabled;
	}
	
	template<DataType T> bool HTTPSettings<T>::IsProxyUsed() const
	{
		return m_useProxy;
	}
	
	template<DataType T> bool HTTPSettings<T>::IsSSLUsed() const
	{
		return m_useSSL;
	}
	
	template<DataType T> void HTTPSettings<T>::SetAcceptEncoding(const String &acceptEncoding)
	{
		m_acceptEncoding = acceptEncoding;
		
		m_memoryHandler.Reset(Converter<T>::StringToCharArray(acceptEncoding));
		curl_easy_setopt(m_curlHandle, CURLOPT_ACCEPT_ENCODING, m_memoryHandler.Get());
	}
	
	template<DataType T> void HTTPSettings<T>::SetBufferSize(long bufferSize)
	{
		if (bufferSize > CURL_MAX_WRITE_SIZE)
			curl_easy_setopt(m_curlHandle, CURLOPT_BUFFERSIZE, CURL_MAX_WRITE_SIZE);
		else
			curl_easy_setopt(m_curlHandle, CURLOPT_BUFFERSIZE, bufferSize);
	}
	
	template<DataType T> void HTTPSettings<T>::SetCurlHandle(CURL* newHandle)
	{
		m_curlHandle = newHandle;
		m_proxy.SetCurlHandle(m_curlHandle);
	}
	
	template<DataType T> void HTTPSettings<T>::SetHTTPVersion(HTTPVersion httpVersion)
	{
		m_httpVersion = httpVersion;
		curl_easy_setopt(m_curlHandle, CURLOPT_HTTP_VERSION, httpVersion);
	}
	
	template<DataType T> void HTTPSettings<T>::SetMaxConnectsCount(long maxConnectsCount)
	{
		m_maxConnects = maxConnectsCount;
		curl_easy_setopt(m_curlHandle, CURLOPT_MAXCONNECTS, maxConnectsCount);
	}
	
	template<DataType T> void HTTPSettings<T>::SetMaxRedirectsCount(long maxRedirectsCount)
	{
		m_maxRedirs = maxRedirectsCount;
		curl_easy_setopt(m_curlHandle, CURLOPT_MAXREDIRS, maxRedirectsCount);
	}
	
	template<DataType T> void HTTPSettings<T>::SetSSLVersion(SSLVersion sslVersion)
	{
		m_sslVersion = sslVersion;
		curl_easy_setopt(m_curlHandle, CURLOPT_SSLVERSION, m_sslVersion);
	}
	
	template<DataType T> void HTTPSettings<T>::SetTimeout(size_t timeout)
	{
		m_timeout = timeout;
		curl_easy_setopt(m_curlHandle, CURLOPT_CONNECTTIMEOUT, timeout);
	}
	
	template<DataType T> void HTTPSettings<T>::SetUserAgent(const String &userAgent)
	{
		m_userAgent = userAgent;
		
		m_memoryHandler.Reset(Converter<T>::StringToCharArray(userAgent));
		curl_easy_setopt(m_curlHandle, CURLOPT_USERAGENT, m_memoryHandler.Get());
	}
	
	template<DataType T> void HTTPSettings<T>::UseAutoFollowing(bool use)
	{
		m_autoFollowing = use;
		curl_easy_setopt(m_curlHandle, CURLOPT_FOLLOWLOCATION, use);
	}
	
	template<DataType T> void HTTPSettings<T>::UseAutoRefering(bool use)
	{
		m_autoRefering = use;
		curl_easy_setopt(m_curlHandle, CURLOPT_AUTOREFERER, use);
	}
	
	template<DataType T> void HTTPSettings<T>::UseContentDecoding(bool use)
	{
		m_contentDecoding = use;
		curl_easy_setopt(m_curlHandle, CURLOPT_HTTP_CONTENT_DECODING, use);
	}
	
	template<DataType T> void HTTPSettings<T>::UseCookieEngine(bool enabled)
	{
		m_cookiesEnabled = enabled;
		
		if (enabled)
			curl_easy_setopt(m_curlHandle, CURLOPT_COOKIELIST, "");
		else
			curl_easy_setopt(m_curlHandle, CURLOPT_COOKIELIST, NULL);
	}
	
	template<DataType T> void HTTPSettings<T>::UseProxy(bool useProxy)
	{
		m_useProxy = useProxy;
		
		if (useProxy)
			m_proxy.Enable();
		else
			m_proxy.Disable();
	}
	
	template<DataType T> void HTTPSettings<T>::UseSSL(bool useSSL)
	{
		m_useSSL = useSSL;
		curl_easy_setopt(m_curlHandle, CURLOPT_USE_SSL, useSSL);
		curl_easy_setopt(m_curlHandle, CURLOPT_SSL_VERIFYHOST, 0L);
		curl_easy_setopt(m_curlHandle, CURLOPT_SSL_VERIFYPEER, 0L);
	}
	
	template<DataType T> void HTTPSettings<T>::UseTransferEncoding(bool use)
	{
		m_useTransferEncoding = use;
		curl_easy_setopt(m_curlHandle, CURLOPT_TRANSFER_ENCODING, use);
	}
}
