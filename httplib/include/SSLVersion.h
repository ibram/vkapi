#pragma once

namespace sonapi
{
	enum SSLVersion
	{
		SSLVersion_Auto,
		SSLVersion_TLSv1,
		SSLVersion_2,
		SSLVersion_3
	};
}