#pragma once
#include <cstdlib>
#include "DataTraits.h"

namespace sonapi
{
	template <DataType T> class Encoder;

	template<>
	class Encoder<DataType_Multibyte>
	{
		typedef typename DataTraits<DataType_Multibyte>::Char Char;
		typedef typename DataTraits<DataType_Multibyte>::String String;
		
	public:
		static void CyryllicToUtf8(Char sourceChar, char *buffer)
		{
			//TODO: реализовать
		}
	
		static const String *EscapeString(const String &str)
		{
			//TODO: реализовать
			return NULL;
		}
	};
	
	template<>
	class Encoder<DataType_Unicode>
	{
	public:
		typedef typename DataTraits<DataType_Unicode>::Char Char;
		typedef typename DataTraits<DataType_Unicode>::String String;
		
		static void CyrillicToUtf8(Char sourceChar, char *buffer)
		{
			buffer[1] = sourceChar & 0x3f;
			buffer[1] = buffer[1] | 0x80;
			buffer[1] = buffer[1] & 0xbf;

			buffer[0] = (sourceChar >> 6) | 0xe0;
			buffer[0] = buffer[0] | 0xc0; //Устанавливаем последние 2 бита 11
			buffer[0] = buffer[0] & 0xdf; //Устанавливаем 6й бит в 0
		}
		
		static const String *EscapeString(const String &unescapedStr)
		{
			String *escapedString = new String();

			for(size_t i = 0; i < unescapedStr.size(); i++)
			{
				//Для hex вида fffffD0 достаточно 9 элементов
				Char buffer[9];

				if (unescapedStr[i] > 31 && unescapedStr[i] < 48)
				{
					swprintf(buffer, sizeof(buffer) / sizeof(Char), L"%x", unescapedStr[i]);
					escapedString->append(L"%");
					*escapedString += buffer[0];
					*escapedString += buffer[1];
				}
				else if (unescapedStr[i] > 1023)
				{
					char utf8bytes[2];
					CyrillicToUtf8(unescapedStr[i], utf8bytes);

					swscanf(buffer, L"%x", utf8bytes[0]);
					escapedString->append(L"%");

					//Пишем последние два символа без предшествующих f'ок
					*escapedString += buffer[6];
					*escapedString += buffer[7];

					swscanf(buffer, L"%x", utf8bytes[1]);
					escapedString->append(L"%");
					*escapedString += buffer[6];
					*escapedString += buffer[7];
				}
			}

			return escapedString;
		}
	};
}
