﻿#pragma once
#include "BaseException.h"

namespace sonapi
{
	class ConnectionFailException: public BaseException
	{
	public:
		ConnectionFailException(int, const char*);
	};
}