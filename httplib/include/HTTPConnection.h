#pragma once
#include <vector>
#include "HTTPConnectionInfo.h"
#include "HTTPSettings.h"
#include "ConnectionFailException.h"
#include "CURLInitException.h"
#include "DataTraits.h"

namespace sonapi
{
	template<DataType T> class HTTPConnection
	{
	private:
		typedef typename DataTraits<T>::IStream IStream;
		typedef typename DataTraits<T>::OStream OStream;
		typedef typename DataTraits<T>::Char Char;
		typedef typename DataTraits<T>::String String;
		
		CURL *m_curlHandle;
		HTTPSettings<T> m_settings;
		HTTPConnectionInfo<T> m_connectionInfo;
		static vector<String> m_responseHeaders;
		AutoPointer<char> m_memoryHandler;
		char m_errorMessage[CURL_ERROR_SIZE];
		curl_slist *m_headers;
		IStream *m_requestStream;
		OStream *m_responseStream;
		static streampos m_requestStreamLength;
		size_t m_responseCode;
		
		static size_t HeaderCallback(char*, size_t, size_t, void*);
		static size_t ReaderCallback(void*, size_t, size_t, IStream**);
		static size_t SeekerCallback(IStream**, curl_off_t, int);
		void SetupCallbacks();
		static size_t WriterCallback(char*, size_t, size_t, OStream**);

	public:
		HTTPConnection();
		~HTTPConnection();
		
		const String *EscapeString(const String&);
		void Get(const String&);
		const HTTPConnectionInfo<T> &GetConnectionInfo();
		IStream *GetRequestStream() const;
		const vector<String> &GetResponseHeaders() const;
		OStream *GetResponseStream() const;
		HTTPSettings<T> &GetSettings();
		void Post(const String&);
		void SetRequestStream(IStream*);
		void SetResponseStream(OStream*);
	};

	template<DataType T> streampos HTTPConnection<T>::m_requestStreamLength = 0;
	template<DataType T> vector<typename HTTPConnection<T>::String> HTTPConnection<T>::m_responseHeaders;

	template<DataType T> HTTPConnection<T>::HTTPConnection():
		m_settings(NULL)
	{
		m_curlHandle = curl_easy_init();

		if (m_curlHandle == NULL)
		{
			CURLInitException curlInitExc(1, "CURL wasn't initialized.");
			throw curlInitExc;
		}

		m_settings.SetCurlHandle(m_curlHandle);
		m_headers = NULL;
		m_requestStream = NULL;
		m_responseStream = NULL;
		curl_easy_setopt(m_curlHandle, CURLOPT_ERRORBUFFER, m_errorMessage);
		SetupCallbacks();
	}
	
	template<DataType T> HTTPConnection<T>::~HTTPConnection()
	{
		curl_easy_cleanup(m_curlHandle);
	}
	
	template<DataType T> const typename HTTPConnection<T>::String *HTTPConnection<T>::EscapeString(const String &url)
	{
		//TODO: реализовать конвертацию
		return NULL;
	}
	
	template<DataType T> void HTTPConnection<T>::Get(const String &url)
	{
		HTTPConnection<T>::m_responseHeaders.clear();
		curl_easy_setopt(m_curlHandle, CURLOPT_HTTPGET, true);
		
		m_memoryHandler.Reset(Converter<T>::StringToCharArray(url));
		curl_easy_setopt(m_curlHandle, CURLOPT_URL, m_memoryHandler.Get());
		
		m_responseCode = curl_easy_perform(m_curlHandle);

		if (m_responseCode != 0)
		{
			ConnectionFailException connectionFailExc(m_responseCode, m_errorMessage);
			throw connectionFailExc;
		}
	}
	
	template<DataType T> const HTTPConnectionInfo<T> &HTTPConnection<T>::GetConnectionInfo()
	{
		double downloadSpeed = 0.0;
		double uploadSpeed = 0.0;
		long responseCode = 0;
		char *url = NULL;
		
		curl_easy_getinfo(m_curlHandle, CURLINFO_EFFECTIVE_URL, &url);
		curl_easy_getinfo(m_curlHandle, CURLINFO_SPEED_DOWNLOAD, &downloadSpeed);
		curl_easy_getinfo(m_curlHandle, CURLINFO_RESPONSE_CODE, &responseCode);
		curl_easy_getinfo(m_curlHandle, CURLINFO_SPEED_UPLOAD, &uploadSpeed);
		
		m_connectionInfo.SetLastUsedURL(*(Converter<T>::CharArrayToString(url)));
		m_connectionInfo.SetDownloadSpeed(downloadSpeed);
		m_connectionInfo.SetUploadSpeed(uploadSpeed);
		m_connectionInfo.SetResponseCode(responseCode);
		
		return m_connectionInfo;
	}
	
	template<DataType T> typename HTTPConnection<T>::IStream *HTTPConnection<T>::GetRequestStream() const
	{
		return m_requestStream;
	}
	
	template<DataType T> const vector<typename HTTPConnection<T>::String> &HTTPConnection<T>::GetResponseHeaders() const
	{
		return m_responseHeaders;
	}
	
	template<DataType T> typename HTTPConnection<T>::OStream *HTTPConnection<T>::GetResponseStream() const
	{
		return m_responseStream;
	}
	
	template<DataType T> HTTPSettings<T> &HTTPConnection<T>::GetSettings()
	{
		return m_settings;
	}
	
	template<DataType T> size_t HTTPConnection<T>::HeaderCallback(char* headerData, size_t blocksCount, size_t blockSize, void *userData)
	{
		size_t dataSize = blocksCount * blockSize;
		const Char *data = DataTraits<T>::GetHeaderData(headerData, dataSize);
		
		if (data != NULL)
			HTTPConnection<T>::m_responseHeaders.push_back(data);
		
		return dataSize;
	}
	
	template<DataType T> void HTTPConnection<T>::Post(const String &url)
	{
		HTTPConnection<T>::m_responseHeaders.clear();
		m_memoryHandler.Reset(Converter<T>::StringToCharArray(url));
		curl_easy_setopt(m_curlHandle, CURLOPT_URL, m_memoryHandler.Get());
		
		streampos currentPosition = m_requestStream->tellg();
		HTTPConnection<T>::m_requestStreamLength = (m_requestStream->seekg(0, IStream::end).tellg() - currentPosition);
		m_requestStream->seekg(currentPosition);
		
		curl_easy_setopt(m_curlHandle, CURLOPT_POST, true);
		curl_easy_setopt(m_curlHandle, CURLOPT_POSTFIELDSIZE, HTTPConnection<T>::m_requestStreamLength);
		m_responseCode = curl_easy_perform(m_curlHandle);
		
		if (m_responseCode != 0)
		{
			ConnectionFailException connectionFailExc(m_responseCode, m_errorMessage);
			throw connectionFailExc;
		}
	}
		
	template<DataType T> size_t HTTPConnection<T>::ReaderCallback(void *requestData, size_t blocksCount, size_t blockSize, IStream **userData)
	{
		if (*userData != NULL)
		{
			streampos currentPosition = (*userData)->tellg();
			streampos available = HTTPConnection<T>::m_requestStreamLength - currentPosition;
			(*userData)->seekg(currentPosition);
			
			if (available > 0)
			{
				streamsize dataToReadSize = min((blocksCount * blockSize), (size_t)available);
				DataTraits<T>::ReadRequestData((*userData), requestData, dataToReadSize);
				
				return dataToReadSize;
			}
		}
		
		return 0;
	}
	
	template<DataType T> size_t HTTPConnection<T>::SeekerCallback(IStream **inputStream, curl_off_t offset, int origin)
	{
		if (*inputStream == NULL)
			return 0;
		
		if (offset > HTTPConnection<T>::m_requestStreamLength)
			return CURL_SEEKFUNC_FAIL;

		if (origin == SEEK_SET || origin == SEEK_CUR)
		{
			(*inputStream)->seekg(offset);
			
			return CURL_SEEKFUNC_OK;
		}
		
		if (origin == SEEK_END)
		{
			(*inputStream)->seekg(m_requestStreamLength);
			
			return CURL_SEEKFUNC_OK;
		}

		return CURL_SEEKFUNC_FAIL;
	}
	
	template<DataType T> void HTTPConnection<T>::SetRequestStream(IStream *requestStream)
	{
		m_requestStream = requestStream;
		HTTPConnection<T>::m_requestStreamLength = m_requestStream->seekg(0, IStream::end).tellg();
		m_requestStream->seekg(0, IStream::beg);
	}
	
	template<DataType T> void HTTPConnection<T>::SetResponseStream(OStream *responseStream)
	{
		m_responseStream = responseStream;
	}

	template<DataType T> void HTTPConnection<T>::SetupCallbacks()
	{
		curl_easy_setopt(m_curlHandle, CURLOPT_HEADERFUNCTION, HeaderCallback);
		curl_easy_setopt(m_curlHandle, CURLOPT_READDATA, &m_requestStream);
		curl_easy_setopt(m_curlHandle, CURLOPT_READFUNCTION, ReaderCallback);
		curl_easy_setopt(m_curlHandle, CURLOPT_SEEKDATA, &m_requestStream);
		curl_easy_setopt(m_curlHandle, CURLOPT_SEEKFUNCTION, SeekerCallback);
		curl_easy_setopt(m_curlHandle, CURLOPT_WRITEDATA, &m_responseStream);
		curl_easy_setopt(m_curlHandle, CURLOPT_WRITEFUNCTION, WriterCallback);
	}
	
	template<DataType T> size_t HTTPConnection<T>::WriterCallback(char *responseData, size_t blocksCount, size_t blockSize, OStream **userData)
	{
		if (userData != NULL)
		{
			streamsize responseSize = blockSize * blocksCount;
			DataTraits<T>::WriteResponseData((*userData), responseData, responseSize);
			
			return responseSize;
		}
		
		return 0;
	}
}
