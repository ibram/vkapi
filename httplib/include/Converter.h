#pragma once
#include <bitset>
#include <stdlib.h>
#include <string>
#include <string.h>
#include "AutoPointer.h"
#include "AutoPointerTraits.h"
#include "DataTraits.h"
using namespace std;

namespace sonapi
{
	template<DataType T> class Converter;

	template<>
	class Converter<DataType_Multibyte>
	{
		typedef typename DataTraits<DataType_Multibyte>::Char Char;
		typedef typename DataTraits<DataType_Multibyte>::String String;
		
	private:
		static AutoPointer<wchar_t> m_memoryHandler;
		
		static size_t GetUtf8CharactersCount(const char *utf8String, size_t utf8StringSize)
		{
			if (utf8String == NULL || utf8StringSize == 0)
				return 0;

			size_t charactersCount = 0;
			int byteIndex = 0;

			while(utf8StringSize > 0)
			{
				if (utf8String[byteIndex] > -1 && utf8String[byteIndex] < 128)
				{
					charactersCount++;
					utf8StringSize--;
					byteIndex++; //Переходим к следующему символу
				}
				else
				{
					//Определяем количество байт на символ по установленным первым битам в первом байте
					size_t bytesPerCharacter = 0;
					bitset<8> firstCharBits(utf8String[byteIndex]);

					for (int i = 7; i > 1; i--)
					{
						if (firstCharBits.test(i))
							bytesPerCharacter++;
						else break;
					}

					//На Windows для utf8 символа > 3 байт требуется 2 wchar_t
					if (sizeof(wchar_t) == 2 && bytesPerCharacter > 3)
						charactersCount += 2;
					else
						charactersCount++;

					utf8StringSize -= bytesPerCharacter;
					byteIndex += bytesPerCharacter; //Переходим к следующему символу
				}
			}

			return charactersCount;
		}

	public:
		static time_t CharArrayToUnixTime(const Char *buffer)
		{
			if (buffer == NULL)
				return 0;

			time_t unixTime = 0;

			if (sizeof(time_t) == sizeof(int))
				unixTime = atoi(buffer);
			else if (sizeof(time_t) == sizeof(long))
				unixTime = atol(buffer);
			else if (sizeof(time_t) == sizeof(long long))
				unixTime = atoll(buffer);

			return unixTime;
		}

		static const String *CharArrayToString(const char *str)
		{
			if (str == NULL)
			{
				String *convertedStr = new String();
				return convertedStr;
			}

			return new String(str);
		}

		static wchar_t *CharArrayToWCharArray(const char*, size_t);

		static char *StringToCharArray(const String &str)
		{
			if (str.empty())
				return '\0';

			size_t dataSize = str.size() + 1;
			char *resultString = new char[dataSize];
			strncpy(resultString, str.c_str(), dataSize);

			return resultString;
		}

		static wchar_t *StringToWideCharArray(const String &str)
		{
			if (str.empty())
				return L'\0';

			size_t dataSize = str.size();
			wchar_t *convertedString = new wchar_t[dataSize + 1];
			mbstowcs(convertedString, str.c_str(), dataSize);
			convertedString[dataSize] = L'\0';

			return convertedString;
		}

		static const Char* UnixTimeToCharArray(time_t unixTime)
		{
			//Определим размер нужного буфера
			int bufSize = 1; //Учитываем место для NULL терминатора
			time_t tempTime = unixTime;

			do
			{
				bufSize++;
				tempTime /= 10;
			}
			while (tempTime > 0);

			Char *buffer = new Char[bufSize];

			if (sizeof(time_t) == sizeof(int))
				snprintf(buffer, bufSize, "%i", static_cast<int>(unixTime));
			else if (sizeof(time_t) == sizeof(long))
				snprintf(buffer, bufSize, "%li", static_cast<long>(unixTime));
			else if (sizeof(time_t) == sizeof(long long))
				snprintf(buffer, bufSize, "%lli", static_cast<long long>(unixTime));

			return buffer;
		}

		static const Char *Utf8ToCharArray(const char *utf8String, size_t utf8StringSize)
		{
			if (utf8String == NULL || utf8StringSize == 0)
				return '\0';

			int byteIndex = 0;

			size_t wideCharStringSize = GetUtf8CharactersCount(utf8String, utf8StringSize);
			m_memoryHandler.Reset(new wchar_t[wideCharStringSize + 1]);
			wchar_t *wideCharString = m_memoryHandler.Get();

			for (size_t i = 0; i < wideCharStringSize; i++)
			{
				if (utf8String[byteIndex] > -1 && utf8String[byteIndex] < 128)
				{
					wideCharString[i] = utf8String[byteIndex];
					byteIndex++; //К следующему символу
				}
				else
				{
					bitset<8> firstCharBits(utf8String[byteIndex]);
					bitset<8> secondCharBits(utf8String[byteIndex + 1]);
					bitset<16> wideCharBits(0);

					int bytesPerCharacter = 0;

					//Определяем количество байт на символ по установленным первым битам в первом байте
					for (int k = 7; k > 1; k--)
					{
						if (firstCharBits.test(k))
							bytesPerCharacter++;
						else break;
					}

					//Переносим нужные биты с первого байта в результат
					int bitOffset = 6 * (bytesPerCharacter - 1);

					for(int k = 0; k < 7 - bytesPerCharacter; k++)
						wideCharBits[k + bitOffset] = firstCharBits[k];

					//Переносим нужные биты со второго байта в результат
					for(int k = 0; k < 6; k++)
						wideCharBits[k] = secondCharBits[k];

					wideCharString[i] = wideCharBits.to_ulong();
					byteIndex += bytesPerCharacter; //К следующему символу
				}
			}

			wideCharString[wideCharStringSize] = 0;

			char *multibyteString = new char[wideCharStringSize + 1];
			wcstombs(multibyteString, wideCharString, wideCharStringSize);
			multibyteString[wideCharStringSize] = 0;

			return multibyteString;
		}

		static char *WCharArrayToCharArray(const wchar_t *wcharArray, size_t arraySize)
		{
			if (wcharArray == NULL)
				return NULL;

			char *convertedString = new char[arraySize];
			wcstombs(convertedString, wcharArray, arraySize);

			return convertedString;
		}
	};
	
	template<>
	class Converter<DataType_Unicode>
	{
		typedef typename DataTraits<DataType_Unicode>::Char Char;
		typedef typename DataTraits<DataType_Unicode>::String String;

	private:
		static AutoPointer<wchar_t> m_memoryHandler;

		static size_t GetUtf8CharactersCount(const char *utf8String, size_t utf8StringSize)
		{
			if (utf8String == NULL || utf8StringSize == 0)
				return 0;

			size_t charactersCount = 0;
			int byteIndex = 0;

			while(utf8StringSize > 0)
			{
				if (utf8String[byteIndex] > -1 && utf8String[byteIndex] < 128)
				{
					charactersCount++;
					utf8StringSize--;
					byteIndex++; //Переходим к следующему символу
				}
				else
				{
					//Определяем количество байт на символ по установленным первым битам в первом байте
					size_t bytesPerCharacter = 0;
					bitset<8> firstCharBits(utf8String[byteIndex]);

					for (int i = 7; i > 1; i--)
					{
						if (firstCharBits.test(i))
							bytesPerCharacter++;
						else break;
					}

					//На Windows для utf8 символа > 3 байт требуется 2 wchar_t
					if (sizeof(wchar_t) == 2 && bytesPerCharacter > 3)
						charactersCount += 2;
					else
						charactersCount++;

					utf8StringSize -= bytesPerCharacter;
					byteIndex += bytesPerCharacter; //Переходим к следующему символу
				}
			}

			return charactersCount;
		}

	public:
		static time_t CharArrayToUnixTime(const Char *buffer)
		{
			if (buffer == NULL)
				return 0;

			time_t unixTime = 0;

			if (sizeof(time_t) == sizeof(int))
				swscanf(buffer, L"%i", &unixTime);
			else if (sizeof(time_t) == sizeof(long))
				unixTime = wcstol(buffer, NULL, 10);
			else if (sizeof(time_t) == sizeof(long long))
				unixTime = wcstoll(buffer, NULL, 10);

			return unixTime;
		}

		static const String *CharArrayToString(const char *str)
		{
			if (str == NULL)
			{
				String *convertedStr = new String();
				return convertedStr;
			}

			size_t cStringSize = strlen(str);
			m_memoryHandler.Reset(new wchar_t[cStringSize + 1]);

			mbstowcs(m_memoryHandler.Get(), str, cStringSize);
			String *convertedStr = new String(m_memoryHandler.Get());

			return convertedStr;
		}

		static wchar_t *CharArrayToWCharArray(const char*, size_t);

		static char *StringToCharArray(const String &str)
		{
			if (str.empty())
				return '\0';

			size_t dataSize = str.size();
			char *resultString = new char[dataSize + 1];
			wcstombs(resultString, str.c_str(), dataSize);
			resultString[dataSize] = '\0';
			return resultString;
		}

		static wchar_t *StringToWideCharArray(const String &str)
		{
			if (str.empty())
				return L'\0';

			size_t dataSize = str.size() + 1;
			wchar_t *resultString = new wchar_t[dataSize];
			wcsncpy(resultString, str.c_str(), dataSize);

			return resultString;
		}

		static const Char* UnixTimeToCharArray(time_t unixTime)
		{
			//Определим размер нужного буфера
			int bufSize = 1; //Выделили место для NULL терминатора
			time_t tempTime = unixTime;

			do
			{
				bufSize++;
				tempTime /= 10;
			}
			while (tempTime > 0);

			Char *buffer = new Char[bufSize];

			if (sizeof(time_t) == sizeof(int))
				swprintf(buffer, bufSize, L"%i", static_cast<int>(unixTime));
			else if (sizeof(time_t) == sizeof(long))
				swprintf(buffer, bufSize, L"%li", static_cast<long>(unixTime));
			else if (sizeof(time_t) == sizeof(long long))
				swprintf(buffer, bufSize, L"%lli", static_cast<long long>(unixTime));

			return buffer;
		}

		static const Char *Utf8ToCharArray(const char *utf8String, size_t utf8StringSize)
		{
			if (utf8String == NULL || utf8StringSize == 0)
				return '\0';

			int byteIndex = 0;

			size_t wideCharStringSize = GetUtf8CharactersCount(utf8String, utf8StringSize);
			wchar_t *wideCharString = new wchar_t[wideCharStringSize + 1];

			for (size_t i = 0; i < wideCharStringSize; i++)
			{
				if (utf8String[byteIndex] > -1 && utf8String[byteIndex] < 128)
				{
					wideCharString[i] = utf8String[byteIndex];
					byteIndex++; //К следующему символу
				}
				else
				{
					bitset<8> firstCharBits(utf8String[byteIndex]);
					bitset<32> wideCharBits(0);

					int bytesPerCharacter = 0;

					//Определяем количество байт на символ по установленным первым битам в первом байте
					for (int j = 7; j > 1; j--)
					{
						if (firstCharBits.test(j))
							bytesPerCharacter++;
						else break;
					}

					//Переносим нужные биты с первого байта в результат
					int bitOffset = 6 * (bytesPerCharacter - 1);

					for(int j = 0; j < 7 - bytesPerCharacter; j++)
						wideCharBits[j + bitOffset] = firstCharBits[j];

					//Переносим нужные биты с остальных байтов в результат
					for (int j = 0; j < bytesPerCharacter - 1; j++)
					{
						bitset<8> byteBits(utf8String[byteIndex + 1 + j]);
						int offset = 6 * (bytesPerCharacter - 2);

						for (int k = 0; k < 6; k++)
							wideCharBits[k + offset] = byteBits[k];
					}

					if (sizeof(wchar_t) == 2 && bytesPerCharacter > 3)
					{
						bitset<16> firstByte, secondByte;

						for (int k = 0; k < 16; k++)
						{
							firstByte[k] = wideCharString[k + 15];
							secondByte[k] = wideCharBits[k];
						}

						wideCharString[i] = firstByte.to_ulong();
						wideCharString[i + 1] = secondByte.to_ulong();
						i++;
					}
					else
						wideCharString[i] = wideCharBits.to_ulong();

					byteIndex += bytesPerCharacter; //К следующему символу
				}
			}

			wideCharString[wideCharStringSize] = 0;

			return wideCharString;
		}

		static char *WCharArrayToCharArray(const wchar_t *wcharArray, size_t arraySize)
		{
			if (wcharArray == NULL)
				return NULL;

			char *convertedString = new char[arraySize];
			wcstombs(convertedString, wcharArray, arraySize);

			return convertedString;
		}
	};
}
