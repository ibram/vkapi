#pragma once
#include "BaseException.h"

namespace sonapi
{
	class NullPtrException: public BaseException
	{
	public:
		NullPtrException(int, const char*);
	};
}
