#pragma once
#include <string>
#include "DataTraits.h"
#include "ProxyType.h"
using namespace std;

namespace sonapi
{
	/**
	 * \brief Прокси сервер.
	 */
	template<DataType T> class HTTPProxy
	{
	private:
		typedef typename DataTraits<T>::String String;
		
		String m_address, m_userName, m_userPassword;
		AutoPointer<char> m_memoryHandler;
		long m_port;
		CURL *m_curlHandle;
		ProxyType m_type;
		bool m_useAuthorization;
		
	public:
		HTTPProxy(CURL*, const String&, long);
		void Disable();
		void Enable();
		const String &GetAddress() const;
		long GetPort() const;
		ProxyType GetType() const;
		const String &GetUsername() const;
		const String &GetUserPassword() const;
		bool IsAuthorizationUsed() const;
		void SetAddress(const String&, long);
		void SetCredentials(const String&, const String&);
		void SetCurlHandle(CURL*);
		void SetType(ProxyType);
		void UseAuthorization(bool);
	};
	
	template<DataType T> HTTPProxy<T>::HTTPProxy(CURL* curlHandle, const String &address, long port):
		m_address(address), m_memoryHandler(NULL), m_port(port), m_curlHandle(curlHandle)
	{
		m_type = ProxyType_HTTP;
		m_useAuthorization = false;
	}
	
	template<DataType T> void HTTPProxy<T>::Disable()
	{
		curl_easy_setopt(m_curlHandle, CURLOPT_PROXY, "");
	}
	
	template<DataType T> void HTTPProxy<T>::Enable()
	{
		SetAddress(m_address, m_port);
	}
	
	template<DataType T> const typename HTTPProxy<T>::String &HTTPProxy<T>::GetAddress() const
	{
		return m_address;
	}
	
	template<DataType T> long HTTPProxy<T>::GetPort() const
	{
		return m_port;
	}
	
	template<DataType T> ProxyType HTTPProxy<T>::GetType() const
	{
		return m_type;
	}
	
	template<DataType T> const typename HTTPProxy<T>::String &HTTPProxy<T>::GetUsername() const
	{
		return m_userName;
	}

	template<DataType T> const typename HTTPProxy<T>::String &HTTPProxy<T>::GetUserPassword() const
	{
		return m_userPassword;
	}

	template<DataType T> bool HTTPProxy<T>::IsAuthorizationUsed() const
	{
		return m_useAuthorization;
	}

	template<DataType T> void HTTPProxy<T>::SetAddress(const String &address, long port)
	{
		m_address = address;
		m_port = port;
		
		m_memoryHandler.Reset(Converter<T>::StringToCharArray(address));
		curl_easy_setopt(m_curlHandle, CURLOPT_PROXY, m_memoryHandler.Get());
		curl_easy_setopt(m_curlHandle, CURLOPT_PROXYPORT, port);
	}
	
	template<DataType T> void HTTPProxy<T>::SetCredentials(const String &userName, const String& userPassword)
	{
		m_userName = userName;
		m_userPassword = userPassword;
		
		m_memoryHandler.Reset(Converter<T>::StringToCharArray(userName));
		curl_easy_setopt(m_curlHandle, CURLOPT_PROXYUSERNAME, m_memoryHandler.Get());
				
		m_memoryHandler.Reset(Converter<T>::StringToCharArray(userPassword));
		curl_easy_setopt(m_curlHandle, CURLOPT_PROXYPASSWORD, m_memoryHandler.Get());
	}
	
	template<DataType T> void HTTPProxy<T>::SetCurlHandle(CURL *newHandle)
	{
		m_curlHandle = newHandle;
	}
	
	template<DataType T> void HTTPProxy<T>::SetType(ProxyType proxyType)
	{
		m_type = proxyType;
	}

	template<DataType T> void HTTPProxy<T>::UseAuthorization(bool use)
	{
		m_useAuthorization = use;
		
		if (use)
		{
			m_memoryHandler.Reset(Converter<T>::StringToCharArray(m_userName));
			curl_easy_setopt(m_curlHandle, CURLOPT_PROXYUSERNAME, m_memoryHandler.Get());
				
			m_memoryHandler.Reset(Converter<T>::StringToCharArray(m_userPassword));
			curl_easy_setopt(m_curlHandle, CURLOPT_PROXYPASSWORD, m_memoryHandler.Get());
		}
		else
		{
			curl_easy_setopt(m_curlHandle, CURLOPT_PROXYUSERNAME, "");
			curl_easy_setopt(m_curlHandle, CURLOPT_PROXYPASSWORD, "");
		}
	}
}
