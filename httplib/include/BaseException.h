﻿#pragma once
#include "AutoPointer.h"
#include <cstring>

namespace sonapi
{
	class BaseException
	{
	private:
		int m_errorCode;
		AutoPointer<const char> m_memoryHandler;
		
	public:
		BaseException(int, const char*);
		BaseException(BaseException&);
		BaseException& operator=(BaseException&);
		virtual ~BaseException() throw();
		
		int code() const;
		const char *what() const throw();
	};
}
