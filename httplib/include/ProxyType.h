﻿#pragma once

namespace sonapi
{
	enum ProxyType
	{
	    ProxyType_HTTP,
	    ProxyType_HTTP_1_0,
	    ProxyType_SOCKS4 = 4,
	    ProxyType_SOCKS5,
	    ProxyType_SOCKS4A,
	    ProxyType_SOCKS5_HOSTNAME
	};
}