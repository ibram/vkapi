#pragma once
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <string>
#include "AutoPointer.h"
#include "TextMacro.h"
using namespace std;

namespace sonapi
{
	enum DataType
	{
	    DataType_Multibyte,
	    DataType_Unicode
	};

	template<DataType T> class DataTraits;

	template<> class DataTraits<DataType_Multibyte>
	{
	public:
		typedef char Char;
		typedef std::string String;
		typedef std::istream IStream;
		typedef std::ostream OStream;
		typedef std::istringstream IStringStream;
		typedef std::ostringstream OStringStream;
		typedef std::ifstream IFileStream;
		typedef std::ofstream OFileStream;

		static float CharArrayToFloat(const Char *buffer)
		{
			if (buffer == NULL)
				return 0.0;

			return atof(buffer);
		}

		static int CharArrayToInt(const Char *buffer)
		{
			if (buffer == NULL)
				return 0;

			return atoi(buffer);
		}

		static int CompareStrings(const Char *str1, const Char *str2)
		{
			return strcmp(str1, str2);
		}

		static const Char *GetHeaderData(char *headerData, size_t headerDataSize)
		{
			if (headerData != NULL)
			{
				m_charArrayHandler.Reset(new char[headerDataSize + 1]);
				memcpy(m_charArrayHandler.Get(), headerData, headerDataSize);
				m_charArrayHandler.Get()[headerDataSize] = '\0';

				return headerData;
			}

			return NULL;
		}

		static const Char *IntToCharArray(int numberToConvert)
		{
			//Определим размер нужного буфера
			int bufSize = 1; //Учитываем место для NULL терминатора
			int tempNumber = numberToConvert;

			do
			{
				bufSize++;
				tempNumber /= 10;
			}
			while (tempNumber > 0);

			m_memoryHandler.Reset(new Char[bufSize]);
			snprintf(m_memoryHandler.Get(), bufSize, "%i", numberToConvert);
			return m_memoryHandler.Get();
		}

		static void ReadRequestData(IStream *inputStream, void *requestData, size_t requestSize)
		{
			inputStream->read((char*)requestData, requestSize);
		}

		static void WriteResponseData(OStream *outputStream, char *responseData, size_t responseSize)
		{
			try
			{
				outputStream->write(responseData, responseSize);
			}
			catch(OStream::failure&)
			{
				throw;
			}
		}

	private:
		static AutoPointer<char> m_charArrayHandler;
		static AutoPointer<wchar_t> m_wCharArrayHandler;
		static AutoPointer<Char> m_memoryHandler;
	};
	
	template<> class DataTraits<DataType_Unicode>
	{
	public:
		typedef wchar_t Char;
		typedef std::wstring String;
		typedef std::wistream IStream;
		typedef std::wostream OStream;
		typedef std::wistringstream IStringStream;
		typedef std::wostringstream OStringStream;
		typedef std::wifstream IFileStream;
		typedef std::wofstream OFileStream;
		
		static float CharArrayToFloat(const Char *buffer)
		{
			if (buffer == NULL)
				return 0.0;

			float convertedValue = 0.0;
			swscanf(buffer, TEXT("%f"), &convertedValue);
			return convertedValue;
		}
		
		static int CharArrayToInt(const Char *buffer)
		{
			if (buffer == NULL)
				return 0;

			int convertedValue = 0;
			swscanf(buffer, L"%i", &convertedValue);
			return convertedValue;
		}
		
		static int CompareStrings(const Char *str1, const Char *str2)
		{
			return wcscmp(str1, str2);
		}
		
		static const Char *GetHeaderData(char *headerData, size_t headerDataSize)
		{
			if (headerData != NULL)
			{
				m_wCharArrayHandler.Reset(new wchar_t[headerDataSize + 1]);
				mbstowcs(m_wCharArrayHandler.Get(), headerData, headerDataSize);
				m_wCharArrayHandler.Get()[headerDataSize] = L'\0';

				return m_wCharArrayHandler.Get();
			}

			return NULL;
		}
		
		static const Char *IntToCharArray(int numberToConvert)
		{
			//Определим размер нужного буфера
			int bufSize = 1; //Учитываем место для NULL терминатора
			int tempNumber = numberToConvert;

			do
			{
				bufSize++;
				tempNumber /= 10;
			}
			while (tempNumber > 0);

			m_memoryHandler.Reset(new Char[bufSize]);
			swprintf(m_memoryHandler.Get(), bufSize, L"%i", numberToConvert);
			return m_memoryHandler.Get();
		}
		
		static void ReadRequestData(IStream *inputStream, void *requestData, size_t requestSize)
		{
			m_wCharArrayHandler.Reset(new wchar_t[requestSize]);
			m_charArrayHandler.Reset(new char[requestSize]);

			inputStream->read(m_wCharArrayHandler.Get(), requestSize);
			wcstombs(m_charArrayHandler.Get(), m_wCharArrayHandler.Get(), requestSize);
			memcpy(requestData, m_charArrayHandler.Get(), requestSize);
		}
		
		static void WriteResponseData(OStream *outputStream, char *responseData, size_t responseSize)
		{
			m_wCharArrayHandler.Reset(new wchar_t[responseSize]);
			mbstowcs(m_wCharArrayHandler.Get(), responseData, responseSize);

			try
			{
				outputStream->write(m_wCharArrayHandler.Get(), responseSize);
			}
			catch(OStream::failure&)
			{
				throw;
			}
		}

	private:
		static AutoPointer<char> m_charArrayHandler;
		static AutoPointer<wchar_t> m_wCharArrayHandler;
		static AutoPointer<Char> m_memoryHandler;
	};
}
