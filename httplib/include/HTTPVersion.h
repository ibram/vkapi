#pragma once

namespace sonapi
{
	enum HTTPVersion
	{
		HTTPVersion_Auto,
		HTTPVersion_1_0,
		HTTPVersion_1_1
	};
}