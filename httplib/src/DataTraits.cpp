#include "DataTraits.h"
using namespace sonapi;

AutoPointer<char> DataTraits<DataType_Multibyte>::m_charArrayHandler(NULL);
AutoPointer<wchar_t> DataTraits<DataType_Multibyte>::m_wCharArrayHandler(NULL);
AutoPointer<DataTraits<DataType_Multibyte>::Char> DataTraits<DataType_Multibyte>::m_memoryHandler(NULL);

AutoPointer<char> DataTraits<DataType_Unicode>::m_charArrayHandler(NULL);
AutoPointer<wchar_t> DataTraits<DataType_Unicode>::m_wCharArrayHandler(NULL);
AutoPointer<DataTraits<DataType_Unicode>::Char> DataTraits<DataType_Unicode>::m_memoryHandler(NULL);
