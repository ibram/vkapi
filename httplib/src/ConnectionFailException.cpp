#include "ConnectionFailException.h"
using namespace sonapi;

ConnectionFailException::ConnectionFailException(int errorCode, const char *errorMessage):
	BaseException(errorCode, errorMessage)
{

}
