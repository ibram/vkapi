#include "BaseException.h"
using namespace sonapi;

BaseException::BaseException(int errorCode, const char *errorMessage):
	m_errorCode(errorCode)
{
	if (errorMessage != NULL)
	{
		char *copiedErrorMessage = new char[strlen(errorMessage) + 1];
		strncpy(copiedErrorMessage, errorMessage, strlen(errorMessage) + 1);
		m_memoryHandler.Reset(copiedErrorMessage);
	}
	else
		m_memoryHandler.Reset();
}

BaseException::BaseException(BaseException &exception):
	m_errorCode(exception.m_errorCode), m_memoryHandler(exception.m_memoryHandler.Release())
{

}

BaseException& BaseException::operator=(BaseException &exception)
{
	if (this != &exception)
	{
		m_errorCode = exception.m_errorCode;
		m_memoryHandler.Reset(exception.m_memoryHandler.Release());
	}
	
	return *this;
}

BaseException::~BaseException() throw()
{

}

int BaseException::code() const
{
	return m_errorCode;
}

const char *BaseException::what() const throw()
{
	return m_memoryHandler.Get();
}
