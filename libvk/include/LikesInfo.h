#pragma once
#include <cstdlib>

namespace sonapi
{
	class LikesInfo
	{
	private:
		size_t m_count;
		bool m_canLike, m_canPublish, m_userLikes;

	public:
		LikesInfo();
		bool CanLike() const;
		bool CanPublish() const;
		size_t GetCount() const;
		bool IsUserLikes() const;
		void SetCanLike(bool);
		void SetCanPublish(bool);
		void SetCount(size_t);
		void SetUserLikes(bool);
	};
}
