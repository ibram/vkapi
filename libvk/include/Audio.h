#pragma once
#include "DataTraits.h"

namespace sonapi
{
	template<DataType T> class Audio
	{
		typedef typename DataTraits<T>::String String;
		
	private:
		String m_artist, m_audioId, m_ownerId, m_title, m_url;
		size_t m_duration;
	
	public:
		const String &GetArtist() const;
		const String &GetAudioId() const;
		size_t GetDuration() const;
		const String &GetOwnerId() const;
		const String &GetTitle() const;
		const String &GetURL() const;
		void SetArtist(const String&);
		void SetAudioId(const String&);
		void SetDuration(size_t);
		void SetOwnerId(const String&);
		void SetTitle(const String&);
		void SetURL(const String&);
	};
	
	template<DataType T> const typename Audio<T>::String &Audio<T>::GetArtist() const
	{
		return m_artist;
	}
	
	template<DataType T> const typename Audio<T>::String &Audio<T>::GetAudioId() const
	{
		return m_audioId;
	}
	
	template<DataType T> size_t Audio<T>::GetDuration() const
	{
		return m_duration;
	}
	
	template<DataType T> const typename Audio<T>::String &Audio<T>::GetOwnerId() const
	{
		return m_ownerId;
	}
	
	template<DataType T> const typename Audio<T>::String &Audio<T>::GetTitle() const
	{
		return m_title;
	}
	
	template<DataType T> const typename Audio<T>::String &Audio<T>::GetURL() const
	{
		return m_url;
	}
	
	template<DataType T> void Audio<T>::SetArtist(const String &artist)
	{
		m_artist = artist;
	}
	
	template<DataType T> void Audio<T>::SetAudioId(const String &audioId)
	{
		m_audioId = audioId;
	}
	
	template<DataType T> void Audio<T>::SetDuration(size_t duration)
	{
		m_duration = duration;
	}
	
	template<DataType T> void Audio<T>::SetOwnerId(const String &ownerId)
	{
		m_ownerId = ownerId;
	}
	
	template<DataType T> void Audio<T>::SetTitle(const String &title)
	{
		m_title = title;
	}
	
	template<DataType T> void Audio<T>::SetURL(const String &url)
	{
		m_url = url;
	}
}