#pragma once
#include <cstdlib>
using namespace std;

namespace sonapi
{
	class UserCounter
	{
	private:
		size_t m_albumsCount, m_audiosCount, m_followersCount, m_friendsCount, m_mutualFriendsCount, m_notesCount;
		size_t m_onlineFriendsCount, m_photosWithUserCount, m_subscriptionsCount, m_videosCount, m_videosWithUserCount;
		
	public:
		UserCounter();
		size_t GetAlbumsCount() const;
		size_t GetAudiosCount() const;
		size_t GetFollowersCount() const;
		size_t GetFriendsCount() const;
		size_t GetMutualFriendsCount() const;
		size_t GetNotesCount() const;
		size_t GetOnlineFriendsCount() const;
		size_t GetPhotosWithUserCount() const;
		size_t GetSubscriptionsCount() const;
		size_t GetVideosCount() const;
		size_t GetVideosWithUserCount() const;
		void SetAlbumsCount(size_t);
		void SetAudiosCount(size_t);
		void SetFollowersCount(size_t);
		void SetFriendsCount(size_t);
		void SetMutualFriendsCount(size_t);
		void SetNotesCount(size_t);
		void SetOnlineFriendsCount(size_t);
		void SetPhotosWithUserCount(size_t);
		void SetSubscriptionsCount(size_t);
		void SetVideosCount(size_t);
		void SetVideosWithUserCount(size_t);
	};
}
