#pragma once
#include "MediaType.h"

namespace sonapi
{
	template<DataType T> class Media
	{
		typedef typename DataTraits<T>::String String;

	private:
		String m_id, m_ownerId, m_url;
		MediaType m_type;

	public:
		const String &GetId() const;
		const String &GetOwnerId() const;
		MediaType GetType() const;
		const String &GetUrl() const;
		void SetId(const String&);
		void SetOwnerId(const String&);
		void SetType(MediaType);
		void SetUrl(const String&);
	};
	
	template<DataType T> const typename Media<T>::String &Media<T>::GetId() const
	{
		return m_id;
	}
	
	template<DataType T> const typename Media<T>::String &Media<T>::GetOwnerId() const
	{
		return m_ownerId;
	}
	
	template<DataType T> MediaType Media<T>::GetType() const
	{
		return m_type;
	}
	
	template<DataType T> const typename Media<T>::String &Media<T>::GetUrl() const
	{
		return m_url;
	}
	
	template<DataType T> void Media<T>::SetId(const String &id)
	{
		m_id = id;
	}
	
	template<DataType T> void Media<T>::SetOwnerId(const String &ownerId)
	{
		m_ownerId = ownerId;
	}
	
	template<DataType T> void Media<T>::SetType(MediaType type)
	{
		m_type = type;
	}
	
	template<DataType T> void Media<T>::SetUrl(const String &url)
	{
		m_url = url;
	}
}
