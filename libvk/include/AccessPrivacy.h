#pragma once

namespace sonapi
{
	enum AccessPrivacy
	{
		AccessPrivacy_AllUsers,
		AccessPrivacy_FriendsOnly,
		AccessPrivacy_FriendsAndSubFriends,
		AccessPrivacy_MeOnly
	};
}