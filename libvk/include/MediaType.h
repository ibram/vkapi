#pragma once

namespace sonapi
{
	enum MediaType
	{
		MediaType_Audio,
		MediaType_Document,
		MediaType_Photo,
		MediaType_Video
	};
}