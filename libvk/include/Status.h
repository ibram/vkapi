#pragma once
#include "FriendStatus.h"

namespace sonapi
{
	template<DataType T> class Status
	{
		typedef typename DataTraits<T>::String String;
		
	private:
		String m_requestMessage, m_userId;
		FriendStatus m_friendStatus;
		
	public:
		FriendStatus Get() const;
		const String &GetRequestMessage() const;
		const String &GetUserId() const;
		void Set(FriendStatus);
		void SetRequestMessage(const String&);
		void SetUserId(const String&);
	};
	
	template<DataType T> FriendStatus Status<T>::Get() const
	{
		return m_friendStatus;
	}
	
	template<DataType T> const typename Status<T>::String &Status<T>::GetRequestMessage() const
	{
		return m_requestMessage;
	}
	
	template<DataType T> const typename Status<T>::String &Status<T>::GetUserId() const
	{
		return m_userId;
	}
	
	template<DataType T> void Status<T>::Set(FriendStatus friendStatus)
	{
		m_friendStatus = friendStatus;
	}
	
	template<DataType T> void Status<T>::SetRequestMessage(const String &requestMessage)
	{
		m_requestMessage = requestMessage;
	}
	
	template<DataType T> void Status<T>::SetUserId(const String &userId)
	{
		m_userId = userId;
	}
}