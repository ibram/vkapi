#pragma once
#include <cstdlib>

namespace sonapi
{
	class CommentsInfo
	{
		size_t m_count;
		bool m_canPost;

	public:
		CommentsInfo();
		bool CanPost() const;
		size_t GetCount() const;
		void SetCanPost(bool);
		void SetCount(size_t);
	};
}
