#pragma once
#include "News.h"
#include "Profile.h"

namespace sonapi
{
	template<DataType T> class NewsFeed
	{
		typedef typename DataTraits<T>::String String;

	private:
		vector<Group<T> > m_groups;
		vector<News<T> > m_news;
		vector<Profile<T> > m_profiles;
		String m_newFromField;
		size_t m_newOffset;

	public:
		const vector<Group<T> > &GetGroups() const;
		const String &GetNewFromField() const;
		const vector<News<T> > &GetNews() const;
		const vector<Profile<T> > &GetProfiles() const;
		size_t GetNewOffset() const;
		void SetGroups(const vector<Group<T> >&);
		void SetNewFromField(const String&);
		void SetNews(const vector<News<T> >&);
		void SetProfiles(const vector<Profile<T> >&);
		void SetNewOffset(size_t);
	};
	
	template<DataType T> const vector<Group<T> > &NewsFeed<T>::GetGroups() const
	{
		return m_groups;
	}
	
	template<DataType T> const typename NewsFeed<T>::String &NewsFeed<T>::GetNewFromField() const
	{
		return m_newFromField;
	}
	
	template<DataType T> const vector<News<T> > &NewsFeed<T>::GetNews() const
	{
		return m_news;
	}
	
	template<DataType T> const vector<Profile<T> > &NewsFeed<T>::GetProfiles() const
	{
		return m_profiles;
	}
	
	template<DataType T> size_t NewsFeed<T>::GetNewOffset() const
	{
		return m_newOffset;
	}
	
	template<DataType T> void NewsFeed<T>::SetGroups(const vector<Group<T> > &groups)
	{
		m_groups = groups;
	}
	
	template<DataType T> void NewsFeed<T>::SetNewFromField(const String &newFromField)
	{
		m_newFromField = newFromField;}
	
	template<DataType T> void NewsFeed<T>::SetNews(const vector<News<T> >& news)
	{
		m_news = news;
	}
	
	template<DataType T> void NewsFeed<T>::SetProfiles(const vector<Profile<T> >& profiles)
	{
		m_profiles = profiles;
	}
	
	template<DataType T> void NewsFeed<T>::SetNewOffset(size_t newOffset)
	{
		m_newOffset = newOffset;
	}
}
