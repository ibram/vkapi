#pragma once

namespace sonapi
{
	enum AudioSortType
	{
		AudioSortType_Date,
		AudioSortType_Duration,
		AudioSortType_Popularity
	};
}