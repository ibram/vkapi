#pragma once

namespace sonapi
{
	template<DataType T> class PhotoAttachment
	{
		typedef typename DataTraits<T>::String String;

	private:
		String m_ownerId, m_photoId, m_photoURL, m_previewURL;

	public:
		const String &GetBigPhotoURL() const;
		const String &GetOwnerId() const;
		const String &GetPhotoId() const;
		const String &GetPreviewURL() const;
		void SetOwnerId(const String&);
		void SetPhotoId(const String&);
		void SetPhotoURL(const String&);
		void SetPreviewURL(const String&);
		
	};
	
	template<DataType T> const typename PhotoAttachment<T>::String &PhotoAttachment<T>::GetBigPhotoURL() const
	{
		return m_photoURL;
	}
	
	template<DataType T> const typename PhotoAttachment<T>::String &PhotoAttachment<T>::GetOwnerId() const
	{
		return m_ownerId;
	}
	
	template<DataType T> const typename PhotoAttachment<T>::String &PhotoAttachment<T>::GetPhotoId() const
	{
		return m_photoId;
	}
	
	template<DataType T> const typename PhotoAttachment<T>::String &PhotoAttachment<T>::GetPreviewURL() const
	{
		return m_previewURL;
	}
	
	template<DataType T> void PhotoAttachment<T>::SetOwnerId(const String &ownerId)
	{
		m_ownerId = ownerId;
	}
	
	template<DataType T> void PhotoAttachment<T>::SetPhotoId(const String &photoId)
	{
		m_photoId = photoId;
	}
	
	template<DataType T> void PhotoAttachment<T>::SetPhotoURL(const String &photoURL)
	{
		m_photoURL = photoURL;
	}
	
	template<DataType T> void PhotoAttachment<T>::SetPreviewURL(const String &previewURL)
	{
		m_previewURL = previewURL;
	}
}
