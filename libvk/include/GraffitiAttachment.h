#pragma once

namespace sonapi
{
	template<DataType T> class GraffitiAttachment
	{
		typedef typename DataTraits<T>::String String;

	private:
		String m_imageUrl, m_id, m_ownerId, m_previewUrl;

	public:
		const String &GetId() const;
		const String &GetImageURL() const;
		const String &GetOwnerId() const;
		const String &GetPreviewURL() const;
		void SetId(const String&);
		void SetImageURL(const String&);
		void SetOwnerId(const String&);
		void SetPreviewURL(const String&);
	};
	
	template<DataType T> const typename GraffitiAttachment<T>::String &GraffitiAttachment<T>::GetId() const
	{
		return m_id;
	}
	
	template<DataType T> const typename GraffitiAttachment<T>::String &GraffitiAttachment<T>::GetImageURL() const
	{
		return m_imageUrl;
	}
	
	template<DataType T> const typename GraffitiAttachment<T>::String &GraffitiAttachment<T>::GetOwnerId() const
	{
		return m_ownerId;
	}
	
	template<DataType T> const typename GraffitiAttachment<T>::String &GraffitiAttachment<T>::GetPreviewURL() const
	{
		return m_previewUrl;
	}
	
	template<DataType T> void GraffitiAttachment<T>::SetImageURL(const String &imageUrl)
	{
		m_imageUrl = imageUrl;
	}
	
	template<DataType T> void GraffitiAttachment<T>::SetId(const String &id)
	{
		m_id = id;
	}
	
	template<DataType T> void GraffitiAttachment<T>::SetOwnerId(const String &ownerId)
	{
		m_ownerId = ownerId;
	}
	
	template<DataType T> void GraffitiAttachment<T>::SetPreviewURL(const String &url)
	{
		m_previewUrl = url;
	}
}
