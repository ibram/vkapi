#pragma once

#include "ApplicationAttachment.h"
#include "AttachmentType.h"
#include "AudioAttachment.h"
#include "Document.h"
#include "GraffitiAttachment.h"
#include "LinkAttachment.h"
#include "Note.h"
#include "PhotoAttachment.h"
#include "PollAttachment.h"
#include "VideoAttachment.h"
#include "WikiPageAttachment.h"

namespace sonapi
{
	template<DataType T> class Attachment
	{
	private:
		ApplicationAttachment<T> *m_appAttachment;
		AudioAttachment<T> *m_audioAttachment;
		Document<T> *m_docAttachment;
		GraffitiAttachment<T> *m_graffitiAttachment;
		LinkAttachment<T> *m_linkAttachment;
		Note<T> *m_noteAttachment;
		PhotoAttachment<T> *m_photoAttachment;
		PollAttachment<T> *m_pollAttachment;
		VideoAttachment<T> *m_videoAttachment;
		WikiPageAttachment<T> *m_wikiPageAttachment;
		AttachmentType m_type;

	public:
		const ApplicationAttachment<T> *GetApp() const;
		const AudioAttachment<T> *GetAudio() const;
		const Document<T> *GetDoc() const;
		const GraffitiAttachment<T> *GetGraffiti() const;
		const LinkAttachment<T> *GetLink() const;
		const Note<T> *GetNote() const;
		const PhotoAttachment<T> *GetPhoto() const;
		const PollAttachment<T> *GetPoll() const;
		AttachmentType GetType() const;
		const VideoAttachment<T> *GetVideo() const;
		const WikiPageAttachment<T> *GetWikiPage() const;
		void SetApp(const ApplicationAttachment<T> *appAttachment);
		void SetAudio(const AudioAttachment<T> *audioAttachment);
		void SetDoc(const Document<T> *docAttachment);
		void SetGraffiti(const GraffitiAttachment<T> *graffitiAttachment);
		void SetLink(const LinkAttachment<T> *linkAttachment);
		void SetNote(const Note<T> *noteAttachment);
		void SetPhoto(const PhotoAttachment<T> *photoAttachment);
		void SetPoll(const PollAttachment<T> *pollAttachment);
		void SetType(const AttachmentType);
		void SetVideo(const VideoAttachment<T> *videoAttachment);
		void SetWikiPage(const WikiPageAttachment<T> *wiliPageAttachment);
	};
	
	template<DataType T> const ApplicationAttachment<T> *Attachment<T>::GetApp() const
	{
		return m_appAttachment;
	}
	
	template<DataType T> const AudioAttachment<T> *Attachment<T>::GetAudio() const
	{
		return m_audioAttachment;
	}
	
	template<DataType T> const Document<T> *Attachment<T>::GetDoc() const
	{
		return m_docAttachment;
	}
	
	template<DataType T> const GraffitiAttachment<T> *Attachment<T>::GetGraffiti() const
	{
		return m_graffitiAttachment;
	}
	
	template<DataType T> const LinkAttachment<T> *Attachment<T>::GetLink() const
	{
		return m_linkAttachment;
	}
	
	template<DataType T> const Note<T> *Attachment<T>::GetNote() const
	{
		return m_noteAttachment;
	}
	
	template<DataType T> const PhotoAttachment<T> *Attachment<T>::GetPhoto() const
	{
		return m_photoAttachment;
	}
	
	template<DataType T> const PollAttachment<T> *Attachment<T>::GetPoll() const
	{
		return m_pollAttachment;
	}
	
	template<DataType T> AttachmentType Attachment<T>::GetType() const
	{
		return m_type;
	}
	
	template<DataType T> const VideoAttachment<T> *Attachment<T>::GetVideo() const
	{
		return m_videoAttachment;
	}
	
	template<DataType T> const WikiPageAttachment<T> *Attachment<T>::GetWikiPage() const
	{
		return m_wikiPageAttachment;
	}
	
	template<DataType T> void Attachment<T>::SetApp(const ApplicationAttachment<T> *appAttachment)
	{
		m_appAttachment = const_cast<ApplicationAttachment<T>*>(appAttachment);
	}
	
	template<DataType T> void Attachment<T>::SetAudio(const AudioAttachment<T> *audioAttachment)
	{
		m_audioAttachment = const_cast<AudioAttachment<T>*>(audioAttachment);
	}
	
	template<DataType T> void Attachment<T>::SetDoc(const Document<T> *docAttachment)
	{
		m_docAttachment = const_cast<Document<T>*>(docAttachment);
	}
	
	template<DataType T> void Attachment<T>::SetGraffiti(const GraffitiAttachment<T> *graffitiAttachment)
	{
		m_graffitiAttachment = const_cast<GraffitiAttachment<T>*>(graffitiAttachment);
	}
	
	template<DataType T> void Attachment<T>::SetLink(const LinkAttachment<T> *linkAttachment)
	{
		m_linkAttachment = const_cast<LinkAttachment<T>*>(linkAttachment);
	}
	
	template<DataType T> void Attachment<T>::SetNote(const Note<T> *noteAttachment)
	{
		m_noteAttachment = const_cast<Note<T>*>(noteAttachment);
	}
	
	template<DataType T> void Attachment<T>::SetPhoto(const PhotoAttachment<T> *photoAttachment)
	{
		m_photoAttachment = const_cast<PhotoAttachment<T>*>(photoAttachment);
	}
	
	template<DataType T> void Attachment<T>::SetPoll(const PollAttachment<T> *pollAttachment)
	{
		m_pollAttachment = const_cast<PollAttachment<T>*>(pollAttachment);
	}
	
	template<DataType T> void Attachment<T>::SetType(const AttachmentType attachmentType)
	{
		m_type = attachmentType;
	}
	
	template<DataType T> void Attachment<T>::SetVideo(const VideoAttachment<T> *videoAttachment)
	{
		m_videoAttachment = const_cast<VideoAttachment<T>*>(videoAttachment);
	}
	
	template<DataType T> void Attachment<T>::SetWikiPage(const WikiPageAttachment<T> *wikiPageAttachment)
	{
		m_wikiPageAttachment = const_cast<WikiPageAttachment<T>*>(wikiPageAttachment);
	}
}
