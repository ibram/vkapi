#pragma once

namespace sonapi
{
	enum LikeType
	{
		LikeType_Post,
		LikeType_Comment,
		LikeType_Photo,
		LikeType_Audio,
		LikeType_Video,
		LikeType_Note,
		LikeType_Sitepage
	};
}