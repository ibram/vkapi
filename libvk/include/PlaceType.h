#pragma once

namespace sonapi
{
	template<DataType T> class PlaceType
	{
		typedef typename DataTraits<T>::String String;

	private:
		String m_id, m_iconURL, m_title;

	public:
		const String &GetIconURL() const;
		const String &GetId() const;
		const String &GetTitle() const;
		void SetIconURL(const String&);
		void SetId(const String&);
		void SetTitle(const String&);
	};
	
	template<DataType T> const typename PlaceType<T>::String &PlaceType<T>::GetIconURL() const
	{
		return m_iconURL;
	}
	
	template<DataType T> const typename PlaceType<T>::String &PlaceType<T>::GetId() const
	{
		return m_id;
	}
	
	template<DataType T> const typename PlaceType<T>::String &PlaceType<T>::GetTitle() const
	{
		return m_title;
	}
	
	template<DataType T> void PlaceType<T>::SetIconURL(const String &iconURL)
	{
		m_iconURL = iconURL;
	}
	
	template<DataType T> void PlaceType<T>::SetId(const String &id)
	{
		m_id = id;
	}
	
	template<DataType T> void PlaceType<T>::SetTitle(const String &title)
	{
		m_title = title;
	}
}
