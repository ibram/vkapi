#pragma once

namespace sonapi
{
	enum NotesSortType
	{
		NotesSortType_DateDescending,
		NotesSortType_DateAscending
	};
}