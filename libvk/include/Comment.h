#pragma once
#include "LikesInfo.h"

namespace sonapi
{
	template<DataType T> class Comment
	{
		typedef typename DataTraits<T>::String String;

	private:
		LikesInfo m_likes;
		String m_id, m_message, m_noteId, m_ownerId, m_replyToCommentId, m_replyToUserId, m_text, m_userId;
		time_t m_date;

	public:
		Comment();
		time_t GetDate() const;
		const String &GetId() const;
		const LikesInfo &GetLikes() const;
		const String &GetNoteId() const;
		const String &GetOwnerId() const;
		const String &GetReplyToCommentId() const;
		const String &GetReplyToUserId() const;
		const String &GetText() const;
		const String &GetUserId() const;
		void SetDate(time_t);
		void SetId(const String &);
		void SetLikes(const LikesInfo &);
		void SetNoteId(const String &);
		void SetOwnerId(const String &);
		void SetReplyToCommentId(const String &);
		void SetReplyToUserId(const String &);
		void SetText(const String &);
		void SetUserId(const String &);
	};

	template<DataType T> Comment<T>::Comment():
		m_date(0)
	{
	
	}
	
	template<DataType T> time_t Comment<T>::GetDate() const
	{
		return m_date;
	}

	template<DataType T> const typename Comment<T>::String &Comment<T>::GetId() const
	{
		return m_id;
	}

	template<DataType T> const LikesInfo &Comment<T>::GetLikes() const
	{
		return m_likes;
	}
	
	template<DataType T> const typename Comment<T>::String &Comment<T>::GetNoteId() const
	{
		return m_noteId;
	}
	
	template<DataType T> const typename Comment<T>::String &Comment<T>::GetOwnerId() const
	{
		return m_ownerId;
	}
	
	template<DataType T> const typename Comment<T>::String &Comment<T>::GetReplyToCommentId() const
	{
		return m_replyToCommentId;
	}

	template<DataType T> const typename Comment<T>::String &Comment<T>::GetReplyToUserId() const
	{
		return m_replyToUserId;
	}

	template<DataType T> const typename Comment<T>::String &Comment<T>::GetText() const
	{
		return m_text;
	}

	template<DataType T> const typename Comment<T>::String &Comment<T>::GetUserId() const
	{
		return m_userId;
	}

	template<DataType T> void Comment<T>::SetDate(time_t date)
	{
		m_date = date;
	}

	template<DataType T> void Comment<T>::SetId(const String &id)
	{
		m_id = id;
	}

	template<DataType T> void Comment<T>::SetLikes(const LikesInfo &likes)
	{
		m_likes = likes;
	}
	
	template<DataType T> void Comment<T>::SetNoteId(const String &noteId)
	{
		m_noteId = noteId;
	}
	
	template<DataType T> void Comment<T>::SetOwnerId(const String &ownerId)
	{
		m_ownerId = ownerId;
	}
	
	template<DataType T> void Comment<T>::SetReplyToCommentId(const String &replyToCommentId)
	{
		m_replyToCommentId = replyToCommentId;
	}

	template<DataType T> void Comment<T>::SetReplyToUserId(const String &replyToUserId)
	{
		m_replyToUserId = replyToUserId;
	}

	template<DataType T> void Comment<T>::SetText(const String &text)
	{
		m_text = text;
	}

	template<DataType T> void Comment<T>::SetUserId(const String &userId)
	{
		m_userId = userId;
	}
}
