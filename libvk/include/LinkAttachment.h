#pragma once

namespace sonapi
{
	template<DataType T> class LinkAttachment
	{
		typedef typename DataTraits<T>::String String;

	private:
		String m_description, m_previewURL, m_title, m_url;

	public:
		const String &GetDescription() const;
		const String &GetPreviewURL() const;
		const String &GetTitle() const;
		const String &GetUrl() const;
		void SetDescription(const String &description);
		void SetPreviewURL(const String &previewURL);
		void SetTitle(const String &title);
		void SetUrl(const String &url);
	};
	
	template<DataType T> const typename LinkAttachment<T>::String &LinkAttachment<T>::GetDescription() const
	{
		return m_description;
	}
	
	template<DataType T> const typename LinkAttachment<T>::String &LinkAttachment<T>::GetPreviewURL() const
	{
		return m_previewURL;
	}
	
	template<DataType T> const typename LinkAttachment<T>::String &LinkAttachment<T>::GetTitle() const
	{
		return m_title;
	}
	
	template<DataType T> const typename LinkAttachment<T>::String &LinkAttachment<T>::GetUrl() const
	{
		return m_url;
	}
	
	template<DataType T> void LinkAttachment<T>::SetDescription(const String &description)
	{
		m_description = description;
	}
	
	template<DataType T> void LinkAttachment<T>::SetPreviewURL(const String &previewURL)
	{
		m_previewURL = previewURL;
	}
	
	template<DataType T> void LinkAttachment<T>::SetTitle(const String &title)
	{
		m_title = title;
	}
	
	template<DataType T> void LinkAttachment<T>::SetUrl(const String &url)
	{
		m_url = url;
	}
}
