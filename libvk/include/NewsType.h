#pragma once

namespace sonapi
{
	enum NewsType
	{
		NewsType_Post = 1,
		NewsType_Photo,
		NewsType_PhotoTag = 4,
		NewsType_Friend = 8,
		NewsType_Note = 16,
		NewsType_All = NewsType_Post | NewsType_Photo | NewsType_PhotoTag |
			NewsType_Friend | NewsType_Note
	};
}