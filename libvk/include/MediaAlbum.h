#pragma once

namespace sonapi
{
	template<DataType T> class MediaAlbum
	{
		typedef typename DataTraits<T>::String String;

	private:
		String m_id, m_ownerId, m_title;

	public:
		const String &GetId() const;
		const String &GetOwnerId() const;
		const String &GetTitle() const;
		void SetId(const String&);
		void SetOwnerId(const String&);
		void SetTitle(const String&);
	};
	
	template<DataType T> const typename MediaAlbum<T>::String &MediaAlbum<T>::GetId() const
	{
		return m_id;
	}
	
	template<DataType T> const typename MediaAlbum<T>::String &MediaAlbum<T>::GetOwnerId() const
	{
		return m_ownerId;
	}
	
	template<DataType T> const typename MediaAlbum<T>::String &MediaAlbum<T>::GetTitle() const
	{
		return m_title;
	}
	
	template<DataType T> void MediaAlbum<T>::SetId(const String &id)
	{
		m_id = id;
	}
	
	template<DataType T> void MediaAlbum<T>::SetOwnerId(const String &ownerId)
	{
		m_ownerId = ownerId;
	}
	
	template<DataType T> void MediaAlbum<T>::SetTitle(const String &title)
	{
		m_title = title;
	}
}
