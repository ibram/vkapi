#pragma once
#include <cstdlib>

namespace sonapi
{
	class RepostsInfo
	{
	private:
		size_t m_count;
		bool m_userReposted;
		
	public:
		RepostsInfo();
		size_t GetCount() const;
		bool UserReposted() const;
		void SetCount(size_t);
		void SetUserReposted(bool);
	};
}