#pragma once

namespace sonapi
{
	enum AttachmentType
	{
		AttachmentType_App,
		AttachmentType_Audio,
		AttachmentType_Document,
		AttachmentType_Graffiti,
		AttachmentType_Link,
		AttachmentType_Note,
		AttachmentType_Photo,
		AttachmentType_Poll,
		AttachmentType_PostedPhoto,
		AttachmentType_Video,
		AttachmentType_WikiPage
	};
}