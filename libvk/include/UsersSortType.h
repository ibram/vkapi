#pragma once

namespace sonapi
{
	enum UsersSortType
	{
		UsersSortType_Id_Ascending,
		UsersSortType_Id_Descending,
		UsersSortType_Time_Ascending,
		UsersSortType_Time_Descending
	};
}