#pragma once

namespace sonapi
{
	enum VideoWidth
	{
		VideoWidth_130,
		VideoWidth_160,
		VideoWidth_320
	};
}