#pragma once
#include "AccessPrivacy.h"

namespace sonapi
{
	template<DataType T> class Note
	{
		typedef typename DataTraits<T>::String String;

	private:
		String m_id, m_ownerId, m_text, m_textWiki, m_title, m_userId;
		time_t m_date;
		size_t m_commentsCount, m_readCommentsCount;
		AccessPrivacy m_privacy, m_commentPrivacy;
		bool m_canComment;

	public:
		Note();
		bool CanComment() const;
		AccessPrivacy GetCommentPrivacy() const;
		size_t GetCommentsCount() const;
		time_t GetDate() const;
		const String &GetId() const;
		const String &GetOwnerId() const;
		AccessPrivacy GetPrivacy() const;
		size_t GetReadCommentsCount() const;
		const String &GetText() const;
		const String &GetTextWiki() const;
		const String &GetTitle() const;
		const String &GetUserId() const;
		void SetCommentAbility(bool);
		void SetCommentPrivacy(AccessPrivacy);
		void SetCommentsCount(size_t);
		void SetDate(time_t);
		void SetId(const String&);
		void SetOwnerId(const String&);
		void SetPrivacy(AccessPrivacy);
		void SetReadCommentsCount(size_t);
		void SetText(const String&);
		void SetTextWiki(const String&);
		void SetTitle(const String&);
		void SetUserId(const String&);
			
	};
	
	template<DataType T> Note<T>::Note():
		m_commentsCount(0), m_readCommentsCount(0)
	{
			
	}
	
	template<DataType T> bool Note<T>::CanComment() const
	{
		return m_canComment;
	}
	
	template<DataType T> AccessPrivacy Note<T>::GetCommentPrivacy() const
	{
		return m_commentPrivacy;
	}
	
	template<DataType T> size_t Note<T>::GetCommentsCount() const
	{
		return m_commentsCount;
	}
	
	template<DataType T> time_t Note<T>::GetDate() const
	{
		return m_date;
	}
	
	template<DataType T> const typename Note<T>::String &Note<T>::GetId() const
	{
		return m_id;
	}
	
	template<DataType T> const typename Note<T>::String &Note<T>::GetOwnerId() const
	{
		return m_ownerId;
	}
	
	template<DataType T> AccessPrivacy Note<T>::GetPrivacy() const
	{
		return m_privacy;
	}
	
	template<DataType T> size_t Note<T>::GetReadCommentsCount() const
	{
		return m_readCommentsCount;
	}
	
	template<DataType T> const typename Note<T>::String &Note<T>::GetText() const
	{
		return m_text;
	}
	
	template<DataType T> const typename Note<T>::String &Note<T>::GetTextWiki() const
	{
		return m_textWiki;
	}
	
	template<DataType T> const typename Note<T>::String &Note<T>::GetTitle() const
	{
		return m_title;
	}
	
	template<DataType T> const typename Note<T>::String &Note<T>::GetUserId() const
	{
		return m_userId;
	}
	
	template<DataType T> void Note<T>::SetCommentAbility(bool canComment)
	{
		m_canComment = canComment;
	}
	
	template<DataType T> void Note<T>::SetCommentPrivacy(AccessPrivacy commentPrivacy)
	{
		m_commentPrivacy = commentPrivacy;
	}
	
	template<DataType T> void Note<T>::SetCommentsCount(size_t commentsCount)
	{
		m_commentsCount = commentsCount;
	}
	
	template<DataType T> void Note<T>::SetDate(time_t date)
	{
		m_date = date;
	}
	
	template<DataType T> void Note<T>::SetId(const String &id)
	{
		m_id = id;
	}
	
	template<DataType T> void Note<T>::SetOwnerId(const String &ownerId)
	{
		m_ownerId = ownerId;
	}
	
	template<DataType T> void Note<T>::SetPrivacy(AccessPrivacy privacy)
	{
		m_privacy = privacy;
	}
	
	template<DataType T> void Note<T>::SetReadCommentsCount(size_t readCommentsCount)
	{
		m_readCommentsCount = readCommentsCount;
	}
	
	template<DataType T> void Note<T>::SetText(const String &text)
	{
		m_text = text;
	}
	
	template<DataType T> void Note<T>::SetTextWiki(const String &textWiki)
	{
		m_textWiki = textWiki;
	}
	
	template<DataType T> void Note<T>::SetTitle(const String &title)
	{
		m_title = title;
	}
	
	template<DataType T> void Note<T>::SetUserId(const String &userId)
	{
		m_userId = userId;
	}
}
