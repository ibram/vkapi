﻿#pragma once
#include <cstring>
#include "BaseException.h"
using namespace std;

namespace sonapi
{
	/**
	 * \brief Исключение, которое выбрасывается, если сервер ответил на запрос < a href = "http://ru.wikipedia.org/wiki/CAPTCHA" > CAPTCHA'ей</a>.
	 */
	class CaptchaNeedException: public BaseException
	{
	private:
		char *m_captchaSid, *m_captchaUrl;
		
	public:
		CaptchaNeedException(int, const char*);
		~CaptchaNeedException() throw();
		const char *GetCaptchaSid() const;
		const char *GetCaptchaURL() const;
		void SetData(const char*, const char*);
	};
}
