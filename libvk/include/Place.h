#pragma once
#include "PlaceType.h"

namespace sonapi
{
	template<DataType T> class Place
	{
		typedef typename DataTraits<T>::String String;

	private:
		String m_cityId, m_countryId, m_id, m_title, typeId;
		float m_latitude, m_longitude;
		time_t m_createdTime;

	public:
		const String &GetCityId() const;
		const String &GetCountryId() const;
		time_t GetCreatedTime() const;
		const String &GetId() const;
		float GetLatitude() const;
		float GetLongitude() const;
		const String &GetTitle() const;
		const String &GetTypeId() const;
		void SetCityId(const String&);
		void SetCountryId(const String&);
		void SetCreatedTime(time_t);
		void SetId(const String&);
		void SetLatitude(float);
		void SetLongitude(float);
		void SetTitle(const String&);
		void SetTypeId(const String&);
	};
	
	template<DataType T> const typename Place<T>::String &Place<T>::GetCityId() const
	{
		return m_cityId;
	}
	
	template<DataType T> const typename Place<T>::String &Place<T>::GetCountryId() const
	{
		return m_countryId;
	}
	
	template<DataType T> time_t Place<T>::GetCreatedTime() const
	{
		return m_createdTime;
	}
	
	template<DataType T> const typename Place<T>::String &Place<T>::GetId() const
	{
		return m_id;
	}
	
	template<DataType T> float Place<T>::GetLatitude() const
	{
		return m_latitude;
	}
	
	template<DataType T> float Place<T>::GetLongitude() const
	{
		return m_longitude;
	}
	
	template<DataType T> const typename Place<T>::String &Place<T>::GetTitle() const
	{
		return m_title;
	}
	
	template<DataType T> const typename Place<T>::String &Place<T>::GetTypeId() const
	{
		return typeId;
	}
	
	template<DataType T> void Place<T>::SetCityId(const String &cityId)
	{
		this->m_cityId = cityId;
	}
	
	template<DataType T> void Place<T>::SetCountryId(const String &countryId)
	{
		this->m_countryId = countryId;
	}
	
	template<DataType T> void Place<T>::SetCreatedTime(time_t createdTime)
	{
		this->m_createdTime = createdTime;
	}
	
	template<DataType T> void Place<T>::SetId(const String &id)
	{
		this->m_id = id;
	}
	
	template<DataType T> void Place<T>::SetLatitude(float latitude)
	{
		this->m_latitude = latitude;
	}
	
	template<DataType T> void Place<T>::SetLongitude(float longitude)
	{
		this->m_longitude = longitude;
	}
	
	template<DataType T> void Place<T>::SetTitle(const String &title)
	{
		this->m_title = title;
	}
	
	template<DataType T> void Place<T>::SetTypeId(const String &typeId)
	{
		this->typeId = typeId;
	}
}
