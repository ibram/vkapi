#pragma once

namespace sonapi
{
	template<DataType T> class PollAttachment
	{
		typedef typename DataTraits<T>::String String;

	private:
		String m_id, m_question;

	public:
		const String &GetId() const;
		const String &GetQuestion() const;
		void SetId(const String&);
		void SetQuestion(const String&);
		
	};
	
	template<DataType T> const typename PollAttachment<T>::String &PollAttachment<T>::GetId() const
	{
		return m_id;
	}
	
	template<DataType T> const typename PollAttachment<T>::String &PollAttachment<T>::GetQuestion() const
	{
		return m_question;
	}
	
	template<DataType T> void PollAttachment<T>::SetId(const String &id)
	{
		m_id = id;
	}
	
	template<DataType T> void PollAttachment<T>::SetQuestion(const String &question)
	{
		m_question = question;
	}
}
