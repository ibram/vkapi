#pragma once

namespace sonapi
{
	enum WallPostFilter
	{
		WallPostFilter_Owner,
		WallPostFilter_Others,
		WallPostFilter_All
	};
}