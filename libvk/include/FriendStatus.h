#pragma once

namespace sonapi
{
	enum FriendStatus
	{
		NotFriend,
		OutcomingSubscription,
		IncomingSubscription,
		Friend
	};
}