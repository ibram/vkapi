#pragma once

namespace sonapi
{
	template<DataType T> class VideoAttachment
	{
		typedef typename DataTraits<T>::String String;

	private:
		String m_ownerId, m_title, m_videoId;
		time_t m_duration;

	public:
		time_t GetDuration() const;
		const String &GetOwnerId() const;
		const String &GetTitle() const;
		const String &GetVideoId() const;
		void SetDuration(time_t);
		void SetOwnerId(const String&);
		void SetTitle(const String&);
		void SetVideoId(const String&);
		
	};
	
	template<DataType T> time_t VideoAttachment<T>::GetDuration() const
	{
		return m_duration;
	}
	
	template<DataType T> const typename VideoAttachment<T>::String &VideoAttachment<T>::GetOwnerId() const
	{
		return m_ownerId;
	}
	
	template<DataType T> const typename VideoAttachment<T>::String &VideoAttachment<T>::GetTitle() const
	{
		return m_title;
	}
	
	template<DataType T> const typename VideoAttachment<T>::String &VideoAttachment<T>::GetVideoId() const {return m_videoId;
	}
	
	template<DataType T> void VideoAttachment<T>::SetDuration(time_t duration)
	{
		m_duration = duration;
	}
	
	template<DataType T> void VideoAttachment<T>::SetOwnerId(const String &ownerId)
	{
		m_ownerId = ownerId;
	}
	
	template<DataType T> void VideoAttachment<T>::SetTitle(const String &title)
	{
		m_title = title;
	}
	
	template<DataType T> void VideoAttachment<T>::SetVideoId(const String &videoId)
	{
		m_videoId = videoId;
	}
}
