#pragma once

namespace sonapi
{
	template<DataType T> class VideoTag
	{
		typedef typename DataTraits<T>::String String;

	private:
		String m_placerId, m_taggedName, m_id, m_userId;
		time_t m_date;
		bool m_isViewed;

	public:
		time_t GetDate() const;
		const String &GetId() const;
		const String &GetPlacerId() const;
		const String &GetTaggedName() const;
		const String &GetUserId() const;
		bool IsViewed() const;
		void SetDate(time_t);
		void SetId(const String&);
		void SetPlacerId(const String&);
		void SetTaggedName(const String&);
		void SetUserId(const String&);
		void SetViewedState(bool);			
	};
	
	template<DataType T> time_t VideoTag<T>::GetDate() const
	{
		return m_date;
	}
	
	template<DataType T> const typename VideoTag<T>::String &VideoTag<T>::GetId() const
	{
		return m_id;
	}
	
	template<DataType T> const typename VideoTag<T>::String &VideoTag<T>::GetPlacerId() const
	{
		return m_placerId;
	}
	
	template<DataType T> const typename VideoTag<T>::String &VideoTag<T>::GetTaggedName() const
	{
		return m_taggedName;
	}
	
	template<DataType T> const typename VideoTag<T>::String &VideoTag<T>::GetUserId() const
	{
		return m_userId;
	}
	
	template<DataType T> bool VideoTag<T>::IsViewed() const
	{
		return m_isViewed;
	}
	
	template<DataType T> void VideoTag<T>::SetDate(time_t date)
	{
		m_date = date;
	}
	
	template<DataType T> void VideoTag<T>::SetId(const String &id)
	{
		m_id = id;
	}
	
	template<DataType T> void VideoTag<T>::SetPlacerId(const String &placerId)
	{
		m_placerId = placerId;
	}
	
	template<DataType T> void VideoTag<T>::SetTaggedName(const String &taggedName)
	{
		m_taggedName = taggedName;
	}
	
	template<DataType T> void VideoTag<T>::SetUserId(const String &userId)
	{
		m_userId = userId;
	}
	
	template<DataType T> void VideoTag<T>::SetViewedState(bool isViewed)
	{
		m_isViewed = isViewed;
	}
}
