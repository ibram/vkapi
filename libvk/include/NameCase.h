#pragma once

namespace sonapi
{
	/// \brief Падеж при склонении.
	enum NameCase
	{
		NameCase_Nominative, ///< Именительный падеж.
		NameCase_Genitive, ///< Родительный падеж.
		NameCase_Dative, ///< Дательный падеж.
		NameCase_Accusative, ///< Винительный падеж.
		NameCase_Instrumental, ///< Творительный падеж.
		NameCase_Ablative ///< Предложный падеж.
	};
}