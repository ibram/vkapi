#pragma once

namespace sonapi
{
	enum CommentsSortType
	{
		CommentsSortType_Ascending,
		CommentsSortType_Descending
	};
}