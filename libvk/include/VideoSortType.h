#pragma once

namespace sonapi
{
	enum VideoSortType
	{
		VideoSortType_Date,
		VideoSortType_Duration,
		VideoSortType_Relevance
	};
}