#pragma once

namespace sonapi
{
	template<DataType T> class ApplicationAttachment
	{
		typedef typename DataTraits<T>::String String;

	private:
		String m_id, m_imageURL, m_name, m_previewImageURL;

	public:
		const String &GetId() const;
		const String &GetImageURL() const;
		const String &GetName() const;
		const String &GetPreviewImageURL() const;
		void SetId(const String&);
		void SetImageURL(const String&);
		void SetName(const String&);
		void SetPreviewImageURL(const String&);
		
	};
	
	template<DataType T> const typename ApplicationAttachment<T>::String &ApplicationAttachment<T>::GetId() const
	{
		return m_id;
	}
	
	template<DataType T> const typename ApplicationAttachment<T>::String &ApplicationAttachment<T>::GetImageURL() const
	{
		return m_imageURL;
	}
	
	template<DataType T> const typename ApplicationAttachment<T>::String &ApplicationAttachment<T>::GetName() const
	{
		return m_name;
	}
	
	template<DataType T> const typename ApplicationAttachment<T>::String &ApplicationAttachment<T>::GetPreviewImageURL() const
	{
		return m_previewImageURL;
	}
	
	template<DataType T> void ApplicationAttachment<T>::SetId(const String &id)
	{
		m_id = id;
	}
	
	template<DataType T> void ApplicationAttachment<T>::SetImageURL(const String &imageURL)
	{
		m_imageURL = imageURL;
	}
	
	template<DataType T> void ApplicationAttachment<T>::SetName(const String &name)
	{
		m_name = name;
	}
	
	template<DataType T> void ApplicationAttachment<T>::SetPreviewImageURL(const String &previewImageURL)
	{
		m_previewImageURL = previewImageURL;
	}
}
