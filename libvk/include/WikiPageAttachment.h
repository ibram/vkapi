#pragma once

namespace sonapi
{
	template<DataType T> class WikiPageAttachment
	{
		typedef typename DataTraits<T>::String String;

	private:
		String m_groupId, m_id, m_title;

	public:
		const String &GetGroupId() const;
		const String &GetId() const;
		const String &GetTitle() const;
		void SetGroupId(const String &groupId);
		void SetId(const String &id);
		void SetTitle(const String &title);
		
	};
	
	template<DataType T> const typename WikiPageAttachment<T>::String &WikiPageAttachment<T>::GetGroupId() const
	{
		return m_groupId;
	}
	
	template<DataType T> const typename WikiPageAttachment<T>::String &WikiPageAttachment<T>::GetId() const
	{
		return m_id;
	}
	
	template<DataType T> const typename WikiPageAttachment<T>::String &WikiPageAttachment<T>::GetTitle() const
	{
		return m_title;
	}
	
	template<DataType T> void WikiPageAttachment<T>::SetGroupId(const String &groupId)
	{
		m_groupId = groupId;
	}
	
	template<DataType T> void WikiPageAttachment<T>::SetId(const String &id)
	{
		m_id = id;
	}
	
	template<DataType T> void WikiPageAttachment<T>::SetTitle(const String &title)
	{
		m_title = title;
	}
}
