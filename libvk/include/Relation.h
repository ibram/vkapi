#pragma once

namespace sonapi
{
	/// \brief Семейное положение.
	enum Relation
	{
		Relation_NotMarried = 1, ///< Не женат/замужем.
		Relation_HasFriend, ///< Есть друг/подруга.
		Relation_Engaged, ///< Помолвлен(-а).
		Relation_Married, ///< Женат/Замужем.
		Relation_AllDifficult, ///< Все сложно.
		Relation_InActiveSearching, ///< В активном поиске.
		Relation_InLove ///< Влюблен(-а).
	};
}