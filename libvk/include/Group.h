#pragma once
#include "GroupCounter.h"

namespace sonapi
{
	template<DataType T> class Group
	{
		typedef typename DataTraits<T>::String String;

	private:
		String m_id, m_cityId, m_countryId, m_description, m_name, m_photo, m_photo_big;
		String m_photo_medium, m_screenName, m_wikiPage;
		GroupCounter m_counter;
		time_t m_startDate, m_endDate;
		size_t m_membersCount;
		bool m_admin, m_closed, m_member;

	public:
		Group();
		bool Admin() const;
		bool Closed() const;
		const String &GetCityId() const;
		const String &GetCountryId() const;
		const String &GetDescription() const;
		const GroupCounter &GetCounter() const;
		time_t GetEndDate() const;
		const String &GetId() const;
		size_t GetMembersCount() const;
		const String &GetName() const;
		const String &GetPhoto() const;
		const String &GetPhotoBig() const;
		const String &GetPhotoMedium() const;
		const String &GetScreenName() const;
		time_t GetStartDate() const;
		const String &GetWikiPage() const;
		bool Member() const;
		void SetAdmin(bool);
		void SetClosed(bool);
		void SetId(const String&);
		void SetCityId(const String&);
		void SetCounter(const GroupCounter&);
		void SetCountryId(const String&);
		void SetDescription(const String&);
		void SetEndDate(time_t);
		void SetMember(bool);
		void SetMembersCount(size_t);
		void SetName(const String&);
		void SetPhoto(const String&);
		void SetPhotoBig(const String&);
		void SetPhotoMedium(const String&);
		void SetScreenName(const String&);
		void SetStartDate(time_t);
		void SetWikiPage(const String&);
	};
	
	template<DataType T> Group<T>::Group():
		m_startDate(0), m_endDate(0), m_membersCount(0)
	{
		m_admin = false;
		m_closed = false;
		m_member = false;
	}
	
	template<DataType T> bool Group<T>::Admin() const
	{
		return m_admin;
	}
	
	template<DataType T> bool Group<T>::Closed() const
	{
		return m_closed;
	}
	
	template<DataType T> const typename Group<T>::String &Group<T>::GetCityId() const
	{
		return m_cityId;
	}
	
	template<DataType T> const typename Group<T>::String &Group<T>::GetCountryId() const
	{
		return m_countryId;
	}
	
	template<DataType T> const typename Group<T>::String &Group<T>::GetDescription() const
	{
		return m_description;
	}
	
	template<DataType T> const GroupCounter &Group<T>::GetCounter() const
	{
		return m_counter;
	}
	
	template<DataType T> time_t Group<T>::GetEndDate() const
	{
		return m_endDate;
	}
	
	template<DataType T> const typename Group<T>::String &Group<T>::GetId() const
	{
		return m_id;
	}
	
	template<DataType T> size_t Group<T>::GetMembersCount() const
	{
		return m_membersCount;
	}
	
	template<DataType T> const typename Group<T>::String &Group<T>::GetName() const
	{
		return m_name;
	}
	
	template<DataType T> const typename Group<T>::String &Group<T>::GetPhoto() const
	{
		return m_photo;
	}
	
	template<DataType T> const typename Group<T>::String &Group<T>::GetPhotoBig() const
	{
		return m_photo_big;
	}
	
	template<DataType T> const typename Group<T>::String &Group<T>::GetPhotoMedium() const
	{
		return m_photo_medium;
	}
	
	template<DataType T> const typename Group<T>::String &Group<T>::GetScreenName() const
	{
		return m_screenName;
	}
	
	template<DataType T> time_t Group<T>::GetStartDate() const
	{
		return m_startDate;;
	}
	
	template<DataType T> const typename Group<T>::String &Group<T>::GetWikiPage() const
	{
		return m_wikiPage;
	}
	
	template<DataType T> bool Group<T>::Member() const
	{
		return m_member;
	}
	
	template<DataType T> void Group<T>::SetAdmin(bool isAdmin)
	{
		m_admin = isAdmin;
	}
	
	template<DataType T> void Group<T>::SetCityId(const String &id)
	{
		m_cityId = id;
	}
	
	template<DataType T> void Group<T>::SetClosed(bool isClosed)
	{
		m_closed = isClosed;
	}
	
	template<DataType T> void Group<T>::SetCounter(const GroupCounter &groupCounter)
	{
		m_counter = groupCounter;
	}
	
	template<DataType T> void Group<T>::SetCountryId(const String &id)
	{
		m_countryId = id;
	}
	
	template<DataType T> void Group<T>::SetDescription(const String &description)
	{
		m_description = description;
	}
	
	template<DataType T> void Group<T>::SetEndDate(time_t endDate)
	{
		m_endDate = endDate;
	}
	
	template<DataType T> void Group<T>::SetId(const String &id)
	{
		m_id = id;
	}
	
	template<DataType T> void Group<T>::SetMember(bool isMember)
	{
		m_member = isMember;
	}
	
	template<DataType T> void Group<T>::SetMembersCount(size_t membersCount)
	{
		m_membersCount = membersCount;
	}
	
	template<DataType T> void Group<T>::SetName(const String &name)
	{
		m_name = name;
	}
	
	template<DataType T> void Group<T>::SetPhoto(const String &photo)
	{
		m_photo = photo;
	}
	
	template<DataType T> void Group<T>::SetPhotoBig(const String &photo_big)
	{
		m_photo_big = photo_big;
	}
	
	template<DataType T> void Group<T>::SetPhotoMedium(const String &photo_medium)
	{
		m_photo_medium = photo_medium;
	}
	
	template<DataType T> void Group<T>::SetScreenName(const String &screenName)
	{
		m_screenName = screenName;
	}
	
	template<DataType T> void Group<T>::SetStartDate(time_t startDate)
	{
		m_startDate = startDate;
	}
	
	template<DataType T> void Group<T>::SetWikiPage(const String &wikiPage)
	{
		m_wikiPage = wikiPage;
	}
}
