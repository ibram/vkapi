#pragma once
#include <string>
#include "DataTraits.h"
#include "Counter.h"
#include "Relation.h"
#include "Sex.h"

namespace sonapi
{
	/// \brief Профиль пользователя.
	template<DataType T> class Profile
	{
		typedef typename DataTraits<T>::String String;
		
	private:
		Relation m_relation;
		Sex m_sex;
		UserCounter m_counter;
		String m_birthDate, m_city, m_country, m_faculty, m_facultyName, m_firstName, m_graduation, m_homePhone;
		String m_lastName, m_mobilePhone, m_nickName, m_photo, m_photoMedium, m_photoBig, m_photoRec;
		String m_screenName, m_timezone, m_university, m_universityName, m_userId;
		size_t m_rate;
		bool m_hasMobile;
		bool m_online;

	public:
		~Profile();
		const String &GetBirthDate() const;
		const String &GetCity() const;
		const UserCounter &GetCounter() const;
		const String &GetCountry() const;
		const String &GetFaculty() const;
		const String &GetFacultyName() const;
		const String &GetFirstName() const;
		const String &GetGraduation() const;
		const String &GetHomePhone() const;
		const String &GetLastName() const;
		const String &GetMobilePhone() const;
		const String &GetNickName() const;
		const String &GetPhoto() const;
		const String &GetPhotoBig() const;
		const String &GetPhotoMedium() const;
		const String &GetPhotoRec() const;
		size_t GetRate() const;
		Relation GetRelation() const;
		const String &GetScreenName() const;
		Sex GetSex() const;
		const String &GetTimezone() const;
		const String &GetUniversity() const;
		const String &GetUniversityName() const;
		const String &GetUserId() const;
		bool HasMobile() const;
		bool IsOnline() const;
		void SetBirthDate(const String&);
		void SetCity(const String&);
		void SetCounter(const UserCounter&);
		void SetCountry(const String&);
		void SetFaculty(const String&);
		void SetFacultyName(const String&);
		void SetFirstName(const String&);
		void SetGraduation(const String&);
		void SetHomePhone(const String&);
		void SetLastName(const String&);
		void SetMobileExistence(bool);
		void SetMobilePhone(const String&);
		void SetNickName(const String&);
		void SetOnline(bool);
		void SetPhoto(const String&);
		void SetPhotoBig(const String&);
		void SetPhotoMedium(const String&);
		void SetPhotoRec(const String&);
		void SetRate(size_t);
		void SetRelation(Relation);
		void SetScreenName(const String&);
		void SetSex(Sex);
		void SetTimezone(const String&);
		void SetUniversity(const String&);
		void SetUniversityName(const String&);
		void SetUserId(const String&);
	};
	
	template<DataType T> Profile<T>::~Profile()
	{
	}
	
	template<DataType T> const typename Profile<T>::String &Profile<T>::GetBirthDate() const
	{
		return m_birthDate;
	}
	
	template<DataType T> const typename Profile<T>::String &Profile<T>::GetCity() const
	{
		return m_city;
	}
	
	template<DataType T> const UserCounter &Profile<T>::GetCounter() const
	{
		return m_counter;
	}
	
	template<DataType T> const typename Profile<T>::String &Profile<T>::GetCountry() const
	{
		return m_country;
	}
	
	template<DataType T> const typename Profile<T>::String &Profile<T>::GetFaculty() const
	{
		return m_faculty;
	}
	
	template<DataType T> const typename Profile<T>::String &Profile<T>::GetFacultyName() const
	{
		return m_facultyName;
	}
	
	template<DataType T> const typename Profile<T>::String &Profile<T>::GetFirstName() const
	{
		return m_firstName;
	}
	
	template<DataType T> const typename Profile<T>::String &Profile<T>::GetGraduation() const
	{
		return m_graduation;
	}
	
	template<DataType T> bool Profile<T>::HasMobile() const
	{
		return m_hasMobile;
	}
	
	template<DataType T> const typename Profile<T>::String &Profile<T>::GetHomePhone() const
	{
		return m_homePhone;
	}
	
	template<DataType T> const typename Profile<T>::String &Profile<T>::GetLastName() const
	{
		return m_lastName;
	}
	
	template<DataType T> const typename Profile<T>::String &Profile<T>::GetMobilePhone() const
	{
		return m_mobilePhone;
	}
	
	template<DataType T> const typename Profile<T>::String &Profile<T>::GetNickName() const
	{
		return m_nickName;
	}
	
	template<DataType T> const typename Profile<T>::String &Profile<T>::GetPhoto() const
	{
		return m_photo;
	}
	
	template<DataType T> const typename Profile<T>::String &Profile<T>::GetPhotoBig() const
	{
		return m_photoBig;
	}
	
	template<DataType T> const typename Profile<T>::String &Profile<T>::GetPhotoMedium() const
	{
		return m_photoMedium;
	}
	
	template<DataType T> const typename Profile<T>::String &Profile<T>::GetPhotoRec() const
	{
		return m_photoRec;
	}
	
	template<DataType T> size_t Profile<T>::GetRate() const
	{
		return m_rate;
	}
	
	template<DataType T> Relation Profile<T>::GetRelation() const
	{
		return m_relation;
	}
	
	template<DataType T> const typename Profile<T>::String &Profile<T>::GetScreenName() const
	{
		return m_screenName;
	}
	
	template<DataType T> Sex Profile<T>::GetSex() const
	{
		return m_sex;
	}
	
	template<DataType T> const typename Profile<T>::String &Profile<T>::GetTimezone() const
	{
		return m_timezone;
	}
	
	template<DataType T> const typename Profile<T>::String &Profile<T>::GetUniversity() const
	{
		return m_university;
	}
	
	template<DataType T> const typename Profile<T>::String &Profile<T>::GetUniversityName() const
	{
		return m_universityName;
	}
	
	template<DataType T> const typename Profile<T>::String &Profile<T>::GetUserId() const
	{
		return m_userId;
	}
	
	template<DataType T> bool Profile<T>::IsOnline() const
	{
		return m_online;
	}
	
	template<DataType T> void Profile<T>::SetBirthDate(const String &birthDate)
	{
		m_birthDate = birthDate;
	}
	
	template<DataType T> void Profile<T>::SetCity(const String &city)
	{
		m_city = city;
	}
	
	template<DataType T> void Profile<T>::SetCounter(const UserCounter &counter)
	{
		m_counter = counter;
	}
	
	template<DataType T> void Profile<T>::SetCountry(const String &country)
	{
		m_country = country;
	}
	
	template<DataType T> void Profile<T>::SetFaculty(const String &faculty)
	{
		m_faculty = faculty;
	}
	
	template<DataType T> void Profile<T>::SetFacultyName(const String &facultyName)
	{
		m_facultyName = facultyName;
	}
	
	template<DataType T> void Profile<T>::SetFirstName(const String &firstName)
	{
		m_firstName = firstName;
	}
	
	template<DataType T> void Profile<T>::SetGraduation(const String &graduation)
	{
		m_graduation = graduation;
	}
	
	template<DataType T> void Profile<T>::SetHomePhone(const String &homePhone)
	{
		m_homePhone = homePhone;
	}
	
	template<DataType T> void Profile<T>::SetLastName(const String &lastName)
	{
		m_lastName = lastName;
	}
	
	template<DataType T> void Profile<T>::SetMobileExistence(bool isExist)
	{
		m_hasMobile = isExist;
	}
	
	template<DataType T> void Profile<T>::SetMobilePhone(const String &mobilePhone)
	{
		m_mobilePhone = mobilePhone;
	}
	
	template<DataType T> void Profile<T>::SetNickName(const String &nickName)
	{
		m_nickName = nickName;
	}
	
	template<DataType T> void Profile<T>::SetOnline(bool online)
	{
		m_online = online;
	}
	
	template<DataType T> void Profile<T>::SetPhoto(const String &photo)
	{
		m_photo = photo;
	}
	
	template<DataType T> void Profile<T>::SetPhotoBig(const String &photoBig)
	{
		m_photoBig = photoBig;
	}
	
	template<DataType T> void Profile<T>::SetPhotoMedium(const String &photoMedium)
	{
		m_photoMedium = photoMedium;
	}
	
	template<DataType T> void Profile<T>::SetPhotoRec(const String &photoRec)
	{
		m_photoRec = photoRec;
	}
	
	template<DataType T> void Profile<T>::SetRate(size_t rate)
	{
		m_rate = rate;
	}
	
	template<DataType T> void Profile<T>::SetRelation(Relation relation)
	{
		m_relation = relation;
	}
	
	template<DataType T> void Profile<T>::SetScreenName(const String &screenName)
	{
		m_screenName = screenName;
	}
	
	template<DataType T> void Profile<T>::SetSex(Sex sex)
	{
		m_sex = sex;
	}
	
	template<DataType T> void Profile<T>::SetTimezone(const String &timezone)
	{
		m_timezone = timezone;
	}
	
	template<DataType T> void Profile<T>::SetUniversity(const String &university)
	{
		m_university = university;
	}
	
	template<DataType T> void Profile<T>::SetUniversityName(const String &universityName)
	{
		m_universityName = universityName;
	}
	
	template<DataType T> void Profile<T>::SetUserId(const String &userId)
	{
		m_userId = userId;
	}
}
