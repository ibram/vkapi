#pragma once

namespace sonapi
{
	template<DataType T> class AudioAttachment
	{
		typedef typename DataTraits<T>::String String;

	private:
		String m_audioId, m_ownerId, m_performer, m_title;
		time_t m_duration;

	public:
		const String &GetAudioId() const;
		time_t GetDuration() const;
		const String &GetOwnerId() const;
		const String &GetPerformer() const;
		const String &GetTitle() const;
		void SetAudioId(const String&);
		void SetDuration(time_t);
		void SetOwnerId(const String&);
		void SetPerformer(const String&);
		void SetTitle(const String&);
	};
	
	template<DataType T> const typename AudioAttachment<T>::String &AudioAttachment<T>::GetAudioId() const
	{
		return m_audioId;
	}
	
	template<DataType T> time_t AudioAttachment<T>::GetDuration() const
	{
		return m_duration;
	}
	
	template<DataType T> const typename AudioAttachment<T>::String &AudioAttachment<T>::GetOwnerId() const
	{
		return m_ownerId;
	}
	
	template<DataType T> const typename AudioAttachment<T>::String &AudioAttachment<T>::GetPerformer() const
	{
		return m_performer;
	}
	
	template<DataType T> const typename AudioAttachment<T>::String &AudioAttachment<T>::GetTitle() const
	{
		return m_title;
	}
	
	template<DataType T> void AudioAttachment<T>::SetAudioId(const String &audioId)
	{
		m_audioId = audioId;
	}
	
	template<DataType T> void AudioAttachment<T>::SetDuration(time_t duration)
	{
		m_duration = duration;
	}
	
	template<DataType T> void AudioAttachment<T>::SetOwnerId(const String &ownerId)
	{
		m_ownerId = ownerId;
	}
	
	template<DataType T> void AudioAttachment<T>::SetPerformer(const String &performer)
	{
		m_performer = performer;
	}
	
	template<DataType T> void AudioAttachment<T>::SetTitle(const String &title)
	{
		m_title = title;
	}
}
