#pragma once
#include <cstdlib>
using namespace std;

namespace sonapi
{
	class GroupCounter
	{
	private:
		size_t m_albumsCount, m_audiosCount, m_docsCount, m_photosCount, m_topicsCount, m_videosCount;
		
	public:
		GroupCounter();
		size_t GetAlbumsCount() const;
		size_t GetAudiosCount() const;
		size_t GetDocsCount() const;
		size_t GetPhotosCount() const;
		size_t GetTopicsCount() const;
		size_t GetVideosCount() const;
		void SetAlbumsCount(size_t);
		void SetAudiosCount(size_t);
		void SetDocsCount(size_t);
		void SetPhotosCount(size_t);
		void SetTopicsCount(size_t);
		void SetVideosCount(size_t);
	};
}
