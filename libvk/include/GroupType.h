#pragma once

namespace sonapi
{
	enum GroupType
	{
		GroupType_Admin,
		GroupType_All,
		GroupType_Event,
		GroupType_Group,
		GroupType_Public
	};
}