#pragma once
#include "TagsInfo.h"

namespace sonapi
{
	template<DataType T> class Photo
	{
		typedef typename DataTraits<T>::String String;

	private:
		String m_albumId, m_bigImageURL, m_id, m_ownerId, m_previewURL, m_text, m_url, m_xBigImageUrl, m_xxBigImageUrl;
		CommentsInfo m_commentInfo;
		LikesInfo m_likesInfo;
		TagsInfo m_tagsInfo;
		time_t m_createdTime;

	public:
		const String &GetAlbumId() const;
		const String &GetBigImageURL() const;
		const CommentsInfo &GetCommentInfo() const;
		time_t GetCreatedTime() const;
		const String &GetId() const;
		const LikesInfo &GetLikesInfo() const;
		const String &GetOwnerId() const;
		const String &GetPreviewURL() const;
		const TagsInfo &GetTagsInfo() const;
		const String &GetText() const;
		const String &GetUrl() const;
		const String &GetXBigImageUrl() const;
		const String &GetXxBigImageUrl() const;
		void SetAlbumId(const String&);
		void SetBigImageURL(const String&);
		void SetCommentInfo(const CommentsInfo&);
		void SetCreatedTime(time_t);
		void SetId(const String&);
		void SetLikesInfo(const LikesInfo&);
		void SetOwnerId(const String&);
		void SetPreviewURL(const String&);
		void SetTagsInfo(const TagsInfo&);
		void SetText(const String&);
		void SetUrl(const String&);
		void SetXBigImageUrl(const String&);
		void SetXxBigImageUrl(const String&);
	};

	template<DataType T> const typename Photo<T>::String &Photo<T>::GetAlbumId() const
	{
		return m_albumId;
	}

	template<DataType T> const typename Photo<T>::String &Photo<T>::GetBigImageURL() const
	{
		return m_bigImageURL;
	}

	template<DataType T> const CommentsInfo &Photo<T>::GetCommentInfo() const
	{
		return m_commentInfo;
	}

	template<DataType T> time_t Photo<T>::GetCreatedTime() const
	{
		return m_createdTime;
	}

	template<DataType T> const typename Photo<T>::String &Photo<T>::GetId() const
	{
		return m_id;
	}

	template<DataType T> const LikesInfo &Photo<T>::GetLikesInfo() const
	{
		return m_likesInfo;
	}

	template<DataType T> const typename Photo<T>::String &Photo<T>::GetOwnerId() const
	{
		return m_ownerId;
	}

	template<DataType T> const typename Photo<T>::String &Photo<T>::GetPreviewURL() const
	{
		return m_previewURL;
	}

	template<DataType T> const TagsInfo &Photo<T>::GetTagsInfo() const
	{
		return m_tagsInfo;
	}

	template<DataType T> const typename Photo<T>::String &Photo<T>::GetText() const
	{
		return m_text;
	}

	template<DataType T> const typename Photo<T>::String &Photo<T>::GetUrl() const
	{
		return m_url;
	}

	template<DataType T> const typename Photo<T>::String &Photo<T>::GetXBigImageUrl() const
	{
		return m_xBigImageUrl;
	}
	
	template<DataType T> const typename Photo<T>::String &Photo<T>::GetXxBigImageUrl() const
	{
		return m_xxBigImageUrl;
	}
	
	template<DataType T> void Photo<T>::SetAlbumId(const String &albumId)
	{
		m_albumId = albumId;
	}

	template<DataType T> void Photo<T>::SetBigImageURL(const String &bigImageURL)
	{
		m_bigImageURL = bigImageURL;
	}

	template<DataType T> void Photo<T>::SetCommentInfo(const CommentsInfo &commentInfo)
	{
		m_commentInfo = commentInfo;
	}

	template<DataType T> void Photo<T>::SetCreatedTime(time_t createdTime)
	{
		m_createdTime = createdTime;
	}

	template<DataType T> void Photo<T>::SetId(const String &id)
	{
		m_id = id;
	}

	template<DataType T> void Photo<T>::SetLikesInfo(const LikesInfo &likesInfo)
	{
		m_likesInfo = likesInfo;
	}

	template<DataType T> void Photo<T>::SetOwnerId(const String &ownerId)
	{
		m_ownerId = ownerId;
	}

	template<DataType T> void Photo<T>::SetPreviewURL(const String &previewURL)
	{
		m_previewURL = previewURL;
	}

	template<DataType T> void Photo<T>::SetTagsInfo(const TagsInfo &tagsInfo)
	{
		m_tagsInfo = tagsInfo;
	}

	template<DataType T> void Photo<T>::SetText(const String &text)
	{
		m_text = text;
	}

	template<DataType T> void Photo<T>::SetUrl(const String &url)
	{
		m_url = url;
	}
	
	template<DataType T> void Photo<T>::SetXBigImageUrl(const String &xBigImageUrl)
	{
		m_xBigImageUrl = xBigImageUrl;
	}
	
	template<DataType T> void Photo<T>::SetXxBigImageUrl(const String &xxBigImageUrl)
	{
		m_xxBigImageUrl = xxBigImageUrl;
	}
}
