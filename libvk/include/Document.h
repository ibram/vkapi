#pragma once

namespace sonapi
{
	template<DataType T> class Document
	{
		typedef typename DataTraits<T>::String String;

	private:
		String m_fileExtension, m_id, m_ownerId, m_title, m_url;
		long m_fileSize;

	public:
		const String &GetFileExtension() const;
		long GetFileSize() const;
		const String &GetId() const;
		const String &GetOwnerId() const;
		const String &GetTitle() const;
		const String &GetUrl() const;
		void SetFileExtension(const String&);
		void SetFileSize(long);
		void SetId(const String&);
		void SetOwnerId(const String&);
		void SetTitle(const String&);
		void SetUrl(const String&);
		
	};
	
	template<DataType T> const typename Document<T>::String &Document<T>::GetFileExtension() const
	{
		return m_fileExtension;
	}
	
	template<DataType T> long Document<T>::GetFileSize() const
	{
		return m_fileSize;
	}
	
	template<DataType T> const typename Document<T>::String &Document<T>::GetId() const
	{
		return m_id;
	}
	
	template<DataType T> const typename Document<T>::String &Document<T>::GetOwnerId() const
	{
		return m_ownerId;
	}
	
	template<DataType T> const typename Document<T>::String &Document<T>::GetTitle() const
	{
		return m_title;
	}
	
	template<DataType T> const typename Document<T>::String &Document<T>::GetUrl() const
	{
		return m_url;
	}
	
	template<DataType T> void Document<T>::SetFileExtension(const String &fileExtension)
	{
		m_fileExtension = fileExtension;
	}
	
	template<DataType T> void Document<T>::SetFileSize(long fileSize)
	{
		m_fileSize = fileSize;
	}
	
	template<DataType T> void Document<T>::SetId(const String &id)
	{
		m_id = id;
	}
	
	template<DataType T> void Document<T>::SetOwnerId(const String &ownerId)
	{
		m_ownerId = ownerId;
	}
	
	template<DataType T> void Document<T>::SetTitle(const String &title)
	{
		m_title = title;
	}
	
	template<DataType T> void Document<T>::SetUrl(const String &url)
	{
		m_url = url;
	}
}
