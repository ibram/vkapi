#pragma once

namespace sonapi
{
	template<DataType T> class Post
	{
		typedef typename DataTraits<T>::String String;

	private:
		vector<Attachment<T> > m_attachments;
		Place<T> m_place;
		CommentsInfo m_comments;
		LikesInfo m_likes;
		RepostsInfo m_reposts;
		String m_copyOwnerId, m_copyPostId, m_copyText, m_fromId, m_id, m_signedId, m_text, m_toId;
		time_t m_date;

	public:
		const vector<Attachment<T> > &GetAttachments() const;
		const CommentsInfo &GetComments() const;
		const String &GetCopyOwnerId() const;
		const String &GetCopyPostId() const;
		const String &GetCopyText() const;
		time_t GetDate() const;
		const String &GetFromId() const;
		const String &GetId() const;
		const LikesInfo &GetLikes() const;
		const Place<T>& GetPlace() const;
		const RepostsInfo &GetReposts() const;
		const String &GetSignedId() const;
		const String &GetText() const;
		const String &GetToId() const;
		void SetAttachments(const vector<Attachment<T> >&);
		void SetComments(const CommentsInfo&);
		void SetCopyOwnerId(const String&);
		void SetCopyPostId(const String&);
		void SetCopyText(const String&);
		void SetDate(time_t);
		void SetFromId(const String&);
		void SetId(const String&);
		void SetLikes(const LikesInfo&);
		void SetPlace(const Place<T>& );
		void SetReposts(const RepostsInfo&);
		void SetSignedId(const String&);
		void SetText(const String&);
		void SetToId(const String&);
	};
	
	template<DataType T> const vector<Attachment<T> > &Post<T>::GetAttachments() const
	{
		return m_attachments;
	}
	
	template<DataType T> const CommentsInfo &Post<T>::GetComments() const
	{
		return m_comments;
	}
	
	template<DataType T> const typename Post<T>::String &Post<T>::GetCopyOwnerId() const
	{
		return m_copyOwnerId;
	}
	
	template<DataType T> const typename Post<T>::String &Post<T>::GetCopyPostId() const
	{
		return m_copyPostId;
	}
	
	template<DataType T> const typename Post<T>::String &Post<T>::GetCopyText() const
	{
		return m_copyText;
	}
	
	template<DataType T> time_t Post<T>::GetDate() const
	{
		return m_date;
	}
	
	template<DataType T> const typename Post<T>::String &Post<T>::GetFromId() const
	{
		return m_fromId;
	}
	
	template<DataType T> const typename Post<T>::String &Post<T>::GetId() const
	{
		return m_id;
	}
	
	template<DataType T> const LikesInfo &Post<T>::GetLikes() const
	{
		return m_likes;
	}
	
	template<DataType T> const Place<T> &Post<T>::GetPlace() const
	{
		return m_place;
	}
	
	template<DataType T> const RepostsInfo &Post<T>::GetReposts() const
	{
		return m_reposts;
	}
	
	template<DataType T> const typename Post<T>::String &Post<T>::GetSignedId() const
	{
		return m_signedId;
	}
	
	template<DataType T> const typename Post<T>::String &Post<T>::GetText() const
	{
		return m_text;
	}
	
	template<DataType T> const typename Post<T>::String &Post<T>::GetToId() const
	{
		return m_toId;
	}
	
	template<DataType T> void Post<T>::SetAttachments(const vector<Attachment<T> >& attachments)
	{
		m_attachments = attachments;
	}
	
	template<DataType T> void Post<T>::SetComments(const CommentsInfo &comments)
	{
		m_comments = comments;
	}
	
	template<DataType T> void Post<T>::SetCopyOwnerId(const String &copyOwnerId)
	{
		m_copyOwnerId = copyOwnerId;
	}
	
	template<DataType T> void Post<T>::SetCopyPostId(const String &copyPostId)
	{
		m_copyPostId = copyPostId;
	}
	
	template<DataType T> void Post<T>::SetCopyText(const String &copyText)
	{
		m_copyText = copyText;
	}
	
	template<DataType T> void Post<T>::SetDate(time_t date)
	{
		m_date = date;
	}
	
	template<DataType T> void Post<T>::SetFromId(const String &fromId)
	{
		m_fromId = fromId;
	}
	
	template<DataType T> void Post<T>::SetId(const String &id)
	{
		m_id = id;
	}
	
	template<DataType T> void Post<T>::SetLikes(const LikesInfo &likes)
	{
		m_likes = likes;
	}
	
	template<DataType T> void Post<T>::SetPlace(const Place<T>& place)
	{
		m_place = place;
	}
	
	template<DataType T> void Post<T>::SetReposts(const RepostsInfo &reposts)
	{
		m_reposts = reposts;
	}
	
	template<DataType T> void Post<T>::SetSignedId(const String &signedId)
	{
		m_signedId = signedId;
	}
	
	template<DataType T> void Post<T>::SetText(const String &text)
	{
		m_text = text;
	}
	
	template<DataType T> void Post<T>::SetToId(const String &toId)
	{
		m_toId = toId;
	}
}
