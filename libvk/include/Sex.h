#pragma once

namespace sonapi
{
	/// \brief Пол.
	enum Sex
	{
		Sex_None, ///< Нету.
		Sex_Female, ///< Женский.
		Sex_Male ///< Мужской.
	};
}