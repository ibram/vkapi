#pragma once

namespace sonapi
{
	template<DataType T> class Video
	{
		typedef typename DataTraits<T>::String String;

	private:
		String m_description, m_id, m_imageUrl, m_link, m_ownerId, m_playerUrl, m_thumbUrl, m_title;
		time_t m_date, m_duration;

	public:
		time_t GetDate() const;
		const String &GetDescription() const;
		time_t GetDuration() const;
		const String &GetId() const;
		const String &GetImageUrl() const;
		const String &GetLink() const;
		const String &GetOwnerId() const;
		const String &GetPlayerUrl() const;
		const String &GetThumbUrl() const;
		const String &GetTitle() const;
		void SetDate(time_t date);
		void SetDescription(const String&);
		void SetDuration(time_t duration);
		void SetId(const String&);
		void SetImageUrl(const String&);
		void SetLink(const String&);
		void SetOwnerId(const String&);
		void SetPlayerUrl(const String&);
		void SetThumbUrl(const String&);
		void SetTitle(const String&);
	};

	template<DataType T> time_t Video<T>::GetDate() const
	{
		return m_date;
	}
	
	template<DataType T> const typename Video<T>::String &Video<T>::GetDescription() const
	{
		return m_description;
	}
	
	template<DataType T> time_t Video<T>::GetDuration() const
	{
		return m_duration;
	}
	
	template<DataType T> const typename Video<T>::String &Video<T>::GetId() const
	{
		return m_id;
	}
	
	template<DataType T> const typename Video<T>::String &Video<T>::GetImageUrl() const
	{
		return m_imageUrl;
	}
	
	template<DataType T> const typename Video<T>::String &Video<T>::GetLink() const
	{
		return m_link;
	}
	
	template<DataType T> const typename Video<T>::String &Video<T>::GetOwnerId() const
	{
		return m_ownerId;
	}
	
	template<DataType T> const typename Video<T>::String &Video<T>::GetPlayerUrl() const
	{
		return m_playerUrl;
	}
	
	template<DataType T> const typename Video<T>::String &Video<T>::GetThumbUrl() const
	{
		return m_thumbUrl;
	}
	
	template<DataType T> const typename Video<T>::String &Video<T>::GetTitle() const
	{
		return m_title;
	}
	
	template<DataType T> void Video<T>::SetDate(time_t date)
	{
		m_date = date;
	}
	
	template<DataType T> void Video<T>::SetDescription(const String &description)
	{
		m_description = description;
	}
	
	template<DataType T> void Video<T>::SetDuration(time_t duration)
	{
		m_duration = duration;
	}
	
	template<DataType T> void Video<T>::SetId(const String &id)
	{
		m_id = id;
	}
	
	template<DataType T> void Video<T>::SetImageUrl(const String &imageUrl)
	{
		m_imageUrl = imageUrl;
	}
	
	template<DataType T> void Video<T>::SetLink(const String &link)
	{
		m_link = link;
	}
	
	template<DataType T> void Video<T>::SetOwnerId(const String &ownerId)
	{
		m_ownerId = ownerId;
	}
	
	template<DataType T> void Video<T>::SetPlayerUrl(const String &playerUrl)
	{
		m_playerUrl = playerUrl;
	}
	
	template<DataType T> void Video<T>::SetThumbUrl(const String &thumbUrl)
	{
		m_thumbUrl = thumbUrl;
	}
	
	template<DataType T> void Video<T>::SetTitle(const String &title)
	{
		m_title = title;
	}
}
