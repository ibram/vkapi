#pragma once
#include "AccessPrivacy.h"

namespace sonapi
{
	template<DataType T> class Album
	{
		typedef typename DataTraits<T>::String String;

	private:
		String m_description, m_id, m_ownerId, m_thumbId, m_thumbUrl, m_title;
		time_t m_createdTime, m_updatedTime;
		size_t m_size;
		AccessPrivacy m_privacyLevel;

	public:
		time_t GetCreatedTime() const;
		const String &GetDescription() const;
		const String &GetId() const;
		const String &GetOwnerId() const;
		AccessPrivacy GetPrivacyLevel() const;
		size_t GetSize() const;
		const String &GetThumbId() const;
		const String &GetThumbUrl() const;
		const String &GetTitle() const;
		time_t GetUpdatedTime() const;
		void SetCreatedTime(time_t);
		void SetDescription(const String&);
		void SetId(const String&);
		void SetOwnerId(const String&);
		void SetPrivacyLevel(AccessPrivacy);
		void SetSize(size_t);
		void SetThumbId(const String&);
		void SetThumbUrl(const String&);
		void SetTitle(const String&);
		void SetUpdatedTime(time_t);
	};
	
	template<DataType T> time_t Album<T>::GetCreatedTime() const
	{
		return m_createdTime;
	}
	
	template<DataType T> const typename Album<T>::String &Album<T>::GetDescription() const
	{
		return m_description;
	}
	
	template<DataType T> const typename Album<T>::String &Album<T>::GetId() const
	{
		return m_id;
	}
	
	template<DataType T> const typename Album<T>::String &Album<T>::GetOwnerId() const
	{
		return m_ownerId;
	}
	
	template<DataType T> AccessPrivacy Album<T>::GetPrivacyLevel() const
	{
		return m_privacyLevel;
	}
	
	template<DataType T> size_t Album<T>::GetSize() const
	{
		return m_size;
	}
	
	template<DataType T> const typename Album<T>::String &Album<T>::GetThumbId() const
	{
		return m_thumbId;
	}
	
	template<DataType T> const typename Album<T>::String &Album<T>::GetThumbUrl() const
	{
		return m_thumbUrl;
	}
	
	template<DataType T> const typename Album<T>::String &Album<T>::GetTitle() const
	{
		return m_title;
	}
	
	template<DataType T> time_t Album<T>::GetUpdatedTime() const
	{
		return m_updatedTime;
	}
	
	template<DataType T> void Album<T>::SetCreatedTime(time_t createdTime)
	{
		m_createdTime = createdTime;
	}
	
	template<DataType T> void Album<T>::SetDescription(const String &description)
	{
		m_description = description;
	}
	
	template<DataType T> void Album<T>::SetId(const String &id)
	{
		m_id = id;
	}
	
	template<DataType T> void Album<T>::SetOwnerId(const String &ownerId)
	{
		m_ownerId = ownerId;
	}
	
	template<DataType T> void Album<T>::SetPrivacyLevel(AccessPrivacy privacyLevel)
	{
		m_privacyLevel = privacyLevel;
	}
	
	template<DataType T> void Album<T>::SetSize(size_t size)
	{
		m_size = size;
	}
	
	template<DataType T> void Album<T>::SetThumbId(const String &thumbId)
	{
		m_thumbId = thumbId;
	}
	
	template<DataType T> void Album<T>::SetThumbUrl(const String &thumbUrl)
	{
		m_thumbUrl = thumbUrl;
	}
	
	template<DataType T> void Album<T>::SetTitle(const String &title)
	{
		m_title = title;
	}
	
	template<DataType T> void Album<T>::SetUpdatedTime(time_t updatedTime)
	{
		m_updatedTime = updatedTime;
	}
}
