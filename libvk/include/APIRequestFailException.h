﻿#pragma once
#include "BaseException.h"

namespace sonapi
{
	class APIRequestFailException: public BaseException
	{
	public:
		APIRequestFailException(int, const char*);
	};
}
