#pragma once

namespace sonapi
{
	enum Service
	{
		Service_None = 1,
		Service_Facebook,
		Service_Twitter = 4,
		Service_All = Service_Facebook | Service_Twitter
	};
}