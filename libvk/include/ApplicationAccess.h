#pragma once

namespace sonapi
{
	/// \brief Права доступа приложений.
	enum ApplicationAccess
	{
		ApplicationAccess_Notify = 1, ///< Доступ к отправке пользователю уведомлений.
		ApplicationAccess_Friends, ///< Доступ к друзьям.
		ApplicationAccess_Photos = 4, ///< Доступ к фотографиям.
		ApplicationAccess_Audio = 8, ///< Доступ к аудиозаписям.
		ApplicationAccess_Video = 16, ///< Доступ к видеозаписям.
		ApplicationAccess_Offers = 32, ///< Доступ к предложениям.
		ApplicationAccess_Questions = 64, ///< Доступ к вопросам.
		ApplicationAccess_Pages = 128, ///< Доступ к wiki-страницам.
		ApplicationAccess_LinkToApp = 256, ///< Доcтуп к добавлению ссылки на приложение в меню слева.
		ApplicationAccess_FastPublishingLink = 512, ///< Доступ к добавлению ссылки на приложение для быстрой публикации на стенах пользователей.
		ApplicationAccess_Statuses = 1024, ///< Доступ к статусам.
		ApplicationAccess_Notes = 2048, ///< Доступ к заметкам.
		ApplicationAccess_Messages = 4096, ///< Доступ к расширенным методам работы с сообщениями.
		ApplicationAccess_Wall = 8192, ///< Доступ к методам работы со стеной.
		ApplicationAccess_Ads = 32678, ///< Доступ к методам работы с рекламным API.
		ApplicationAccess_Docs = 131072, ///< Доступ к документам.
		ApplicationAccess_Groups = 262144, ///< Доступ к группам.
		ApplicationAccess_NotifyAboutAnswers = 524288, ///< Доступ к оповещениям об ответах пользователю.
		ApplicationAccess_FullAccess = ApplicationAccess_Notify | ApplicationAccess_Friends | ApplicationAccess_Photos |
			ApplicationAccess_Audio | ApplicationAccess_Video | ApplicationAccess_Offers | ApplicationAccess_Questions |
			ApplicationAccess_Pages | ApplicationAccess_LinkToApp | ApplicationAccess_FastPublishingLink | ApplicationAccess_Statuses |
			ApplicationAccess_Notes | ApplicationAccess_Messages | ApplicationAccess_Wall | ApplicationAccess_Ads |
			ApplicationAccess_Docs | ApplicationAccess_Groups | ApplicationAccess_NotifyAboutAnswers ///< Полный доступ.
	};
}