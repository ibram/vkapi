#pragma once
#include <Converter.h>
#include <memory>
#include <rapidxml.hpp>
#include <vector>
#include "Album.h"
#include "APIRequestFailException.h"
#include "ApplicationAttachment.h"
#include "Attachment.h"
#include "Audio.h"
#include "AutoPointer.h"
#include "MediaAlbum.h"
#include "AudioAttachment.h"
#include "CaptchaNeedException.h"
#include "Comment.h"
#include "Counter.h"
#include "Document.h"
#include "GraffitiAttachment.h"
#include "Group.h"
#include "LinkAttachment.h"
#include "NewsFeed.h"
#include "Note.h"
#include "Photo.h"
#include "PhotoAttachment.h"
#include "Place.h"
#include "PollAttachment.h"
#include "Post.h"
#include "Profile.h"
#include "Status.h"
#include "ResponseParsingException.h"
#include "Video.h"
#include "VideoAttachment.h"
#include "VideoTag.h"
#include "WikiPageAttachment.h"
using namespace rapidxml;
using namespace sonapi;
using namespace std;

namespace sonapi
{
	template<DataType T> class ResponseParser
	{
		typedef typename DataTraits<T>::Char Char;
		typedef typename DataTraits<T>::String String;
	
	private:
		xml_document<char> m_xmlDoc;
		AutoPointer<char> m_xmlBuffer;
		AutoPointer<const Char> m_memoryHandler;
		
		const xml_node<char> *FindSpecifiedNode(const xml_node<char>*, const char*);
		const ApplicationAttachment<T> *ParseApplicationAttachment(const xml_node<char>*);
		const Attachment<T> *ParseAttachment(const xml_node<char>*);
		const vector<Attachment<T> > *ParseAttachments(const xml_node<char>*);
		const MediaAlbum<T> *ParseMediaAlbum(const xml_node<char>*);
		const AudioAttachment<T> *ParseAudioAttachment(const xml_node<char>*);
		const CommentsInfo *ParseCommentsInfo(const xml_node<char>*);
		const GraffitiAttachment<T> *ParseGraffitiAttachment(const xml_node<char>*);
		const Group<T> *ParseGroup(const xml_node<char>*);
		const LikesInfo *ParseLikesInfo(const xml_node<char>*);
		const LinkAttachment<T> *ParseLinkAttachment(const xml_node<char>*);
		const PhotoAttachment<T> *ParsePhotoAttachment(const xml_node<char>*);
		const Place<T> *ParsePlace(const xml_node<char>*);
		const PollAttachment<T> *ParsePollAttachment(const xml_node<char>*);
		const Post<T> *ParsePost(const xml_node<char>*);
		const Profile<T> *ParseProfile(const xml_node<char>*);
		const RepostsInfo *ParseRepostsInfo(const xml_node<char>*);
		const Status<T> *ParseStatus(const xml_node<char>*);
		const TagsInfo *ParseTagsInfo(const xml_node<char>*);
		const VideoAttachment<T> *ParseVideoAttachment(const xml_node<char>*);
		const VideoTag<T> *ParseVideoTag(const xml_node<char>*);
		const WikiPageAttachment<T> *ParseWikiPageAttachment(const xml_node<char>*);
		
	public:
		ResponseParser();
		ResponseParser(const String&);
		
		const Album<T> *ParseAlbum(const xml_node<char>*);
		const vector<Album<T> > *ParseAlbums();
		const Audio<T>  *ParseAudio(const xml_node<char>*);
		const vector<MediaAlbum<T> > *ParseMediaAlbums();
		const vector<Audio<T> > *ParseAudios();
		bool ParseBoolean();
		const Comment<T> *ParseComment(const xml_node<char>*);
		const vector<Comment<T> > *ParseComments();
		const Document<T> *ParseDocument(const xml_node<char>*);
		const vector<Document<T> > *ParseDocuments();
		const GroupCounter *ParseGroupCounter(const xml_node<char>*);
		const vector<Group<T> > *ParseGroups();
		const String &ParseNestedString();
		size_t ParseNestedUInt();
		const vector<News<T> > *ParseNews();
		const NewsFeed<T> *ParseNewsFeed();
		const Note<T> *ParseNote(const xml_node<char>*);
		const vector<Note<T> > *ParseNotes();
		const Photo<T> *ParsePhoto(const xml_node<char>*);
		const vector<Photo<T> > *ParsePhotos();
		const vector<Post<T> > *ParsePosts();
		const vector<Profile<T> > *ParseProfiles();
		const vector<Status<T> > *ParseStatuses();
		const String *ParseString(const char *nodeName);
		const vector<String> *ParseStringArray();
		const UserCounter *ParseUserCounter(const xml_node<char>*);
		const Video<T> *ParseVideo(const xml_node<char>*);
		const vector<Video<T> > *ParseVideos();
		const vector<VideoTag<T> > *ParseVideoTags();
		void ProcessAPIError();
		void SetBuffer(const String&);
	};
	
	template<DataType T> ResponseParser<T>::ResponseParser()
	{
		
	}
	
	template<DataType T> ResponseParser<T>::ResponseParser(const String &xmlBuffer):
		m_xmlBuffer(Converter<T>::StringToCharArray(xmlBuffer)), m_memoryHandler(NULL)
	{
		try
		{
			if (!xmlBuffer.empty())
				m_xmlDoc.parse<parse_default>(m_xmlBuffer.Get());
		}
		catch(parse_error &exc)
		{
			ResponseParsingException parsingException(-1, exc.what());
			throw parsingException;
		}
	}
	
	template<DataType T> const xml_node<char> *ResponseParser<T>::FindSpecifiedNode(const xml_node<char> *rootNode, const char *nodeName)
	{
		xml_node<char> *mainNode = rootNode->first_node();
		
		while(mainNode != NULL)
		{
			const xml_node<char> *foundNode = FindSpecifiedNode(mainNode, nodeName);
			
			if (foundNode != NULL)
				return foundNode;
			else if (strcmp(mainNode->name(), nodeName) == 0)
				return mainNode;
			
			mainNode = mainNode->next_sibling();
		}
		
		return NULL;
	}
	
	template<DataType T> const Album<T> *ResponseParser<T>::ParseAlbum(const xml_node<char> *albumNode)
	{
		if (albumNode == NULL)
			albumNode = FindSpecifiedNode(m_xmlDoc.first_node(), "album");
		
		Album<T> *album = new Album<T>();
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(albumNode->first_node("aid")->value(), albumNode->first_node("aid")->value_size()));
		album->SetId(m_memoryHandler.Get());
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(albumNode->first_node("thumb_id")->value(), albumNode->first_node("thumb_id")->value_size()));
		album->SetThumbId(m_memoryHandler.Get());
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(albumNode->first_node("owner_id")->value(), albumNode->first_node("owner_id")->value_size()));
		album->SetOwnerId(m_memoryHandler.Get());
		
		if (albumNode->first_node("title")->value_size() > 0)
		{
			m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(albumNode->first_node("title")->value(), albumNode->first_node("title")->value_size()));
			album->SetTitle(m_memoryHandler.Get());
		}
		
		if (albumNode->first_node("description")->value_size() > 0)
		{
			m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(albumNode->first_node("description")->value(), albumNode->first_node("description")->value_size()));
			album->SetDescription(m_memoryHandler.Get());
		}
		
		if (albumNode->first_node("thumb_src") != NULL)
		{
			m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(albumNode->first_node("thumb_src")->value(), albumNode->first_node("thumb_src")->value_size()));
			album->SetThumbUrl(m_memoryHandler.Get());
		}
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(albumNode->first_node("created")->value(), albumNode->first_node("created")->value_size()));
		album->SetCreatedTime(Converter<T>::CharArrayToUnixTime(m_memoryHandler.Get()));
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(albumNode->first_node("updated")->value(), albumNode->first_node("updated")->value_size()));
		album->SetUpdatedTime(Converter<T>::CharArrayToUnixTime(m_memoryHandler.Get()));
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(albumNode->first_node("size")->value(), albumNode->first_node("size")->value_size()));
		album->SetSize(DataTraits<T>::CharArrayToInt(m_memoryHandler.Get()));
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(albumNode->first_node("privacy")->value(), albumNode->first_node("privacy")->value_size()));
		album->SetPrivacyLevel(static_cast<AccessPrivacy>(DataTraits<T>::CharArrayToInt(m_memoryHandler.Get())));
		
		return album;
	}
	
	template<DataType T> const vector<Album<T> > *ResponseParser<T>::ParseAlbums()
	{
		vector<Album<T> > *albums = new vector<Album<T> >();
		
		const xml_node<char> *albumNode = FindSpecifiedNode(m_xmlDoc.first_node(), "album");
		
		while (albumNode != NULL)
		{
			const Album<T> *album = ParseAlbum(albumNode);
			albums->push_back(*album);
			delete album;
			
			albumNode = albumNode->next_sibling();
		}
		
		return albums;
	}
	
	template<DataType T> const ApplicationAttachment<T> *ResponseParser<T>::ParseApplicationAttachment(const xml_node<char> *node)
	{
		ApplicationAttachment<T> *attachment = new ApplicationAttachment<T>();
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(node->first_node("app_id")->value(), node->first_node("app_id")->value_size()));
		attachment->SetId(m_memoryHandler.Get());
		
		if (node->first_node("app_name")->value_size() > 0)
		{
			m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(node->first_node("app_name")->value(), node->first_node("app_name")->value_size()));
			attachment->SetName(m_memoryHandler.Get());
		}
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(node->first_node("src")->value(), node->first_node("src")->value_size()));
		attachment->SetPreviewImageURL(m_memoryHandler.Get());
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(node->first_node("src_big")->value(), node->first_node("src_big")->value_size()));
		attachment->SetImageURL(m_memoryHandler.Get());
		
		return attachment;
	}
	
	template<DataType T> const Attachment<T> *ResponseParser<T>::ParseAttachment(const xml_node<char> *attachmentNode)
	{
		Attachment<T> *attachment = new Attachment<T>();
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(attachmentNode->first_node("type")->value(), attachmentNode->first_node("type")->value_size()));

		if (DataTraits<T>::CompareStrings(m_memoryHandler.Get(), TEXT("photo")) == 0)
		{
			attachment->SetType(AttachmentType_Photo);
			attachment->SetPhoto(ParsePhotoAttachment(attachmentNode->first_node("type")->next_sibling()));
		}
		else if (DataTraits<T>::CompareStrings(m_memoryHandler.Get(), TEXT("posted_photo")) == 0)
		{
			attachment->SetType(AttachmentType_PostedPhoto);
			attachment->SetPhoto(ParsePhotoAttachment(attachmentNode->first_node("type")->next_sibling()));
		}
		else if (DataTraits<T>::CompareStrings(m_memoryHandler.Get(), TEXT("video")) == 0)
		{
			attachment->SetType(AttachmentType_Video);
			attachment->SetVideo(ParseVideoAttachment(attachmentNode->first_node("type")->next_sibling()));
		}
		else if (DataTraits<T>::CompareStrings(m_memoryHandler.Get(), TEXT("audio")) == 0)
		{
			attachment->SetType(AttachmentType_Audio);
			attachment->SetAudio(ParseAudioAttachment(attachmentNode->first_node("type")->next_sibling()));
		}
		else if (DataTraits<T>::CompareStrings(m_memoryHandler.Get(), TEXT("doc")) == 0)
		{
			attachment->SetType(AttachmentType_Document);
			attachment->SetDoc(ParseDocument(attachmentNode->first_node("type")->next_sibling()));
		}
		else if (DataTraits<T>::CompareStrings(m_memoryHandler.Get(), TEXT("graffiti")) == 0)
		{
			attachment->SetType(AttachmentType_Graffiti);
			attachment->SetGraffiti(ParseGraffitiAttachment(attachmentNode->first_node("type")->next_sibling()));
		}
		else if (DataTraits<T>::CompareStrings(m_memoryHandler.Get(), TEXT("link")) == 0)
		{
			attachment->SetType(AttachmentType_Link);
			attachment->SetLink(ParseLinkAttachment(attachmentNode->first_node("type")->next_sibling()));
		}
		else if (DataTraits<T>::CompareStrings(m_memoryHandler.Get(), TEXT("note")) == 0)
		{
			attachment->SetType(AttachmentType_Note);
			attachment->SetNote(ParseNote(attachmentNode->first_node("type")->next_sibling()));
		}
		else if (DataTraits<T>::CompareStrings(m_memoryHandler.Get(), TEXT("app")) == 0)
		{
			attachment->SetType(AttachmentType_App);
			attachment->SetApp(ParseApplicationAttachment(attachmentNode->first_node("type")->next_sibling()));
		}
		else if (DataTraits<T>::CompareStrings(m_memoryHandler.Get(), TEXT("poll")) == 0)
		{
			attachment->SetType(AttachmentType_Poll);
			attachment->SetPoll(ParsePollAttachment(attachmentNode->first_node("type")->next_sibling()));
		}
		else if (DataTraits<T>::CompareStrings(m_memoryHandler.Get(), TEXT("page")) == 0)
		{
			attachment->SetType(AttachmentType_WikiPage);
			attachment->SetWikiPage(ParseWikiPageAttachment(attachmentNode->first_node("type")->next_sibling()));
		}
		
		return attachment;
	}
	
	template<DataType T> const vector<Attachment<T> > *ResponseParser<T>::ParseAttachments(const xml_node<char> *attsNode)
	{
		vector<Attachment<T> > *attachments = new vector<Attachment<T> >();
		
		xml_node<char> *node = attsNode->first_node();
		
		while(node != NULL)
		{
			const Attachment<T> *attachment = ParseAttachment(node);
			attachments->push_back(*attachment);
			delete attachment;
			node = node->next_sibling();
		}
			
		return attachments;
	}
	
	template<DataType T> void ResponseParser<T>::ProcessAPIError()
	{
		xml_node<char> *firstNode = m_xmlDoc.first_node();
		
		if (strcmp(firstNode->name(), "error") == 0)
		{
			if (strcmp(firstNode->first_node()->name(), "captcha_img") == 0)
			{
				// TODO : Реализовать извлечение sid, url и указать их ниже
				CaptchaNeedException captchaNeedExc(-1, "Request returned CAPTCHA");
				throw captchaNeedExc;
			}

			char *nodeValue = firstNode->first_node()->value();
			xml_node<char> *nextNode = firstNode->first_node()->next_sibling();
			
			APIRequestFailException exc(atoi(nodeValue), nextNode->value());
			throw exc;
		}
	}
	
	template<DataType T> const Audio<T> *ResponseParser<T>::ParseAudio(const xml_node<char> *node)
	{
		if (node == NULL)
			node = FindSpecifiedNode(m_xmlDoc.first_node(), "audio");
		
		Audio<T> *audio = new Audio<T>();
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(node->first_node("aid")->value(), node->first_node("aid")->value_size()));
		audio->SetAudioId(m_memoryHandler.Get());
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(node->first_node("owner_id")->value(), node->first_node("owner_id")->value_size()));
		audio->SetOwnerId(m_memoryHandler.Get());
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(node->first_node("artist")->value(), node->first_node("artist")->value_size()));
		audio->SetArtist(m_memoryHandler.Get());
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(node->first_node("title")->value(), node->first_node("title")->value_size()));
		audio->SetTitle(m_memoryHandler.Get());
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(node->first_node("duration")->value(), node->first_node("duration")->value_size()));
		audio->SetDuration(DataTraits<T>::CharArrayToInt(m_memoryHandler.Get()));
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(node->first_node("url")->value(), node->first_node("url")->value_size()));
		audio->SetURL(m_memoryHandler.Get());
		
		return audio;
	}
	
	template<DataType T> const MediaAlbum<T> *ResponseParser<T>::ParseMediaAlbum(const xml_node<char> *albumNode)
	{
		if (albumNode == NULL)
			albumNode = FindSpecifiedNode(m_xmlDoc.first_node(), "album");
		
		MediaAlbum<T> *album = new MediaAlbum<T>();
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(albumNode->first_node("owner_id")->value(),
			albumNode->first_node("owner_id")->value_size()));
		album->SetOwnerId(m_memoryHandler.Get());
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(albumNode->first_node("album_id")->value(),
			albumNode->first_node("album_id")->value_size()));
		album->SetId(m_memoryHandler.Get());
		
		if (albumNode->first_node("title")->value_size() > 0)
		{
			m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(albumNode->first_node("title")->value(),
				albumNode->first_node("title")->value_size()));
			album->SetTitle(m_memoryHandler.Get());
		}
		
		return album;
	}
	
	template<DataType T> const vector<MediaAlbum<T> > *ResponseParser<T>::ParseMediaAlbums()
	{
		const xml_node<char> *albumNode = FindSpecifiedNode(m_xmlDoc.first_node(), "album");
		vector<MediaAlbum<T> > *albums = new vector<MediaAlbum<T> >();
		
		while (albumNode != NULL)
		{
			const MediaAlbum<T> *album = ParseMediaAlbum(albumNode);
			albums->push_back(*album);
			delete album;
			
			albumNode = albumNode->next_sibling();
		}
		
		return albums;
	}
	
	template <DataType T> const AudioAttachment<T> *ResponseParser<T>::ParseAudioAttachment(const xml_node<char> *node)
	{
		AudioAttachment<T> *attachment = new AudioAttachment<T>();
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(node->first_node("aid")->value(), node->first_node("aid")->value_size()));
		attachment->SetPerformer(m_memoryHandler.Get());
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(node->first_node("owner_id")->value(), node->first_node("owner_id")->value_size()));
		attachment->SetOwnerId(m_memoryHandler.Get());
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(node->first_node("performer")->value(), node->first_node("performer")->value_size()));
		attachment->SetPerformer(m_memoryHandler.Get());
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(node->first_node("title")->value(), node->first_node("title")->value_size()));
		attachment->SetTitle(m_memoryHandler.Get());
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(node->first_node("duration")->value(), node->first_node("duration")->value_size()));
		attachment->SetDuration(Converter<T>::CharArrayToUnixTime(m_memoryHandler.Get()));
		
		return attachment;
	}
	
	template<DataType T> const vector<Audio<T> > *ResponseParser<T>::ParseAudios()
	{
		vector<Audio<T> > *audios = new vector<Audio<T> >();
		const xml_node<char> *audioNode = FindSpecifiedNode(m_xmlDoc.first_node(), "audio");

		while (audioNode != NULL)
		{
			audios->push_back(*(ParseAudio(audioNode)));
			audioNode = audioNode->next_sibling();
		}

		return audios;
	}
	
	template<DataType T> bool ResponseParser<T>::ParseBoolean()
	{
		xml_node<char> *node = m_xmlDoc.first_node();
		
		while (node->value_size() < 1)
			node = node->first_node();
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(node->value(), node->value_size()));
		
		return DataTraits<T>::CharArrayToInt(m_memoryHandler.Get());
	}
	
	template<DataType T> const Comment<T> *ResponseParser<T>::ParseComment(const xml_node<char> *commentNode)
	{
		if (commentNode == NULL)
			commentNode = FindSpecifiedNode(m_xmlDoc.first_node(), "comment");
			
		Comment<T> *comment = new Comment<T>();
		
		if (commentNode->first_node("cid") != NULL)
			m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(commentNode->first_node("cid")->value(),
				commentNode->first_node("cid")->value_size()));
		else
			m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(commentNode->first_node("id")->value(),
				commentNode->first_node("id")->value_size()));		
		
		comment->SetId(m_memoryHandler.Get());
		
		if (commentNode->first_node("uid") != NULL)
		{
			m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(commentNode->first_node("uid")->value(),
				commentNode->first_node("uid")->value_size()));
			comment->SetUserId(m_memoryHandler.Get());
		}
		
		if (commentNode->first_node("oid") != NULL)
		{
			m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(commentNode->first_node("oid")->value(),
				commentNode->first_node("oid")->value_size()));
			comment->SetOwnerId(m_memoryHandler.Get());
		}
		
		if (commentNode->first_node("nid") != NULL)
		{
			m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(commentNode->first_node("nid")->value(),
				commentNode->first_node("nid")->value_size()));
			comment->SetNoteId(m_memoryHandler.Get());
		}
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(commentNode->first_node("date")->value(),
			commentNode->first_node("date")->value_size()));
		comment->SetDate(Converter<T>::CharArrayToUnixTime(m_memoryHandler.Get()));
		
		if (commentNode->first_node("text") != NULL && commentNode->first_node("text")->value_size() > 0)
		{
			m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(commentNode->first_node("text")->value(),
				commentNode->first_node("text")->value_size()));
			comment->SetText(m_memoryHandler.Get());
		}
		else if (commentNode->first_node("message")->value_size() > 0)
		{
			m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(commentNode->first_node("message")->value(),
				commentNode->first_node("message")->value_size()));
			comment->SetText(m_memoryHandler.Get());
		}
		
		if (commentNode->first_node("reply_to_uid") != NULL)
		{
			m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(commentNode->first_node("reply_to_uid")->value(),
				commentNode->first_node("reply_to_uid")->value_size()));
			comment->SetReplyToUserId(m_memoryHandler.Get());
		}
		else if (commentNode->first_node("reply_to"))
		{
			m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(commentNode->first_node("reply_to")->value(),
				commentNode->first_node("reply_to")->value_size()));
			comment->SetReplyToUserId(m_memoryHandler.Get());
		}
		
		if (commentNode->first_node("reply_to_cid") != NULL)
		{
			m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(commentNode->first_node("reply_to_cid")->value(),
				commentNode->first_node("reply_to_cid")->value_size()));
			comment->SetReplyToCommentId(m_memoryHandler.Get());
		}
		
		if (commentNode->first_node("likes") != NULL)
		{
			const LikesInfo *likes = ParseLikesInfo(commentNode->first_node("likes"));
			comment->SetLikes(*likes);
			delete likes;
		}
		
		return comment;
	}
	
	template<DataType T> const vector<Comment<T> > *ResponseParser<T>::ParseComments()
	{
		vector<Comment<T> > *comments = new vector<Comment<T> >();
		const xml_node<char> *commentNode = FindSpecifiedNode(m_xmlDoc.first_node(), "comment");
		
		while (commentNode != NULL)
		{
			const Comment<T> *comment = ParseComment(commentNode);
			comments->push_back(*comment);
			delete comment;
			commentNode = commentNode->next_sibling();
		}
		
		return comments;
	}
	
	template<DataType T> const CommentsInfo *ResponseParser<T>::ParseCommentsInfo(const xml_node<char> *commentsInfoNode)
	{
		CommentsInfo *commentsInfo = new CommentsInfo;
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(commentsInfoNode->first_node("count")->value(), commentsInfoNode->first_node("count")->value_size()));
		commentsInfo->SetCount(DataTraits<T>::CharArrayToInt(m_memoryHandler.Get()));
		
		if (commentsInfoNode->first_node("can_post") != NULL)
		{
			m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(commentsInfoNode->first_node("can_post")->value(), commentsInfoNode->first_node("can_post")->value_size()));
			commentsInfo->SetCanPost(DataTraits<T>::CharArrayToInt(m_memoryHandler.Get()));
		}
		
		return commentsInfo;
	}
	
	template<DataType T> const Document<T> *ResponseParser<T>::ParseDocument(const xml_node<char> *docNode)
	{
		if (docNode == NULL)
			docNode = FindSpecifiedNode(m_xmlDoc.first_node(), "doc");
		
		Document<T> *document = new Document<T>();
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(docNode->first_node("did")->value(),
			docNode->first_node("did")->value_size()));
		document->SetId(m_memoryHandler.Get());
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(docNode->first_node("owner_id")->value(),
			docNode->first_node("owner_id")->value_size()));
		document->SetOwnerId(m_memoryHandler.Get());
		
		if (docNode->first_node("title")->value_size() > 0)
		{
			m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(docNode->first_node("title")->value(),
				docNode->first_node("title")->value_size()));
			document->SetTitle(m_memoryHandler.Get());
		}
		
		document->SetFileSize(atol(docNode->first_node("size")->value()));
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(docNode->first_node("ext")->value(),
			docNode->first_node("ext")->value_size()));
		document->SetFileExtension(m_memoryHandler.Get());
		
		if (docNode->first_node("url") != NULL)
		{
			m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(docNode->first_node("url")->value(),
				docNode->first_node("url")->value_size()));
			document->SetUrl(m_memoryHandler.Get());
		}
		
		return document;
	}
	
	template<DataType T> const vector<Document<T> > *ResponseParser<T>::ParseDocuments()
	{
		const xml_node<char> *docNode = FindSpecifiedNode(m_xmlDoc.first_node(), "doc");
		vector<Document<T> > *documents = new vector<Document<T> >();
		
		while (docNode != NULL)
		{
			const Document<T> *document = ParseDocument(docNode);
			documents->push_back(*document);
			delete document;
			
			docNode = docNode->next_sibling();
		}
		
		return documents;
	}
	
	template<DataType T> const GraffitiAttachment<T> *ResponseParser<T>::ParseGraffitiAttachment(const xml_node<char> *graffAttNode)
	{
		GraffitiAttachment<T> *attachment = new GraffitiAttachment<T>();
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(graffAttNode->first_node("gid")->value(), graffAttNode->first_node("gid")->value_size()));
		attachment->SetId(m_memoryHandler.Get());
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(graffAttNode->first_node("owner_id")->value(), graffAttNode->first_node("owner_id")->value_size()));
		attachment->SetOwnerId(m_memoryHandler.Get());
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(graffAttNode->first_node("src")->value(), graffAttNode->first_node("src")->value_size()));
		attachment->SetPreviewURL(m_memoryHandler.Get());
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(graffAttNode->first_node("src_big")->value(), graffAttNode->first_node("src_big")->value_size()));
		attachment->SetImageURL(m_memoryHandler.Get());
		
		return attachment;
	}
	
	template<DataType T> const GroupCounter *ResponseParser<T>::ParseGroupCounter(const xml_node<char> *node)
	{
		GroupCounter *groupCounter = new GroupCounter();
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(node->first_node("albums")->value(), node->first_node("albums")->value_size()));
		groupCounter->SetAlbumsCount(DataTraits<T>::CharArrayToInt(m_memoryHandler.Get()));
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(node->first_node("videos")->value(), node->first_node("videos")->value_size()));
		groupCounter->SetVideosCount(DataTraits<T>::CharArrayToInt(m_memoryHandler.Get()));
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(node->first_node("audios")->value(), node->first_node("audios")->value_size()));
		groupCounter->SetAudiosCount(DataTraits<T>::CharArrayToInt(m_memoryHandler.Get()));
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(node->first_node("topics")->value(), node->first_node("topics")->value_size()));
		groupCounter->SetTopicsCount(DataTraits<T>::CharArrayToInt(m_memoryHandler.Get()));
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(node->first_node("docs")->value(), node->first_node("docs")->value_size()));
		groupCounter->SetDocsCount(DataTraits<T>::CharArrayToInt(m_memoryHandler.Get()));
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(node->first_node("photos")->value(), node->first_node("photos")->value_size()));
		groupCounter->SetPhotosCount(DataTraits<T>::CharArrayToInt(m_memoryHandler.Get()));
		
		return groupCounter;
	}
	
	template<DataType T> const UserCounter *ResponseParser<T>::ParseUserCounter(const xml_node<char> *node)
	{
		xml_node<char> *counterNode = node->first_node("counters");
		UserCounter *userCounter = new UserCounter();
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(counterNode->first_node("albums")->value(), counterNode->first_node("albums")->value_size()));
		userCounter->SetAlbumsCount(DataTraits<T>::CharArrayToInt(m_memoryHandler.Get()));
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(counterNode->first_node("videos")->value(), counterNode->first_node("videos")->value_size()));
		userCounter->SetVideosCount(DataTraits<T>::CharArrayToInt(m_memoryHandler.Get()));
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(counterNode->first_node("audios")->value(), counterNode->first_node("audios")->value_size()));
		userCounter->SetAudiosCount(DataTraits<T>::CharArrayToInt(m_memoryHandler.Get()));
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(counterNode->first_node("notes")->value(), counterNode->first_node("notes")->value_size()));
		userCounter->SetNotesCount(DataTraits<T>::CharArrayToInt(m_memoryHandler.Get()));
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(counterNode->first_node("friends")->value(), counterNode->first_node("friends")->value_size()));
		userCounter->SetFriendsCount(DataTraits<T>::CharArrayToInt(m_memoryHandler.Get()));
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(counterNode->first_node("online_friends")->value(), counterNode->first_node("online_friends")->value_size()));
		userCounter->SetOnlineFriendsCount(DataTraits<T>::CharArrayToInt(m_memoryHandler.Get()));
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(counterNode->first_node("user_photos")->value(), counterNode->first_node("user_photos")->value_size()));
		userCounter->SetPhotosWithUserCount(DataTraits<T>::CharArrayToInt(m_memoryHandler.Get()));
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(counterNode->first_node("user_videos")->value(), counterNode->first_node("user_videos")->value_size()));
		userCounter->SetVideosWithUserCount(DataTraits<T>::CharArrayToInt(m_memoryHandler.Get()));
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(counterNode->first_node("followers")->value(), counterNode->first_node("followers")->value_size()));
		userCounter->SetFollowersCount(DataTraits<T>::CharArrayToInt(m_memoryHandler.Get()));
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(counterNode->first_node("subscriptions")->value(), counterNode->first_node("subscriptions")->value_size()));
		userCounter->SetSubscriptionsCount(DataTraits<T>::CharArrayToInt(m_memoryHandler.Get()));
		
		return userCounter;
	}
	
	template<DataType T> const Video<T> *ResponseParser<T>::ParseVideo(const xml_node<char> *videoNode)
	{
		if (videoNode == NULL)
			videoNode = FindSpecifiedNode(m_xmlDoc.first_node(), "video");
			
		Video<T> *video = new Video<T>();
		
		if (videoNode->first_node("vid") != NULL)
		{
			m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(videoNode->first_node("vid")->value(),
				videoNode->first_node("vid")->value_size()));
			video->SetId(m_memoryHandler.Get());
		}
		else
		{
			m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(videoNode->first_node("id")->value(),
				videoNode->first_node("id")->value_size()));
			video->SetId(m_memoryHandler.Get());
		}
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(videoNode->first_node("owner_id")->value(),
			videoNode->first_node("owner_id")->value_size()));
		video->SetOwnerId(m_memoryHandler.Get());
		
		if (videoNode->first_node("title") != NULL && videoNode->first_node("title")->value_size() > 0)
		{
			m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(videoNode->first_node("title")->value(),
				videoNode->first_node("title")->value_size()));
			video->SetTitle(m_memoryHandler.Get());
		}
			
		if (videoNode->first_node("description") != NULL && videoNode->first_node("description")->value_size() > 0)
		{
			m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(videoNode->first_node("description")->value(),
				videoNode->first_node("description")->value_size()));
			video->SetDescription(m_memoryHandler.Get());
		}
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(videoNode->first_node("duration")->value(),
			videoNode->first_node("duration")->value_size()));
		video->SetDuration(Converter<T>::CharArrayToUnixTime(m_memoryHandler.Get()));
		
		if (videoNode->first_node("image") != NULL)
		{
			m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(videoNode->first_node("image")->value(),
				videoNode->first_node("image")->value_size()));
			video->SetImageUrl(m_memoryHandler.Get());
		}
		
		if (videoNode->first_node("link") != NULL)
		{
			m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(videoNode->first_node("link")->value(),
				videoNode->first_node("link")->value_size()));
			video->SetLink(m_memoryHandler.Get());
		}
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(videoNode->first_node("date")->value(),
			videoNode->first_node("date")->value_size()));
		video->SetDate(Converter<T>::CharArrayToUnixTime(m_memoryHandler.Get()));
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(videoNode->first_node("player")->value(),
			videoNode->first_node("player")->value_size()));
		video->SetPlayerUrl(m_memoryHandler.Get());
		
		if (videoNode->first_node("thumb") != NULL)
		{
			m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(videoNode->first_node("thumb")->value(),
				videoNode->first_node("thumb")->value_size()));
			video->SetThumbUrl(m_memoryHandler.Get());
		}
		
		return video;
	}
	
	template<DataType T> const VideoAttachment<T> *ResponseParser<T>::ParseVideoAttachment(const xml_node<char> *videoAttNode)
	{
		VideoAttachment<T> *attachment = new VideoAttachment<T>();
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(videoAttNode->first_node("vid")->value(), videoAttNode->first_node("vid")->value_size()));
		attachment->SetVideoId(m_memoryHandler.Get());
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(videoAttNode->first_node("owner_id")->value(), videoAttNode->first_node("owner_id")->value_size()));
		attachment->SetOwnerId(m_memoryHandler.Get());
		
		if (videoAttNode->first_node("title") != NULL && videoAttNode->first_node("title")->value_size() > 0)
		{
			m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(videoAttNode->first_node("title")->value(), videoAttNode->first_node("title")->value_size()));
			attachment->SetTitle(m_memoryHandler.Get());
		}
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(videoAttNode->first_node("duration")->value(), videoAttNode->first_node("duration")->value_size()));
		attachment->SetDuration(DataTraits<T>::CharArrayToInt(m_memoryHandler.Get()));
		
		return attachment;
	}
	
	template<DataType T> const VideoTag<T> *ResponseParser<T>::ParseVideoTag(const xml_node<char> *tagNode)
	{
		if (tagNode == NULL)
			tagNode = FindSpecifiedNode(m_xmlDoc.first_node(), "tag");
			
		VideoTag<T> *tag = new VideoTag<T>();
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(tagNode->first_node("uid")->value(),
			tagNode->first_node("uid")->value_size()));
		tag->SetUserId(m_memoryHandler.Get());
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(tagNode->first_node("tag_id")->value(),
			tagNode->first_node("tag_id")->value_size()));
		tag->SetId(m_memoryHandler.Get());
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(tagNode->first_node("placer_id")->value(),
			tagNode->first_node("placer_id")->value_size()));
		tag->SetPlacerId(m_memoryHandler.Get());
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(tagNode->first_node("tagged_name")->value(),
			tagNode->first_node("tagged_name")->value_size()));
		tag->SetTaggedName(m_memoryHandler.Get());
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(tagNode->first_node("date")->value(),
			tagNode->first_node("date")->value_size()));
		tag->SetDate(Converter<T>::CharArrayToUnixTime(m_memoryHandler.Get()));
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(tagNode->first_node("viewed")->value(),
			tagNode->first_node("viewed")->value_size()));
		tag->SetViewedState(DataTraits<T>::CharArrayToInt(m_memoryHandler.Get()));
		
		return tag;
	}
	
	template<DataType T> const vector<VideoTag<T> > *ResponseParser<T>::ParseVideoTags()
	{
		vector<VideoTag<T> > *tags = new vector<VideoTag<T> >();
		const xml_node<char> *tagNode = FindSpecifiedNode(m_xmlDoc.first_node(), "tag");
		
		while (tagNode != NULL)
		{
			const VideoTag<T> *tag = ParseVideoTag(tagNode);
			tags->push_back(*tag);
			delete tag;
			
			tagNode = tagNode->next_sibling();
		}
		
		return tags;
	}
	
	template<DataType T> const vector<Video<T> > *ResponseParser<T>::ParseVideos()
	{
		vector<Video<T> > *videos = new vector<Video<T> >();
		const xml_node<char> *videoNode = FindSpecifiedNode(m_xmlDoc.first_node(), "video");
		
		while (videoNode != NULL)
		{
			const Video<T> *video = ParseVideo(videoNode);
			videos->push_back(*video);
			delete video;
			
			videoNode = videoNode->next_sibling();
		}
		
		return videos;
	}
	
	template<DataType T> const WikiPageAttachment<T> *ResponseParser<T>::ParseWikiPageAttachment(const xml_node<char> *wikiAttNode)
	{
		WikiPageAttachment<T> *attachment = new WikiPageAttachment<T>();
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(wikiAttNode->first_node("pid")->value(), wikiAttNode->first_node("pid")->value_size()));
		attachment->SetId(m_memoryHandler.Get());
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(wikiAttNode->first_node("gid")->value(), wikiAttNode->first_node("gid")->value_size()));
		attachment->SetGroupId(m_memoryHandler.Get());
		
		if (wikiAttNode->first_node("title") != NULL && wikiAttNode->first_node("title")->value_size() > 0)
		{
			m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(wikiAttNode->first_node("title")->value(), wikiAttNode->first_node("title")->value_size()));
			attachment->SetTitle(m_memoryHandler.Get());
		}
		
		return attachment;
	}
	
	template<DataType T> const Group<T> *ResponseParser<T>::ParseGroup(const xml_node<char> *node)
	{
		Group<T> *group = new Group<T>();
		
		if (node->first_node("city") != NULL && node->first_node("city")->value_size() > 0)
		{
			m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(node->first_node("city")->value(), node->first_node("city")->value_size()));
			group->SetCityId(m_memoryHandler.Get());
		}
		
		if (node->first_node("country") != NULL && node->first_node("country")->value_size() > 0)
		{
			m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(node->first_node("country")->value(), node->first_node("country")->value_size()));
			group->SetCountryId(m_memoryHandler.Get());
		}
		
		if (node->first_node("description") != NULL && node->first_node("description")->value_size() > 0)
		{
			m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(node->first_node("description")->value(), node->first_node("description")->value_size()));
			group->SetDescription(m_memoryHandler.Get());
		}
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(node->first_node("gid")->value(), node->first_node("gid")->value_size()));
		group->SetId(m_memoryHandler.Get());
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(node->first_node("name")->value(), node->first_node("name")->value_size()));
		group->SetName(m_memoryHandler.Get());
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(node->first_node("screen_name")->value(), node->first_node("screen_name")->value_size()));
		group->SetScreenName(m_memoryHandler.Get());
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(node->first_node("is_closed")->value(), node->first_node("is_closed")->value_size()));
		group->SetClosed(DataTraits<T>::CharArrayToInt(m_memoryHandler.Get()));
			
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(node->first_node("is_admin")->value(), node->first_node("is_admin")->value_size()));
		group->SetAdmin(DataTraits<T>::CharArrayToInt(m_memoryHandler.Get()));
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(node->first_node("is_member")->value(), node->first_node("is_member")->value_size()));
		group->SetMember(DataTraits<T>::CharArrayToInt(m_memoryHandler.Get()));
		
		if (node->first_node("members_count") != NULL)
		{
			m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(node->first_node("members_count")->value(), node->first_node("members_count")->value_size()));
			group->SetMember(DataTraits<T>::CharArrayToInt(m_memoryHandler.Get()));
		}
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(node->first_node("photo")->value(), node->first_node("photo")->value_size()));
		group->SetPhoto(m_memoryHandler.Get());

		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(node->first_node("photo_medium")->value(), node->first_node("photo_medium")->value_size()));
		group->SetPhotoMedium(m_memoryHandler.Get());
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(node->first_node("photo_big")->value(), node->first_node("photo_big")->value_size()));
		group->SetPhotoBig(m_memoryHandler.Get());
		
		if (node->first_node("wiki_page") != NULL && node->first_node("wiki_page")->value_size() > 0)
		{
			m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(node->first_node("wiki_page")->value(), node->first_node("wiki_page")->value_size()));
			group->SetDescription(m_memoryHandler.Get());
		}
		
		if (node->first_node("start_date") != NULL)
		{
			m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(node->first_node("start_date")->value(), node->first_node("start_date")->value_size()));
			group->SetStartDate(Converter<T>::CharArrayToUnixTime(m_memoryHandler.Get()));
		}
		
		if (node->first_node("end_date") != NULL)
		{
			m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(node->first_node("end_date")->value(), node->first_node("end_date")->value_size()));
			group->SetEndDate(Converter<T>::CharArrayToUnixTime(m_memoryHandler.Get()));
		}
		
		if (node->first_node("wiki_page") != NULL && node->first_node("wiki_page")->value_size() > 0)
		{
			m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(node->first_node("wiki_page")->value(), node->first_node("wiki_page")->value_size()));
			group->SetDescription(m_memoryHandler.Get());
		}
		
		if (node->first_node("place") != NULL)
			ParsePlace(node->first_node("place"));
			
		if (node->first_node("counters") != NULL)
			group->SetCounter(*(ParseGroupCounter(node->first_node("counters"))));
		
		return group;
	}
	
	template<DataType T> const vector<Group<T> > *ResponseParser<T>::ParseGroups()
	{
		vector<Group<T> > *groups = new vector<Group<T> >();
		xml_node<char> *groupNode = NULL;
		
		//Если возвращается count - пропускаем этот узел
		if (m_xmlDoc.first_node()->first_node("groups") != NULL)
			groupNode = m_xmlDoc.first_node()->first_node("groups")->first_node();
		else if (strcmp(m_xmlDoc.first_node()->first_node()->name(), "count") == 0)
			groupNode = m_xmlDoc.first_node()->first_node()->next_sibling();
		else
			groupNode = m_xmlDoc.first_node()->first_node();
		
		while (groupNode != NULL)
		{
			groups->push_back(*(ParseGroup(groupNode)));
			groupNode = groupNode->next_sibling();
		}
		
		return groups;
	}
	
	template<DataType T> const LikesInfo *ResponseParser<T>::ParseLikesInfo(const xml_node<char> *likesInfoNode)
	{
		LikesInfo *likesInfo = new LikesInfo;
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(likesInfoNode->first_node("count")->value(), likesInfoNode->first_node("count")->value_size()));
		likesInfo->SetCount(DataTraits<T>::CharArrayToInt(m_memoryHandler.Get()));
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(likesInfoNode->first_node("user_likes")->value(), likesInfoNode->first_node("user_likes")->value_size()));
		likesInfo->SetUserLikes(DataTraits<T>::CharArrayToInt(m_memoryHandler.Get()));
		
		if (likesInfoNode->first_node("can_like") != NULL)
		{
			m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(likesInfoNode->first_node("can_like")->value(), likesInfoNode->first_node("can_like")->value_size()));
			likesInfo->SetCanLike(DataTraits<T>::CharArrayToInt(m_memoryHandler.Get()));
		}
		
		if (likesInfoNode->first_node("can_publish") != NULL)
		{
			m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(likesInfoNode->first_node("can_publish")->value(), likesInfoNode->first_node("can_publish")->value_size()));
			likesInfo->SetCanPublish(DataTraits<T>::CharArrayToInt(m_memoryHandler.Get()));
		}
		
		return likesInfo;
	}
	
	template<DataType T> const LinkAttachment<T> *ResponseParser<T>::ParseLinkAttachment(const xml_node<char> *linkAttNode)
	{
		LinkAttachment<T> *attachment = new LinkAttachment<T>();
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(linkAttNode->first_node("url")->value(), linkAttNode->first_node("url")->value_size()));
		attachment->SetUrl(m_memoryHandler.Get());
		
		if (linkAttNode->first_node("title")->value_size() > 0)
		{
			m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(linkAttNode->first_node("title")->value(), linkAttNode->first_node("title")->value_size()));
			attachment->SetTitle(m_memoryHandler.Get());
		}
		
		if (linkAttNode->first_node("description")->value_size() > 0)
		{
			m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(linkAttNode->first_node("description")->value(), linkAttNode->first_node("description")->value_size()));
			attachment->SetDescription(m_memoryHandler.Get());
		}
		
		if (linkAttNode->first_node("image_src") != NULL && linkAttNode->first_node("image_src")->value_size() > 0)
		{
			m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(linkAttNode->first_node("image_src")->value(), linkAttNode->first_node("image_src")->value_size()));
			attachment->SetPreviewURL(m_memoryHandler.Get());
		}
		
		return attachment;
	}
	
	template<DataType T> size_t ResponseParser<T>::ParseNestedUInt()
	{
		xml_node<char> *node = m_xmlDoc.first_node()->first_node();
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(node->value(), node->value_size()));
		return DataTraits<T>::CharArrayToInt(m_memoryHandler.Get());
	}
	
	template<DataType T> const vector<News<T> > *ResponseParser<T>::ParseNews()
	{
		vector<News<T> > *news = new vector<News<T> >();
		
		const xml_node<char> *itemNode = FindSpecifiedNode(m_xmlDoc.first_node(), "item");
		
		while (itemNode != NULL)
		{
			News<T> *oneNews = new News<T>();
			
			if (itemNode->first_node("type") != NULL)
			{
				if (strcmp(itemNode->first_node("type")->value(), "post") == 0)
					oneNews->SetType(NewsType_Post);
				else if (strcmp(itemNode->first_node("type")->value(), "photo") == 0)
					oneNews->SetType(NewsType_Photo);
				else if (strcmp(itemNode->first_node("type")->value(), "photo_tag") == 0)
					oneNews->SetType(NewsType_PhotoTag);
				else if (strcmp(itemNode->first_node("type")->value(), "friend") == 0)
					oneNews->SetType(NewsType_Friend);
				else if (strcmp(itemNode->first_node("type")->value(), "note") == 0)
					oneNews->SetType(NewsType_Note);
			}
			
			if (itemNode->first_node("source_id") != NULL)
			{
				m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(itemNode->first_node("source_id")->value(), itemNode->first_node("source_id")->value_size()));
				oneNews->SetSourceId(m_memoryHandler.Get());
			}
			
			m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(itemNode->first_node("date")->value(), itemNode->first_node("date")->value_size()));
			oneNews->SetDate(Converter<T>::CharArrayToUnixTime(m_memoryHandler.Get()));
			
			if (itemNode->first_node("post_id") != NULL)
			{
				m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(itemNode->first_node("post_id")->value(), itemNode->first_node("post_id")->value_size()));
				oneNews->SetPostId(m_memoryHandler.Get());
			}
			
			if (itemNode->first_node("text") != NULL && itemNode->first_node("text")->value_size() > 0)
			{
				m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(itemNode->first_node("text")->value(), itemNode->first_node("text")->value_size()));
				oneNews->SetText(m_memoryHandler.Get());
			}
			
			if (itemNode->first_node("copy_owner_id") != NULL)
			{
				m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(itemNode->first_node("copy_owner_id")->value(), itemNode->first_node("copy_owner_id")->value_size()));
				oneNews->SetCopyOwnerId(m_memoryHandler.Get());
			}
			
			if (itemNode->first_node("copy_post_id") != NULL)
			{
				m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(itemNode->first_node("copy_post_id")->value(), itemNode->first_node("copy_post_id")->value_size()));
				oneNews->SetCopyPostId(m_memoryHandler.Get());
			}
			
			if (itemNode->first_node("copy_post_date") != NULL)
			{
				m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(itemNode->first_node("copy_post_date")->value(), itemNode->first_node("copy_post_date")->value_size()));
				oneNews->SetCopyPostDate(Converter<T>::CharArrayToUnixTime(m_memoryHandler.Get()));
			}
			
			if (itemNode->first_node("comments") != NULL)
			{
				xml_node<char> *commentsNode = itemNode->first_node("comments");
				oneNews->SetCommentsInfo(*(ParseCommentsInfo(commentsNode)));
			}
			
			if (itemNode->first_node("likes") != NULL)
			{
				xml_node<char> *likesNode = itemNode->first_node("likes");
				oneNews->SetLikesInfo(*(ParseLikesInfo(likesNode)));
			}
			
			if (itemNode->first_node("reposts") != NULL)
			{
				xml_node<char> *repostsNode = itemNode->first_node("reposts");
				oneNews->SetRepostsInfo(*(ParseRepostsInfo(repostsNode)));
			}
			
			if (itemNode->first_node("geo") != NULL)
			{
				xml_node<char> *geoNode = itemNode->first_node("geo")->first_node("place");
				oneNews->SetPlace(*(ParsePlace(geoNode)));
			}
			
			if (itemNode->first_node("photos") != NULL)
			{
				xml_node<char> *photosNode = itemNode->first_node("photos")->first_node("photos");
				oneNews->SetPhotos(*(ParsePhotos()));
			}
			
			if (itemNode->first_node("photo_tags") != NULL)
			{
				xml_node<char> *photosNode = itemNode->first_node("photo_tags")->first_node("photo_tags");
				//TODO: парсить фотки oneNews->SetPlace(*(ParsePlace(photosNode)));
			}
			
			if (itemNode->first_node("attachments") != NULL)
				oneNews->SetAttachments(*(ParseAttachments(itemNode->first_node("attachments"))));
			
			news->push_back(*oneNews);
			delete oneNews;
			
			itemNode = itemNode->next_sibling();
		}
		
		return news;
	}
	
	template<DataType T> const NewsFeed<T> *ResponseParser<T>::ParseNewsFeed()
	{
		NewsFeed<T> *newsFeed = new NewsFeed<T>();
		xml_node<char> *itemNode = m_xmlDoc.first_node()->first_node()->first_node();
		newsFeed->SetNews(*(ParseNews(itemNode)));
		
		if (m_xmlDoc.first_node()->first_node("profiles") != NULL)
			newsFeed->SetProfiles(*(ParseProfiles()));
			
		if (m_xmlDoc.first_node()->first_node("groups") != NULL)
			newsFeed->SetGroups(*(ParseGroups()));
		
		xml_node<char> *newOffsetNode = m_xmlDoc.first_node()->first_node("new_offset");
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(newOffsetNode->value(), newOffsetNode->value_size()));
		newsFeed->SetNewOffset(DataTraits<T>::CharArrayToInt(m_memoryHandler.Get()));
		
		xml_node<char> *newFromNode = m_xmlDoc.first_node()->first_node("new_from");
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(newFromNode->value(), newFromNode->value_size()));
		newsFeed->SetNewFromField(m_memoryHandler.Get());
		
		return newsFeed;
	}
	
	template<DataType T> const Note<T> *ResponseParser<T>::ParseNote(const xml_node<char> *noteNode)
	{
		if (noteNode == NULL)
			noteNode = FindSpecifiedNode(m_xmlDoc.first_node(), "note");
		
		Note<T> *note = new Note<T>();
		
		//TODO: Ошибка ВКонтакте АПИ. Срезать лишнюю проверку лишнего XML узла после исправления.
		if (noteNode->first_node("user_id") != NULL)
		{
			m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(noteNode->first_node("user_id")->value(),
				noteNode->first_node("user_id")->value_size()));
			note->SetUserId(m_memoryHandler.Get());
		}
		else
		{
			m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(noteNode->first_node("uid")->value(),
				noteNode->first_node("uid")->value_size()));
			note->SetUserId(m_memoryHandler.Get());
		}
		
		//TODO: Ошибка ВКонтакте АПИ. Срезать лишнюю проверку лишнего XML узла после исправления.
		if (noteNode->first_node("id") != NULL)
		{
			m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(noteNode->first_node("id")->value(),
				noteNode->first_node("id")->value_size()));
			note->SetId(m_memoryHandler.Get());
		}
		else
		{
			m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(noteNode->first_node("nid")->value(),
				noteNode->first_node("nid")->value_size()));
			note->SetId(m_memoryHandler.Get());
		}
		
		if (noteNode->first_node("owner_id") != NULL)
		{
			m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(noteNode->first_node("owner_id")->value(),
				noteNode->first_node("owner_id")->value_size()));
			note->SetOwnerId(m_memoryHandler.Get());
		}
		
		if (noteNode->first_node("title") != NULL && noteNode->first_node("title")->value_size() > 0)
		{
			m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(noteNode->first_node("title")->value(),
				noteNode->first_node("title")->value_size()));
			note->SetTitle(m_memoryHandler.Get());
		}
		
		if (noteNode->first_node("text") != NULL && noteNode->first_node("text")->value_size() > 0)
		{
			m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(noteNode->first_node("text")->value(),
				noteNode->first_node("text")->value_size()));
			note->SetText(m_memoryHandler.Get());
		}
		
		if (noteNode->first_node("text_wiki") != NULL && noteNode->first_node("text_wiki")->value_size() > 0)
		{
			m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(noteNode->first_node("text_wiki")->value(),
				noteNode->first_node("text_wiki")->value_size()));
			note->SetTextWiki(m_memoryHandler.Get());
		}
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(noteNode->first_node("ncom")->value(),
			noteNode->first_node("ncom")->value_size()));
		note->SetCommentsCount(DataTraits<T>::CharArrayToInt(m_memoryHandler.Get()));
		
		if (noteNode->first_node("read_ncom") != NULL)
		{
			m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(noteNode->first_node("read_ncom")->value(),
				noteNode->first_node("read_ncom")->value_size()));
			note->SetReadCommentsCount(DataTraits<T>::CharArrayToInt(m_memoryHandler.Get()));
		}
		
		if (noteNode->first_node("privacy") != NULL)
		{
			m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(noteNode->first_node("privacy")->value(),
				noteNode->first_node("privacy")->value_size()));
			note->SetPrivacy(
				static_cast<AccessPrivacy>(DataTraits<T>::CharArrayToInt(m_memoryHandler.Get())));
		}
		
		if (noteNode->first_node("comment_privacy") != NULL)
		{
			m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(noteNode->first_node("comment_privacy")->value(),
				noteNode->first_node("comment_privacy")->value_size()));
			note->SetCommentPrivacy(
				static_cast<AccessPrivacy>(DataTraits<T>::CharArrayToInt(m_memoryHandler.Get())));
		}
		
		if (noteNode->first_node("can_comment") != NULL)
		{
			m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(noteNode->first_node("can_comment")->value(),
				noteNode->first_node("can_comment")->value_size()));
			note->SetCommentAbility(DataTraits<T>::CharArrayToInt(m_memoryHandler.Get()));
		}
		
		if (noteNode->first_node("date") != NULL)
		{
			m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(noteNode->first_node("date")->value(),
				noteNode->first_node("date")->value_size()));
			note->SetDate(Converter<T>::CharArrayToUnixTime(m_memoryHandler.Get()));
		}
		
		return note;
	}
	
	template<DataType T> const vector<Note<T> > *ResponseParser<T>::ParseNotes()
	{
		const xml_node<char> *noteNode = FindSpecifiedNode(m_xmlDoc.first_node(), "note");
		vector<Note<T> > *notes = new vector<Note<T> >();
		
		while (noteNode != NULL)
		{
			const Note<T> *note = ParseNote(noteNode);
			notes->push_back(*note);
			delete note;
			
			noteNode = noteNode->next_sibling();
		}
		
		return notes;
	}
	
	template<DataType T> const Photo<T> *ResponseParser<T>::ParsePhoto(const xml_node<char> *photoNode)
	{
		if (photoNode != NULL)
			photoNode = FindSpecifiedNode(m_xmlDoc.first_node(), "photo");
		
		Photo<T> *photo = new Photo<T>();
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(photoNode->first_node("pid")->value(), photoNode->first_node("pid")->value_size()));
		photo->SetId(m_memoryHandler.Get());
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(photoNode->first_node("aid")->value(), photoNode->first_node("aid")->value_size()));
		photo->SetAlbumId(m_memoryHandler.Get());
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(photoNode->first_node("owner_id")->value(), photoNode->first_node("owner_id")->value_size()));
		photo->SetOwnerId(m_memoryHandler.Get());
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(photoNode->first_node("src")->value(), photoNode->first_node("src")->value_size()));
		photo->SetUrl(m_memoryHandler.Get());
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(photoNode->first_node("src_small")->value(), photoNode->first_node("src_small")->value_size()));
		photo->SetPreviewURL(m_memoryHandler.Get());
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(photoNode->first_node("src_big")->value(), photoNode->first_node("src_big")->value_size()));
		photo->SetBigImageURL(m_memoryHandler.Get());
		
		if (photoNode->first_node("src_xbig") != NULL)
		{
			m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(photoNode->first_node("src_xbig")->value(), photoNode->first_node("src_xbig")->value_size()));
			photo->SetXBigImageUrl(m_memoryHandler.Get());
		}
		
		if (photoNode->first_node("src_xxbig") != NULL)
		{
			m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(photoNode->first_node("src_xxbig")->value(), photoNode->first_node("src_xxbig")->value_size()));
			photo->SetXxBigImageUrl(m_memoryHandler.Get());
		}
		
		if (photoNode->first_node("text")->value_size() > 0)
		{
			m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(photoNode->first_node("text")->value(), photoNode->first_node("text")->value_size()));
			photo->SetText(m_memoryHandler.Get());
		}
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(photoNode->first_node("created")->value(), photoNode->first_node("created")->value_size()));
		photo->SetCreatedTime(Converter<T>::CharArrayToUnixTime(m_memoryHandler.Get()));
		
		if (photoNode->first_node("likes") != NULL)
			photo->SetLikesInfo(*(ParseLikesInfo(photoNode->first_node("likes"))));
		
		if (photoNode->first_node("comments") != NULL)
			photo->SetCommentInfo(*(ParseCommentsInfo(photoNode->first_node("comments"))));
		
		if (photoNode->first_node("tags") != NULL)
			photo->SetTagsInfo(*(ParseTagsInfo(photoNode->first_node("tags"))));
		
		return photo;
	}
	
	template<DataType T> const vector<Photo<T> > *ResponseParser<T>::ParsePhotos()
	{
		vector<Photo<T> > *photos = new vector<Photo<T> >();
		const xml_node<char> *photoNode = FindSpecifiedNode(m_xmlDoc.first_node(), "photo");
		
		while (photoNode != NULL)
		{
			const Photo<T> *photo = ParsePhoto(photoNode);
			photos->push_back(*photo);
			delete photo;
			
			photoNode = photoNode->next_sibling();
		}
		
		return photos;
	}
	
	template<DataType T> const PhotoAttachment<T> *ResponseParser<T>::ParsePhotoAttachment(const xml_node<char> *photoAttNode)
	{
		PhotoAttachment<T> *attachment = new PhotoAttachment<T>();
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(photoAttNode->first_node("pid")->value(), photoAttNode->first_node("pid")->value_size()));
		attachment->SetPhotoId(m_memoryHandler.Get());
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(photoAttNode->first_node("owner_id")->value(), photoAttNode->first_node("owner_id")->value_size()));
		attachment->SetOwnerId(m_memoryHandler.Get());
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(photoAttNode->first_node("src")->value(), photoAttNode->first_node("src")->value_size()));
		attachment->SetPreviewURL(m_memoryHandler.Get());
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(photoAttNode->first_node("src_big")->value(), photoAttNode->first_node("src_big")->value_size()));
		attachment->SetPhotoURL(m_memoryHandler.Get());
		
		return attachment;
	}
	
	template<DataType T> const Place<T> *ResponseParser<T>::ParsePlace(const xml_node<char> *node)
	{
		Place<T> *place = new Place<T>();
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(node->first_node("pid")->value(), node->first_node("pid")->value_size()));
		place->SetId(m_memoryHandler.Get());
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(node->first_node("latitude")->value(), node->first_node("latitude")->value_size()));
		place->SetLatitude(DataTraits<T>::CharArrayToFloat(m_memoryHandler.Get()));
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(node->first_node("longitude")->value(), node->first_node("longitude")->value_size()));
		place->SetLongitude(DataTraits<T>::CharArrayToFloat(m_memoryHandler.Get()));
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(node->first_node("created")->value(), node->first_node("created")->value_size()));
		place->SetCreatedTime(Converter<T>::CharArrayToUnixTime(m_memoryHandler.Get()));
		
		if (node->first_node("title") != NULL && node->first_node("title")->value_size() > 0)
		{
			m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(node->first_node("title")->value(), node->first_node("title")->value_size()));
			place->SetTitle(m_memoryHandler.Get());
		}
		
		if (node->first_node("type") != NULL && node->first_node("type")->value_size() > 0)
		{
			m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(node->first_node("type")->value(), node->first_node("type")->value_size()));
			place->SetTypeId(m_memoryHandler.Get());
		}
		
		if (node->first_node("city") != NULL)
		{
			m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(node->first_node("city")->value(), node->first_node("city")->value_size()));
			place->SetCityId(m_memoryHandler.Get());
		}
		
		if (node->first_node("country") != NULL)
		{
			m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(node->first_node("country")->value(), node->first_node("country")->value_size()));
			place->SetCountryId(m_memoryHandler.Get());
		}
		
		return place;
	}
	
	template<DataType T> const PollAttachment<T> *ResponseParser<T>::ParsePollAttachment(const xml_node<char> *pollAttNode)
	{
		PollAttachment<T> *attachment = new PollAttachment<T>();
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(pollAttNode->first_node("poll_id")->value(), pollAttNode->first_node("poll_id")->value_size()));
		attachment->SetId(m_memoryHandler.Get());
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(pollAttNode->first_node("question")->value(), pollAttNode->first_node("question")->value_size()));
		attachment->SetQuestion(m_memoryHandler.Get());
		
		return attachment;
	}
	
	template<DataType T> const Post<T> *ResponseParser<T>::ParsePost(const xml_node<char> *postNode)
	{
		if (postNode == NULL)
			postNode = FindSpecifiedNode(m_xmlDoc.first_node(), "post");
			
		Post<T> *post = new Post<T>();
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(postNode->first_node("id")->value(),
			postNode->first_node("id")->value_size()));
		post->SetId(m_memoryHandler.Get());
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(postNode->first_node("from_id")->value(),
			postNode->first_node("from_id")->value_size()));
		post->SetFromId(m_memoryHandler.Get());
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(postNode->first_node("to_id")->value(),
			postNode->first_node("to_id")->value_size()));
		post->SetToId(m_memoryHandler.Get());
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(postNode->first_node("date")->value(),
			postNode->first_node("date")->value_size()));
		post->SetDate(Converter<T>::CharArrayToUnixTime(m_memoryHandler.Get()));
		
		if (postNode->first_node("text") != NULL && postNode->first_node("text")->value_size() > 0)
		{
			m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(postNode->first_node("text")->value(),
				postNode->first_node("text")->value_size()));
			post->SetText(m_memoryHandler.Get());
		}
		
		if (postNode->first_node("attachments") != NULL)
			post->SetAttachments(*(ParseAttachments(postNode->first_node("attachments"))));
		
		if (postNode->first_node("comments") != NULL)
			post->SetComments(*(ParseCommentsInfo(postNode->first_node("comments"))));
			
		if (postNode->first_node("likes") != NULL)
			post->SetLikes(*(ParseLikesInfo(postNode->first_node("likes"))));
			
		if (postNode->first_node("reposts") != NULL)
			post->SetReposts(*(ParseRepostsInfo(postNode->first_node("reposts"))));
		
		if (postNode->first_node("signed_id") != NULL)
		{
			m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(postNode->first_node("signed_id")->value(),
				postNode->first_node("signed_id")->value_size()));
			post->SetSignedId(m_memoryHandler.Get());
		}
		
		if (postNode->first_node("copy_owner_id") != NULL)
		{
			m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(postNode->first_node("copy_owner_id")->value(),
				postNode->first_node("copy_owner_id")->value_size()));
			post->SetCopyOwnerId(m_memoryHandler.Get());
		}
		
		if (postNode->first_node("copy_post_id") != NULL)
		{
			m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(postNode->first_node("copy_post_id")->value(),
				postNode->first_node("copy_post_id")->value_size()));
			post->SetCopyPostId(m_memoryHandler.Get());
		}
		
		if (postNode->first_node("copy_text") != NULL && postNode->first_node("copy_text")->value_size() > 0)
		{
			m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(postNode->first_node("copy_text")->value(),
				postNode->first_node("copy_text")->value_size()));
			post->SetCopyText(m_memoryHandler.Get());
		}
		
		if (postNode->first_node("geo") != NULL)
			post->SetPlace(*(ParsePlace(FindSpecifiedNode(postNode->first_node("geo"), "place"))));
		
		return post;
	}
	
	template<DataType T> const vector<Post<T> > *ResponseParser<T>::ParsePosts()
	{
		vector<Post<T> > *posts = new vector<Post<T> >();
		
		const xml_node<char> *postNode = FindSpecifiedNode(m_xmlDoc.first_node(), "post");
		
		while (postNode != NULL)
		{
			const Post<T> *post = ParsePost(postNode);
			posts->push_back(*post);
			delete post;
			postNode = postNode->next_sibling();
		}
		
		return posts;
	}
	
	template<DataType T> const Profile<T> *ResponseParser<T>::ParseProfile(const xml_node<char> *node)
	{
		Profile<T> *newProfile = new Profile<T>;
		
		//Нижние поля парсим без проверки, так как возвращаются в любом случае.
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(node->first_node("uid")->value(), node->first_node("uid")->value_size()));
		newProfile->SetUserId(m_memoryHandler.Get());
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(node->first_node("first_name")->value(), node->first_node("first_name")->value_size()));
		newProfile->SetFirstName(m_memoryHandler.Get());
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(node->first_node("last_name")->value(), node->first_node("last_name")->value_size()));
		newProfile->SetLastName(m_memoryHandler.Get());
		
		if (node->first_node("nickname") != NULL && node->first_node("nickname")->value_size() > 0)
		{
			m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(node->first_node("nickname")->value(), node->first_node("nickname")->value_size()));
			newProfile->SetNickName(m_memoryHandler.Get());
		}
		
		if (node->first_node("screen_name") != NULL && node->first_node("screen_name")->value_size() > 0)
		{
			m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(node->first_node("screen_name")->value(), node->first_node("screen_name")->value_size()));
			newProfile->SetScreenName(m_memoryHandler.Get());
		}
		
		if (node->first_node("sex") != NULL && node->first_node("sex")->value_size() > 0)
		{
			m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(node->first_node("sex")->value(), node->first_node("sex")->value_size()));
			newProfile->SetSex(static_cast<Sex>(DataTraits<T>::CharArrayToInt(m_memoryHandler.Get())));
		}
		
		if (node->first_node("bdate") != NULL && node->first_node("bdate")->value_size() > 0)
		{
			m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(node->first_node("bdate")->value(), node->first_node("bdate")->value_size()));
			newProfile->SetBirthDate(m_memoryHandler.Get());
		}
		
		if (node->first_node("city") != NULL && node->first_node("city")->value_size() > 0)
		{
			m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(node->first_node("city")->value(), node->first_node("city")->value_size()));
			newProfile->SetCity(m_memoryHandler.Get());
		}
		
		if (node->first_node("country") != NULL && node->first_node("country")->value_size() > 0)
		{
			m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(node->first_node("country")->value(), node->first_node("country")->value_size()));
			newProfile->SetCountry(m_memoryHandler.Get());
		}
		
		if (node->first_node("timezone") != NULL && node->first_node("timezone")->value_size() > 0)
		{
			m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(node->first_node("timezone")->value(), node->first_node("timezone")->value_size()));
			newProfile->SetTimezone(m_memoryHandler.Get());
		}
		
		if (node->first_node("photo") != NULL)
		{
			m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(node->first_node("photo")->value(), node->first_node("photo")->value_size()));
			newProfile->SetPhoto(m_memoryHandler.Get());
		}
		
		if (node->first_node("photo_medium") != NULL)
		{
			m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(node->first_node("photo_medium")->value(), node->first_node("photo_medium")->value_size()));
			newProfile->SetPhotoMedium(m_memoryHandler.Get());
		}
		
		if (node->first_node("photo_big") != NULL)
		{
			m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(node->first_node("photo_big")->value(), node->first_node("photo_big")->value_size()));
			newProfile->SetPhotoBig(m_memoryHandler.Get());
		}
		
		if (node->first_node("has_mobile") != NULL)
		{
			m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(node->first_node("has_mobile")->value(), node->first_node("has_mobile")->value_size()));
			newProfile->SetMobileExistence(DataTraits<T>::CharArrayToInt(m_memoryHandler.Get()));
		}
		
		if (node->first_node("rate") != NULL)
		{
			m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(node->first_node("rate")->value(), node->first_node("rate")->value_size()));
			newProfile->SetRate(DataTraits<T>::CharArrayToInt(m_memoryHandler.Get()));
		}
		
		if (node->first_node("mobile_phone") != NULL && node->first_node("mobile_phone")->value_size() > 0)
		{
			m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(node->first_node("mobile_phone")->value(), node->first_node("mobile_phone")->value_size()));
			newProfile->SetMobilePhone(m_memoryHandler.Get());
		}
		
		if (node->first_node("home_phone") != NULL && node->first_node("home_phone")->value_size() > 0)
		{
			m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(node->first_node("home_phone")->value(), node->first_node("home_phone")->value_size()));
			newProfile->SetHomePhone(m_memoryHandler.Get());
		}
		
		if (node->first_node("university") != NULL && node->first_node("university")->value_size() > 0)
		{
			m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(node->first_node("university")->value(), node->first_node("university")->value_size()));
			newProfile->SetUniversity(m_memoryHandler.Get());
		}
		
		if (node->first_node("university_name") != NULL && node->first_node("university_name")->value_size() > 0)
		{
			m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(node->first_node("university_name")->value(), node->first_node("university_name")->value_size()));
			newProfile->SetUniversityName(m_memoryHandler.Get());
		}
		
		if (node->first_node("faculty") != NULL && node->first_node("faculty")->value_size() > 0)
		{
			m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(node->first_node("faculty")->value(), node->first_node("faculty")->value_size()));
			newProfile->SetFaculty(m_memoryHandler.Get());
		}
		
		if (node->first_node("faculty_name") != NULL && node->first_node("faculty_name")->value_size() > 0)
		{
			m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(node->first_node("faculty_name")->value(), node->first_node("faculty_name")->value_size()));
			newProfile->SetFacultyName(m_memoryHandler.Get());
		}
		
		if (node->first_node("graduation") != NULL && node->first_node("graduation")->value_size() > 0)
		{
			m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(node->first_node("graduation")->value(), node->first_node("graduation")->value_size()));
			newProfile->SetGraduation(m_memoryHandler.Get());
		}
		
		if (node->first_node("online") != NULL)
		{
			m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(node->first_node("online")->value(), node->first_node("online")->value_size()));
			newProfile->SetOnline(DataTraits<T>::CharArrayToInt(m_memoryHandler.Get()));
		}
		
		if (node->first_node("counters") != NULL)
			newProfile->SetCounter(*(ParseUserCounter(node)));
		
		return newProfile;
	}
	
	template<DataType T> const vector<Profile<T> > *ResponseParser<T>::ParseProfiles()
	{
		vector<Profile<T> > *profiles = new vector<Profile<T> >();
		xml_node<char> *userNode = NULL;
		
		if (m_xmlDoc.first_node()->first_node("profiles") != NULL)
			userNode = m_xmlDoc.first_node()->first_node("profiles")->first_node();
		else if (strcmp(m_xmlDoc.first_node()->first_node()->name(), "count") == 0)
			userNode = m_xmlDoc.first_node()->first_node()->next_sibling();
		else
			userNode = m_xmlDoc.first_node()->first_node();

		while (userNode != NULL)
		{
			profiles->push_back(*(ParseProfile(userNode)));
			userNode = userNode->next_sibling();
		}

		return profiles;
	}
	
	template<DataType T> const RepostsInfo *ResponseParser<T>::ParseRepostsInfo(const xml_node<char> *repostsInfoNode)
	{
		RepostsInfo *repostsInfo = new RepostsInfo;
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(repostsInfoNode->first_node("count")->value(), repostsInfoNode->first_node("count")->value_size()));
		repostsInfo->SetCount(DataTraits<T>::CharArrayToInt(m_memoryHandler.Get()));
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(repostsInfoNode->first_node("user_reposted")->value(), repostsInfoNode->first_node("user_reposted")->value_size()));
		repostsInfo->SetUserReposted(DataTraits<T>::CharArrayToInt(m_memoryHandler.Get()));
		
		return repostsInfo;
	}
	
	template<DataType T> const Status<T> *ResponseParser<T>::ParseStatus(const xml_node<char> *node)
	{
		Status<T> *status = new Status<T>();
		
		//Нижние поля парсим без проверки, так как возвращаются в любом случае.
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(node->first_node("uid")->value(), node->first_node("uid")->value_size()));
		status->SetUserId(m_memoryHandler.Get());
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(node->first_node("friend_status")->value(), node->first_node("friend_status")->value_size()));
		status->Set((FriendStatus)DataTraits<T>::CharArrayToInt(m_memoryHandler.Get()));
		
		if (node->first_node("request_message") != NULL && node->first_node("request_message")->value_size() > 0)
		{
			m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(node->first_node("request_message")->value(), node->first_node("request_message")->value_size()));
			status->SetRequestMessage(m_memoryHandler.Get());
		}
		
		return status;
	}
	
	template<DataType T> const vector<Status<T> > *ResponseParser<T>::ParseStatuses()
	{
		vector<Status<T> > *statuses = new vector<Status<T> >();
		
		xml_node<char> *statusNode = NULL;
		
		if (strcmp(m_xmlDoc.first_node()->first_node()->name(), "count") == 0)
			statusNode = m_xmlDoc.first_node()->first_node()->next_sibling();
		else
			statusNode = m_xmlDoc.first_node()->first_node();

		while (statusNode != NULL)
		{
			statuses->push_back(*(ParseStatus(statusNode)));
			statusNode = statusNode->next_sibling();
		}

		return statuses;
	}
	
	template<DataType T> const typename ResponseParser<T>::String *ResponseParser<T>::ParseString(const char *nodeName)
	{
		String *str = new String();
		const xml_node<char> *node = NULL;
		
		if (nodeName != NULL)
			node = FindSpecifiedNode(m_xmlDoc.first_node(), nodeName);
		else
			node = m_xmlDoc.first_node();
		
		if (node != NULL && node->value_size() > 0)
		{
			const String *nodeConvertedValue = Converter<T>::CharArrayToString(node->value());
			str->append(*nodeConvertedValue);
			delete nodeConvertedValue;
		}
		
		return str;
	}
	
	template<DataType T> const vector<typename ResponseParser<T>::String> *ResponseParser<T>::ParseStringArray()
	{
		vector<String> *userIds = new vector<String>();
		xml_node<char> *node = NULL;
		
		if (strcmp(m_xmlDoc.first_node()->first_node()->name(), "count") == 0)
		{
			if (strcmp(m_xmlDoc.first_node()->first_node()->next_sibling()->name(), "users") == 0)
				node = m_xmlDoc.first_node()->first_node()->next_sibling()->first_node();
			else
				node = m_xmlDoc.first_node()->first_node()->next_sibling();
		}
		else
			node = m_xmlDoc.first_node()->first_node();
		
		while (node != NULL)
		{
			userIds->push_back(Converter<T>::Utf8ToCharArray(node->value(), node->value_size()));
			node = node->next_sibling();
		}
			
		return userIds;
	}
	
	template<DataType T> const TagsInfo *ResponseParser<T>::ParseTagsInfo(const xml_node<char> *tagsNode)
	{
		TagsInfo *tagsInfo = new TagsInfo();
		
		m_memoryHandler.Reset(Converter<T>::Utf8ToCharArray(tagsNode->first_node("count")->value(), tagsNode->first_node("count")->value_size()));
		tagsInfo->SetCount(DataTraits<T>::CharArrayToInt(m_memoryHandler.Get()));
		
		return tagsInfo;
	}
	
	template<DataType T> void ResponseParser<T>::SetBuffer(const String &xmlBuffer)
	{
		try
		{
			if (!xmlBuffer.empty())
			{
				m_xmlBuffer.Reset(Converter<T>::StringToCharArray(xmlBuffer));
				m_xmlDoc.clear();
				m_xmlDoc.template parse<parse_default>(m_xmlBuffer.Get());
			}
		}
		catch(parse_error &exc)
		{
			ResponseParsingException parsingException(-1, exc.what());
			throw parsingException;
		}
	}
}
