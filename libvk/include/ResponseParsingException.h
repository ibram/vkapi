﻿#pragma once
#include "BaseException.h"

namespace sonapi
{
	class ResponseParsingException: public BaseException
	{
	public:
		ResponseParsingException(int, const char*);
	};
}