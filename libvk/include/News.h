#pragma once
#include "CommentsInfo.h"
#include "LikesInfo.h"
#include "NewsType.h"
#include "Photo.h"
#include "Place.h"
#include "RepostsInfo.h"

namespace sonapi
{
	template<DataType T> class News
	{
		typedef typename DataTraits<T>::String String;

	private:
		CommentsInfo m_commentsInfo;
		LikesInfo m_likesInfo;
		vector<Photo<T> > m_photos;
		Place<T> m_place;
		RepostsInfo m_repostsInfo;
		vector<Attachment<T> > m_attachments;
		String m_copyOwnerId, m_copyPostId, m_postId, m_sourceId, m_text;
		time_t m_date, m_copyPostDate;
		NewsType m_type;

	public:
		const vector<Attachment<T> > &GetAttachments() const;
		const CommentsInfo &GetCommentsInfo() const;
		const String &GetCopyOwnerId() const;
		const String &GetCopyPostId() const;
		time_t GetCopyPostDate() const;
		time_t GetDate() const;
		const LikesInfo &GetLikesInfo() const;
		const vector<Photo<T> > &GetPhotos() const;
		const Place<T> &GetPlace() const;
		const String &GetPostId() const;
		const RepostsInfo &GetRepostsInfo() const;
		const String &GetSourceId() const;
		const String &GetText() const;
		const NewsType &GetType() const;
		void SetAttachments(const vector<Attachment<T> >&);
		void SetCommentsInfo(const CommentsInfo&);
		void SetCopyOwnerId(const String&);
		void SetCopyPostDate(time_t);
		void SetCopyPostId(const String&);
		void SetDate(time_t date);
		void SetLikesInfo(const LikesInfo&);
		void SetPhotos(const vector<Photo<T> >&);
		void SetPlace(const Place<T>&);
		void SetPostId(const String&);
		void SetRepostsInfo(const RepostsInfo&);
		void SetSourceId(const String &sourceId);
		void SetText(const String &text);
		void SetType(const NewsType type);
	};
	
	template <DataType T> const vector<Attachment<T> > &News<T>::GetAttachments() const
	{
		return m_attachments;
	}
	
	template<DataType T> const CommentsInfo &News<T>::GetCommentsInfo() const
	{
		return m_commentsInfo;
	}
	
	template<DataType T> const typename News<T>::String &News<T>::GetCopyOwnerId() const
	{
		return m_copyOwnerId;
	}
	
	template<DataType T> time_t News<T>::GetCopyPostDate() const
	{
		return m_copyPostDate;
	}
	
	template<DataType T> const typename News<T>::String &News<T>::GetCopyPostId() const
	{
		return m_copyPostId;
	}
	
	template<DataType T> time_t News<T>::GetDate() const
	{
		return m_date;
	}
	
	template<DataType T> const LikesInfo &News<T>::GetLikesInfo() const
	{
		return m_likesInfo;
	}
	
	template<DataType T> const vector<Photo<T> > &News<T>::GetPhotos() const
	{
		return m_photos;
	}
	
	template<DataType T> const typename News<T>::String &News<T>::GetPostId() const
	{
		return m_postId;
	}
	
	template<DataType T> const RepostsInfo &News<T>::GetRepostsInfo() const
	{
		return m_repostsInfo;
	}
	
	template<DataType T> const typename News<T>::String &News<T>::GetSourceId() const
	{
		return m_sourceId;
	}
	
	template<DataType T> const typename News<T>::String &News<T>::GetText() const
	{
		return m_text;
	}
	
	template<DataType T> const NewsType &News<T>::GetType() const
	{
		return m_type;
	}
	
	template<DataType T> void News<T>::SetAttachments(const vector<Attachment<T> > &attachments)
	{
		m_attachments = attachments;
	}
	
	template<DataType T> void News<T>::SetCommentsInfo(const CommentsInfo &commentsInfo)
	{
		m_commentsInfo = commentsInfo;
	}
	
	template<DataType T> void News<T>::SetCopyOwnerId(const String &copyOwnerId)
	{
		m_copyOwnerId = copyOwnerId;
	}
	
	template<DataType T> void News<T>::SetCopyPostDate(time_t copyPostDate)
	{
		m_copyPostDate = copyPostDate;
	}
	
	template<DataType T> void News<T>::SetCopyPostId(const String &copyPostId)
	{
		m_copyPostId = copyPostId;
	}
	
	template<DataType T> void News<T>::SetDate(time_t date)
	{
		m_date = date;
	}
	
	template<DataType T> void News<T>::SetLikesInfo(const LikesInfo &likesInfo)
	{
		m_likesInfo = likesInfo;
	}
	
	template<DataType T> void News<T>::SetPhotos(const vector<Photo<T> > &photos)
	{
		m_photos = photos;
	}
	
	template<DataType T> void News<T>::SetPlace(const Place<T> &place)
	{
		m_place = place;
	}
	
	template<DataType T> void News<T>::SetPostId(const String &postId)
	{
		m_postId = postId;
	}
	
	template<DataType T> void News<T>::SetRepostsInfo(const RepostsInfo &repostsInfo)
	{
		m_repostsInfo = repostsInfo;
	}
	
	template<DataType T> void News<T>::SetSourceId(const String &sourceId)
	{
		m_sourceId = sourceId;
	}
	
	template<DataType T> void News<T>::SetText(const String &text)
	{
		m_text = text;
	}
	
	template<DataType T> void News<T>::SetType(const NewsType type)
	{
		m_type = type;
	}
}
