#pragma once
#include <cstdlib>

namespace sonapi
{
	class TagsInfo
	{
	private:
		size_t m_count;

	public:
		size_t GetCount() const;
		void SetCount(size_t);
	};
}
