#pragma once
#include <Encoder.h>
#include <sstream>
#include "ApplicationAccess.h"
#include "AudioSortType.h"
#include "CommentsSortType.h"
#include "FriendStatus.h"
#include "GroupType.h"
#include "HTTPConnection.h"
#include "LikeType.h"
#include "ListOrder.h"
#include "Media.h"
#include "NameCase.h"
#include "NotesSortType.h"
#include "ResponseParsingException.h"
#include "ResponseParser.h"
#include "Service.h"
#include "UsersSortType.h"
#include "VideoSortType.h"
#include "VideoWidth.h"
#include "WallPostFilter.h"
using namespace std;
using namespace sonapi;

/**
* \brief Пространство имен соцсети ВКонтакте.
* \details Содержит необходимые члены для работы с официальным API социальной сети.
*/
namespace sonapi
{
	/**
	 * \brief Основной класс клиента. Предоставляет доступ к ВКонтакте API.
	 */
	template<DataType T> class VKClient
	{
	private:
		typedef typename DataTraits<T>::Char Char;
		typedef typename DataTraits<T>::String String;
		typedef typename DataTraits<T>::OStream OStream;
		typedef typename DataTraits<T>::IStringStream IStringStream;
		typedef typename DataTraits<T>::OStringStream OStringStream;
		
		String m_accessToken, m_apiServer, m_applicationId, m_captchaSid, m_id, m_hash, m_installationLink, m_pageLink;
		bool m_autoInstall;
		AutoPointer<const Char> m_memoryHandler;
		AutoPointer<const String, ObjectDeleter<const String> > m_stringHandler;
		HTTPConnection<T> *m_connection;
		ResponseParser<T> *m_responseParser;
		OStringStream *m_responseStream;

		void ActivateDesktopApp(const String&, ApplicationAccess);
		void SendAPIRequest(const String&, const String& = TEXT(""));

	public:
		VKClient(HTTPConnection<T>*);
		~VKClient();
		
		void AllowInstall(bool);
		const String *MediaAlbumAdd(const String&, const String& = TEXT(""), bool = false);
		bool AudioDelete(const String&, const String&);
		bool MediaAlbumDelete(const String&, const String& = TEXT(""), bool = false);
		bool AudioEdit(const String&, const String&, const String&, const String&, const String& = TEXT(""), bool = false);
		bool MediaAlbumEdit(const String&, const String&, const String& = TEXT(""), bool = false);
		const vector<MediaAlbum<T> > *MediaAlbumGet(const String& = TEXT(""), bool = false, size_t = 0, size_t = 0, bool = false);
		const vector<Audio<T> > *AudioGetById(const String*, const String*, size_t);
		const String *AudioGetLyrics(const String&);
		const String *AudioGetUploadServer();
		bool AudioMoveToAlbum(const String*, size_t, const String&, const String& = TEXT(""), bool = false);
		bool AudioReorder(const String&, const String&, const String&, const String& = TEXT(""));
		const Audio<T> *AudioRestore(const String&, const String& = TEXT(""));
		const Audio<T> *AudioSave(const String&, const String&, const String&, const String& = TEXT(""), const String& = TEXT(""));
		const vector<Audio<T> > *AudioSearch(const String&, bool = true, AudioSortType = AudioSortType_Popularity,
			bool = false, size_t = 0, size_t = 0);
		const Album<T> *CreateAlbum(const String&, AccessPrivacy = AccessPrivacy_AllUsers, AccessPrivacy = AccessPrivacy_AllUsers, const String& = TEXT(""), const String& = TEXT(""));
		bool DocsDelete(const String&, const String&);
		const vector<Document<T> > *DocsGet(const String& = TEXT(""), size_t = 0, size_t = 0);
		const vector<Document<T> > *DocsGetById(const String*, const String*, size_t);
		const String *DocsGetUploadServer();
		const Document<T> *DocsSave(const String&);
		bool EditAlbum(const String&, const String&, AccessPrivacy = AccessPrivacy_MeOnly, AccessPrivacy = AccessPrivacy_MeOnly, const String& = TEXT(""));
		bool EditPhoto(const String&, const String&, const String&);
		const String &GetAccessToken() const;
		const vector<Album<T> > *GetAlbums(const String&, bool = false, const String* = NULL, size_t = 0);
		const vector<Photo<T> > *GetAllPhotos(const String&, bool = true, size_t = 0, size_t = 100);
		const String &GetAPIServer() const;
		const String &GetApplicationId() const;
		const vector<String> *GetAppUsers();
		const vector<Audio<T> > *GetAudios(const String&, bool, const String&, const String*, size_t, bool = false, size_t = 0, size_t = 0);
		const vector<Profile<T> > *GetFriends(const String& = TEXT(""), NameCase = NameCase_Nominative, size_t = 0, size_t = 0, const String& = TEXT(""), ListOrder = ListOrder_Name);
		const vector<Status<T> > *GetFriendStatuses(const String*, size_t);
		const vector<String> *GetGroupMembers(const String&, size_t = 0, size_t = 0, UsersSortType = UsersSortType_Id_Ascending);
		const vector<Group<T> > *GetGroups(const String& = TEXT(""), GroupType = GroupType_All, size_t = 0, size_t = 0);
		const vector<Group<T> > *GetGroupsById(const String*, size_t);
		const String &GetId() const;
		const String &GetInstallationLink() const;
		const vector<String> *GetLikedUsers(LikeType, const String&, const String&, const String&, bool = true, bool = false, size_t = 0, size_t = 0);
		const vector<String> *GetMutualFriends(const String&, const String&);
		const NewsFeed<T> *GetNewsFeed(const String* = NULL, size_t = 0, const String* = NULL, size_t = 0, NewsType = NewsType_All, time_t = 0, time_t = 0, size_t = 0, const String& = TEXT(""), size_t = 50, size_t = 5);
		const vector<String> *GetOnlineFriends(const String&);
		const String &GetPageLink() const;
		const vector <Photo<T> > *GetPhotos(const String&, bool, const String&, const String* = NULL, size_t = 0, size_t = 0, size_t = 0, time_t = 0, NewsType = NewsType_Photo);
		const vector<Photo<T> > *GetPhotosById(const String*, const String*, size_t);
		const String *GetPhotoUploadServer(const String&, bool = false);
		const String *GetPhotoWallUploadServer(const String& = TEXT(""), bool = false);
		const vector<Photo<T> > *GetProfilePhotos(const String&, bool = 0, size_t = 0, size_t = 0);
		const String *GetProfileUploadServer();
		time_t GetServerTime();
		float GetUserBalance();
		ApplicationAccess GetUserRights(const String& = TEXT(""));
		const vector<Profile<T> > *GetUsers(const String* = NULL, size_t = 1, NameCase = NameCase_Nominative);
		const vector<Post<T> > *GetWall(const String& = TEXT(""), size_t = 0, size_t = 0, WallPostFilter = WallPostFilter_All);
		const vector<Comment<T> > *GetWallComments(const String&, const String&, CommentsSortType = CommentsSortType_Ascending,
			size_t = 0, size_t = 0, size_t = 0);
		bool IsAppUser(const String& = TEXT(""));
		bool IsGroupMember(const String&, const String& = TEXT(""));
		bool IsInstallAllowed() const;
		bool LogIn(const String&, const String&, ApplicationAccess = ApplicationAccess_FullAccess, const String& = TEXT(""));
		bool LogIn(const String&, const String&, const String&, ApplicationAccess = ApplicationAccess_FullAccess, const String& = TEXT(""));
		void LogOut();
		bool MakeAlbumCover(const String&, const String&, const String& = TEXT(""));
		bool MovePhoto(const String&, const String&, const String&);
		const String *NotesAdd(const String&, const String&, AccessPrivacy = AccessPrivacy_MeOnly,
			AccessPrivacy = AccessPrivacy_MeOnly);
		bool NotesDelete(const String&);
		bool NotesEdit(const String&, const String&, const String&, AccessPrivacy = AccessPrivacy_MeOnly,
			AccessPrivacy = AccessPrivacy_MeOnly);
		const vector<Note<T> > *NotesGet(const String*, size_t, const String& = TEXT(""),
			NotesSortType = NotesSortType_DateDescending, size_t = 100, size_t = 0);
		const vector<Comment<T> > *NotesGetComments(const String&, const String& = TEXT(""),
			CommentsSortType = CommentsSortType_Ascending, size_t = 100, size_t = 0);
		const Note<T> *NotesGetById(const String&, const String& = TEXT(""), bool = false);
		const vector<Note<T> > *NotesGetFriendNotes(size_t = 100, size_t = 0);
		bool ReorderAlbums(const String&, const String&, const String&, const String& = TEXT(""));
		bool ReorderPhotos(const String&, const String&, const String&, const String& = TEXT(""));
		const vector<Photo<T> > *SavePhotos(const String&, bool, const String&, const String&, const String&, const String& = TEXT(""));
		void SaveProfilePhoto(const String&, const String&, const String&);
		const Photo<T> *SaveWallPhoto(const String&, const String&, const String&, const String&, bool = false);
		const vector<Group<T> > *SearchGroups(const String&, size_t = 0, size_t = 0);
		const vector<News<T> > *SearchNews(const String&, size_t = 0, size_t = 0, time_t = 0, time_t = 0, const String& = TEXT(""));
		const vector<Profile<T> > *SearchUsers(const String&, size_t = 0, size_t = 0);
		void SetAPIServer(const String&);
		void SetApplicationId(const String&);
		void SetConnection(HTTPConnection<T>*);
		const String *VideoAdd(const String&, const String&);
		const String *VideoCreateComment(const String&, const String&, const String& = TEXT(""));
		bool VideoDelete(const String&, const String&);
		bool VideoDeleteComment(const String&, const String& = TEXT(""));
		bool VideoEdit(const String&, const String&, const String&, const String& = TEXT(""),
			AccessPrivacy = AccessPrivacy_FriendsOnly, AccessPrivacy = AccessPrivacy_FriendsOnly);
		bool VideoEditComment(const String&, const String&, const String& = TEXT(""));
		const vector<Video<T> > *VideoGet(const String&, bool = false, const String& = TEXT(""), VideoWidth = VideoWidth_160,
			size_t = 0, size_t = 0);
		const vector<Comment<T> > *VideoGetComments(const String&, const String& = TEXT(""), size_t = 0, size_t = 0,
			CommentsSortType = CommentsSortType_Ascending);
		const vector<VideoTag<T> >*VideoGetTags(const String&, const String& = TEXT(""));
		const vector<Video<T> > *VideoGetUserVideos(const String& = TEXT(""), size_t = 0, size_t = 0);
		const String *VideoPutTag(const String&, const String&, const String& = TEXT(""));
		bool VideoRemoveTag(const String&, const String&, const String& = TEXT(""));
		const String *VideoSave(const String&, const String&, const String& = TEXT(""),
			AccessPrivacy = AccessPrivacy_FriendsOnly, AccessPrivacy = AccessPrivacy_FriendsOnly);
		const vector<Video<T> > *VideoSearch(const String&, VideoSortType = VideoSortType_Relevance,
			bool = false, size_t = 0, size_t = 0);
		const vector<Post<T> > *WallGetById(const String*, const String*, size_t);
		const String *WallPost(const String&, const String&, const Media<T>*, size_t, const Place<T>&, Service,
			bool = false, bool = 0, bool = 1);
	};

	/**
	 * \brief Конструктор класса.
	 * \param connection HTTP соединение, которое будет использоваться клиентом.
	 */
	template<DataType T> VKClient<T>::VKClient(HTTPConnection<T> *connection):
		m_apiServer(TEXT("api.vk.com")), m_applicationId(TEXT("000000"))
	{
		m_autoInstall = true;
		m_responseParser = new ResponseParser<T>();
		m_responseStream = new OStringStream();
		m_connection = connection;
		m_connection->SetResponseStream(m_responseStream);
	}
	
	/// \brief Деструктор класса.
	template<DataType T> VKClient<T>::~VKClient()
	{
		delete m_responseParser;
		delete m_responseStream;
	}
	
	/**
	 * \brief Автоматически добавляет указанное приложение к пользователю и выдает ему указанные права.
	 * \param app_id Идентификатор приложения.
	 * \param access Уровень доступа.
	 * \return true в случае успешного добавления приложения, иначе false.
	 */
	template<DataType T> void VKClient<T>::ActivateDesktopApp(const String &app_id, ApplicationAccess access)
	{
		String authUrl(TEXT("http://oauth.vk.com/oauth/authorize?client_id=") + app_id);
		authUrl += TEXT("&scope="); authUrl += DataTraits<T>::IntToCharArray(access);
		authUrl += TEXT("&redirect_uri=http://api.vk.com/blank.html"
				"&display=popup"
				"&response_type=token");
		
		try
		{
			m_responseStream->str(TEXT(""));
			m_connection->Get(authUrl);
		}
		catch (ConnectionFailException &exc)
		{
			throw exc;
		}
		
		String response = m_responseStream->str();
		
		/*
		 * Имитация разрешения доступа приложению.
		 * Выдергиваем из javascript ссылку на разрешение и переходим по ней.
		 * Если юзер уже разрешал, то получаем взамен страницу Login success
		 */
		if (m_autoInstall == true)
		{
			if (response.find(TEXT("Login success")) == String::npos)
			{
				int start = response.find(TEXT("function allow()")) + 139;
				int end = response.find(TEXT("return")) - 12;
				String url = response.substr(start, end - start);
				
				try
				{
					m_responseStream->str(TEXT(""));
					m_connection->Get(url);
				}
				catch (ConnectionFailException &exc)
				{
					throw exc;
				}
			}
		
			response = m_responseStream->str();

			//Вычислим положение и длину access_token и извлечем его.
			String redirectionUrl = m_connection->GetConnectionInfo().GetLastUsedURL();
			int accessTokenLength = 0;
			int accessTokenStart = redirectionUrl.find(TEXT("access_token")) + 13;

			for (size_t i = accessTokenStart; i < redirectionUrl.size(); i++)
			{
				if (redirectionUrl[i] != '&')
					accessTokenLength++;
				else break;
			}

			m_accessToken = redirectionUrl.substr(accessTokenStart, accessTokenLength);
		}
	}

	/**
	 * \brief Указывает, устанавливать ли приложение пользователю автоматически.
	 * \details Автоматическая установка приложения подразумевает собой скрытую операцию
	 * добавления приложения к пользователю с нужными правами без его согласия.
	 * \param allowAutoSetup true - устанавливать автоматически, false - не устанавливать.
	 */
	template<DataType T> void VKClient<T>::AllowInstall(bool allowAutoSetup)
	{
		m_autoInstall = allowAutoSetup;
	}
	
	template<DataType T> const typename VKClient<T>::String *VKClient<T>::MediaAlbumAdd(const String &title,
		const String &groupId, bool isVideo)
	{
		String parameters(TEXT("&title="));
		
		m_stringHandler.Reset(Encoder<T>::EscapeString(title));
		parameters += *m_stringHandler;
		
		parameters += TEXT("&gid="); parameters += groupId;
		
		try
		{
			if (!isVideo)
				SendAPIRequest(TEXT("audio.addAlbum"), parameters);
			else
				SendAPIRequest(TEXT("video.addAlbum"), parameters);
			
			return m_responseParser->ParseString("album_id");
		}
		catch(...)
		{
			throw;
		}
	}
	
	template<DataType T> bool VKClient<T>::AudioDelete(const String &audioId, const String &ownerId)
	{
		String parameters(TEXT("&aid=") + audioId +
			TEXT("&oid=") + ownerId);
			
		try
		{
			SendAPIRequest(TEXT("audio.delete"), parameters);
			return m_responseParser->ParseBoolean();
		}
		catch(...)
		{
			throw;
		}
	}
	
	template<DataType T> bool VKClient<T>::MediaAlbumDelete(const String &albumId, const String &groupId, bool isVideo)
	{
		try
		{
			if (!isVideo)
				SendAPIRequest(TEXT("audio.deleteAlbum"), TEXT("&album_id=") + albumId +
					TEXT("&gid=") + groupId);
			else
				SendAPIRequest(TEXT("video.deleteAlbum"), TEXT("&album_id=") + albumId +
					TEXT("&gid=") + groupId);
			
			return m_responseParser->ParseBoolean();
		}
		catch(...)
		{
			throw;
		}
	}
	
	template<DataType T> bool VKClient<T>::AudioEdit(const String &audioId, const String &ownerId, const String &newArtist,
		const String &newTitle, const String &newText, bool hideFromSearch)
	{
		String parameters(TEXT("&aid=") + audioId +
			TEXT("&oid=") + ownerId +
			TEXT("&artist=") + newArtist +
			TEXT("&title=") + newTitle +
			TEXT("&text=") + newText);
			
		if (hideFromSearch)
			parameters += TEXT("&no_search=1");
		else
			parameters += TEXT("&no_search=0");
			
		try
		{
			SendAPIRequest(TEXT("audio.edit"), parameters);
			return m_responseParser->ParseBoolean();
		}
		catch(...)
		{
			throw;
		}
	}
	
	template<DataType T> bool VKClient<T>::MediaAlbumEdit(const String &newTitle, const String &albumId,
		const String &groupId, bool isVideo)
	{
		String parameters(TEXT("&title="));
		
		m_stringHandler.Reset(Encoder<T>::EscapeString(newTitle));
		parameters += *m_stringHandler;
		
		parameters += TEXT("&album_id="); parameters += albumId;
		parameters += TEXT("&gid="); parameters += groupId;
		
		try
		{
			if (!isVideo)
				SendAPIRequest(TEXT("audio.editAlbum"), parameters);
			else
				SendAPIRequest(TEXT("video.editAlbum"), parameters);
			
			return m_responseParser->ParseBoolean();
		}
		catch(...)
		{
			throw;
		}
	}
	
	template<DataType T> const vector<MediaAlbum<T> > *VKClient<T>::MediaAlbumGet(const String &objectId, bool isGroup, size_t count,
		size_t offset, bool isVideo)
	{
		if (count > 100)
			count = 100;
		
		String parameters(TEXT("&"));
		
		if (!isGroup)
			parameters += TEXT("uid=");
		else
			parameters += TEXT("gid=");
		
		parameters += objectId;
		
		parameters += TEXT("&count="); parameters += DataTraits<T>::IntToCharArray(count);
		parameters += TEXT("&offset="); parameters += DataTraits<T>::IntToCharArray(offset);
		
		try
		{
			if (!isVideo)
				SendAPIRequest(TEXT("audio.getAlbums"), parameters);
			else
				SendAPIRequest(TEXT("video.getAlbums"), parameters);
			
			return m_responseParser->ParseAudioAlbums();
		}
		catch(...)
		{
			throw;
		}
	}
	
	template<DataType T> const vector<Audio<T> > *VKClient<T>::AudioGetById(const String *objectIds, const String *audioIds, size_t audiosCount)
	{
		vector<Audio<T> > *audios = new vector<Audio<T> >();
		String parameters(TEXT("&audios="));
		
		if (objectIds != NULL && audioIds != NULL && audiosCount > 0)
		{
			for(size_t i = 0; i < audiosCount; i++)
			{
				parameters += objectIds[i];
				parameters += TEXT("_");
				parameters += audioIds[i];
				
				if (i < audiosCount - 1)
					parameters += TEXT(",");
			}
		}
		
		try
		{
			SendAPIRequest(TEXT("audio.getById"), parameters);
			return m_responseParser->ParseAudios();
		}
		catch(...)
		{
			throw;
		}
		
		return audios;
	}
	
	template<DataType T> const typename VKClient<T>::String *VKClient<T>::AudioGetLyrics(const String &lyricsId)
	{
		String parameters(TEXT("&lyrics_id=") + lyricsId);
		
		try
		{
			SendAPIRequest(TEXT("audio.getLyrics"), parameters);
			return m_responseParser->ParseString("text");
		}
		catch(...)
		{
			throw;
		}
	}
	
	template<DataType T> const typename VKClient<T>::String *VKClient<T>::AudioGetUploadServer()
	{
		try
		{
			SendAPIRequest(TEXT("audio.getUploadServer"));
			return m_responseParser->ParseString("upload_url");
		}
		catch(...)
		{
			throw;
		}
	}
	
	template<DataType T> bool VKClient<T>::AudioMoveToAlbum(const String *objectIds, size_t objectCount,
		const String &albumId, const String &groupId, bool isVideo)
	{
		String parameters(TEXT("&"));
		
		if (!isVideo)
			parameters += TEXT("aids=");
		else
			parameters += TEXT("vids=");
		
		for(size_t i = 0; i < objectCount; i++)
		{
			parameters += objectIds[i];
			
			if (i < objectCount - 1)
				parameters += TEXT(",");
		}
		
		parameters += TEXT("&album_id="); parameters += albumId;
		parameters += TEXT("&gid="); parameters += groupId;
		
		try
		{
			if (!isVideo)
				SendAPIRequest(TEXT("audio.moveToAlbum"), parameters);
			else
				SendAPIRequest(TEXT("video.moveToAlbum"), parameters);
			
			return m_responseParser->ParseBoolean();
		}
		catch(...)
		{
			throw;
		}
	}
	
	template<DataType T> bool VKClient<T>::AudioReorder(const String &audioId, const String &audioAfterId,
		const String &audioBeforeId, const String &ownerId)
	{
		String parameters(TEXT("&aid=") + audioId +
			TEXT("&after=") + audioAfterId +
			TEXT("&before=") + audioBeforeId);
			
		if (!ownerId.empty())
		{
			parameters += TEXT("&oid=");
			parameters += ownerId;
		}
		
		try
		{
			SendAPIRequest(TEXT("audio.reorder"), parameters);
			return m_responseParser->ParseBoolean();
		}
		catch(...)
		{
			throw;
		}
	}
	
	template<DataType T> const Audio<T> *VKClient<T>::AudioRestore(const String &audioId, const String &ownerId)
	{
		String parameters(TEXT("&aid=") + audioId +
			TEXT("&oid=") + ownerId);
			
		try
		{
			SendAPIRequest(TEXT("audio.restore"), parameters);
			return m_responseParser->ParseAudio(NULL);
		}
		catch(...)
		{
			throw;
		}
	}
	
	template<DataType T> const Audio<T> *VKClient<T>::AudioSave(const String &server, const String &audio, const String &hash,
		const String &artist, const String &title)
	{
		String parameters(TEXT("&server=") + server +
			TEXT("&audio=") + audio +
			TEXT("&hash=") + hash);
			
		if (!artist.empty())
		{
			parameters += TEXT("&artist=");
			parameters += artist;
		}
		
		if (!title.empty())
		{
			parameters += TEXT("&title=");
			parameters += title;
		}
		
		try
		{
			SendAPIRequest(TEXT("audio.save"), parameters);
			return m_responseParser->ParseAudio(NULL);
		}
		catch(...)
		{
			throw;
		}
	}
	
	template<DataType T> const vector<Audio<T> > *VKClient<T>::AudioSearch(const String &query, bool autoComplete, AudioSortType sortType,
			bool withLyrics, size_t count, size_t offset)
	{
		if (count > 200)
			count = 200;
		
		String parameters(TEXT("&q=") + query);
		
		if (autoComplete)
			parameters += TEXT("&auto_complete=1");
		else
			parameters += TEXT("&auto_complete=0");
			
		parameters += TEXT("&sort=");
		parameters += DataTraits<T>::IntToCharArray(static_cast<int>(sortType));
		
		if (withLyrics)
			parameters += TEXT("&lyrics=1");
		else
			parameters += TEXT("&lyrics=0");
			
		parameters += TEXT("&count="); parameters += DataTraits<T>::IntToCharArray(count);
		parameters += TEXT("&offset="); parameters += DataTraits<T>::IntToCharArray(offset);
		
		try
		{
			SendAPIRequest(TEXT("audio.search"), parameters);
			return m_responseParser->ParseAudios();
		}
		catch(...)
		{
			throw;
		}
	}
	
	template<DataType T> const Album<T> *VKClient<T>::CreateAlbum(const String &title, AccessPrivacy privacyLevel, AccessPrivacy commentPrivacyLevel, const String &description, const String &groupId)
	{
		String parameters(TEXT("&title="));
		
		m_stringHandler.Reset(Encoder<T>::EscapeString(title));
		parameters += *m_stringHandler;
		
		/* Если указана группа, уровень доступа может принимать только два значения и,
		 * если передано недопустимое значение, оно автоматически исправляется на значение,
		 * разрешающее доступ только для участников группы.
		 */
		if (!groupId.empty())
		{
			if (static_cast<int>(privacyLevel) > 1)
				privacyLevel = AccessPrivacy_FriendsOnly;
				
			if (static_cast<int>(commentPrivacyLevel) > 1)
				commentPrivacyLevel = AccessPrivacy_FriendsOnly;
		}
		
		parameters += TEXT("&privacy=");
		parameters += DataTraits<T>::IntToCharArray(static_cast<int>(privacyLevel));
		parameters += TEXT("&comment_privacy=");
		parameters += DataTraits<T>::IntToCharArray(static_cast<int>(commentPrivacyLevel));
		parameters += TEXT("&description="); parameters += description;
		parameters += TEXT("&gid="); parameters += groupId;
		
		try
		{
			SendAPIRequest(TEXT("photos.createAlbum"), parameters);
			return m_responseParser->ParseAlbum(NULL);
		}
		catch(...)
		{
			throw;
		}
	}
	
	template<DataType T> bool VKClient<T>::DocsDelete(const String &documentId, const String &ownerId)
	{
		try
		{
			SendAPIRequest(TEXT("docs.delete"), TEXT("&did=") + documentId +
				TEXT("&oid=") + ownerId);
			return m_responseParser->ParseBoolean();
		}
		catch(...)
		{
			throw;
		}
	}
	
	template<DataType T> const vector<Document<T> > *VKClient<T>::DocsGet(const String &objectId, size_t count, size_t offset)
	{
		String parameters(TEXT("&oid=") + objectId);
		parameters += TEXT("&count="); parameters += DataTraits<T>::IntToCharArray(count);
		parameters += TEXT("&offset="); parameters += DataTraits<T>::IntToCharArray(offset);
		
		try
		{
			SendAPIRequest(TEXT("docs.get"), parameters);
			return m_responseParser->ParseDocuments();
		}
		catch(...)
		{
			throw;
		}
	}
	
	template<DataType T> const vector<Document<T> > *VKClient<T>::DocsGetById(const String *userIds, const String *documentIds,
		size_t documentsCount)
	{
		if (userIds != NULL && documentIds != NULL && documentsCount > 0)
		{
			String parameters(TEXT("&docs="));
			
			for(size_t i = 0; i < documentsCount; i++)
			{
				parameters += userIds[i];
				parameters += TEXT("_");
				parameters += documentIds[i];
				
				if (i < documentsCount - 1)
					parameters += TEXT(",");
			}
			
			try
			{
				SendAPIRequest(TEXT("docs.getById"), parameters);
				return m_responseParser->ParseDocuments();
			}
			catch(...)
			{
				throw;
			}
		}
		
		return NULL;
	}
	
	template<DataType T> const typename VKClient<T>::String *VKClient<T>::DocsGetUploadServer()
	{
		try
		{
			SendAPIRequest(TEXT("docs.getUploadServer"));
			return m_responseParser->ParseString("upload_url");
		}
		catch(...)
		{
			throw;
		}
	}
	
	template<DataType T> const Document<T> *VKClient<T>::DocsSave(const String &file)
	{
		try
		{
			SendAPIRequest(TEXT("docs.save"), TEXT("&file=") + file);
			return m_responseParser->ParseDocument(NULL);
		}
		catch(...)
		{
			throw;
		}
	}
	
	template<DataType T> bool VKClient<T>::EditAlbum(const String &albumId, const String &title, AccessPrivacy albumPrivacy, AccessPrivacy commentPrivacy, const String &description)
	{
		String parameters(TEXT("&aid="));
		parameters += albumId;
		
		parameters += TEXT("&title=");
		
		m_stringHandler.Reset(Encoder<T>::EscapeString(title));
		parameters += *m_stringHandler;
		parameters += TEXT("&privacy="); DataTraits<T>::IntToCharArray(static_cast<int>(albumPrivacy));
		parameters += TEXT("&comment_privacy="); DataTraits<T>::IntToCharArray(static_cast<int>(commentPrivacy));
		parameters += TEXT("&description="); parameters += description;
		
		try
		{
			SendAPIRequest(TEXT("photos.editAlbum"), parameters);
			return m_responseParser->ParseBoolean();
		}
		catch(...)
		{
			throw;
		}
	}
	
	template<DataType T> bool VKClient<T>::EditPhoto(const String &ownerId, const String &photoId, const String &newCaption)
	{
		String parameters(TEXT("&owner_id=") + ownerId +
			TEXT("&pid=") + photoId +
			TEXT("&caption=") + newCaption);
			
		try
		{
			SendAPIRequest(TEXT("photos.edit"), parameters);
			return m_responseParser->ParseBoolean();
		}
		catch(...)
		{
			throw;
		}
	}
	
	/**
	 * \brief Возвращает Access Token, который выдается при успешной авторизации.
	 * \return Access Token.
	 */
	template<DataType T> const typename VKClient<T>::String &VKClient<T>::GetAccessToken() const
	{
		return m_accessToken;
	}
	
	template<DataType T> const vector<Album<T> > *VKClient<T>::GetAlbums(const String &objectId, bool isGroup, const String *albumIds, size_t albumsCount)
	{
		String parameters(TEXT("&"));
		
		if (isGroup)
			parameters += TEXT("gid=");
		else
			parameters += TEXT("uid=");
			
		parameters += objectId;
		
		if (albumIds != NULL && albumsCount > 0)
		{
			parameters += TEXT("&aids=");
			
			for(size_t i = 0; i < albumsCount - 1; i++)
			{
				parameters += albumIds[i];
				parameters += TEXT(",");
			}
			
			parameters += albumIds[albumsCount - 1];
		}
		
		parameters += TEXT("&need_covers=1");
		
		try
		{
			SendAPIRequest(TEXT("photos.getAlbums"), parameters);
			return m_responseParser->ParseAlbums();
		}
		catch(...)
		{
			throw;
		}
	}
	
	template<DataType T> const vector<Photo<T> > *VKClient<T>::GetAllPhotos(const String &ownerId, bool serviceAlbums, size_t offset, size_t count)
	{
		String parameters(TEXT("&owner_id="));
		parameters += ownerId;
		
		if (serviceAlbums)
			parameters += TEXT("&no_service_albums=0");
		else
			parameters += TEXT("&no_service_albums=1");
			
		parameters += TEXT("&offset="); DataTraits<T>::IntToCharArray(offset);
		parameters += TEXT("&count="); DataTraits<T>::IntToCharArray(count);
		parameters += TEXT("&extended=1");
		
		try
		{
			SendAPIRequest(TEXT("photos.getAll"), parameters);
			return m_responseParser->ParsePhotos();
		}
		catch(...)
		{
			throw;
		}
	}
	
	/**
	 * \brief Получает адрес сервера API.
	 * \return Строка, содержащая адрес сервера API.
	 */
	template<DataType T> const typename VKClient<T>::String &VKClient<T>::GetAPIServer() const
	{
		return m_apiServer;
	}
	
	/**
	 * \brief Возвращает идентификатор приложения.
	 * \return Идентификатор приложения.
	 */
	template<DataType T> const typename VKClient<T>::String &VKClient<T>::GetApplicationId() const
	{
		return m_applicationId;
	}
	
	template<DataType T> const vector<typename VKClient<T>::String> *VKClient<T>::GetAppUsers()
	{
		try
		{
			SendAPIRequest(TEXT("friends.getAppUsers"), TEXT(""));
			return m_responseParser->ParseStringArray();
		}
		catch(...)
		{
			throw;
		}
	}
	
	template<DataType T> const vector<Audio<T> > *VKClient<T>::GetAudios(const String &groupOrUserId, bool isGroup, const String &albumId, const String* audioIds, size_t audioIdsCount, bool needUser, size_t count, size_t offset)
	{
		String audio_ids, parameters(TEXT("&"));
		
		//Форимурем список идентификаторов аудио
		if (audioIds != NULL && audioIdsCount != 0)
		{
			for (size_t i = 0; i < audioIdsCount - 1; i++)
				audio_ids += audioIds[i] + TEXT(",");
				
			audio_ids += audioIds[audioIdsCount - 1];
		}
			
		if (!isGroup)
			parameters += TEXT("uid=");
		else
			parameters += TEXT("gid=");
		
		parameters += groupOrUserId;
		parameters += TEXT("&album_id=") + albumId;
		parameters += TEXT("&aids=") + audio_ids;
		
		if (needUser)
			parameters += TEXT("&need_user=1");
		else
			parameters += TEXT("&need_user=0");
		
		parameters += TEXT("&count=");
		parameters += DataTraits<T>::IntToCharArray(count);
		parameters += TEXT("&offset=");
		parameters += DataTraits<T>::IntToCharArray(offset);
		
		try
		{
			SendAPIRequest(TEXT("audio.get"), parameters);
		}
		catch(...)
		{
			throw;
		}
		
		return m_responseParser->ParseAudios();
	}
	
	template<DataType T> const vector<Profile<T> > *VKClient<T>::GetFriends(const String &userId, NameCase nameCase, size_t count, size_t offset, const String &listId, ListOrder listOrder)
	{
		String parameters(TEXT("&uid="));
		
		if (userId.empty())
			parameters += m_id;
		else
			parameters += userId;
			
		parameters += TEXT("&fields=uid,first_name,last_name,nickname,sex,bdate,city,country,"
			"timezone,photo,photo_medium,photo_big,domain,has_mobile,rate,contacts,education");
		parameters += TEXT("&name_case=");
		
		switch (nameCase)
		{
			case NameCase_Nominative:
				parameters += TEXT("nom");
				break;
			case NameCase_Genitive:
				parameters += TEXT("gen");
				break;
			case NameCase_Dative:
				parameters += TEXT("dat");
				break;
			case NameCase_Accusative:
				parameters += TEXT("acc");
				break;
			case NameCase_Instrumental:
				parameters += TEXT("ins");
				break;
			case NameCase_Ablative:
				parameters += TEXT("abl");
				break;
		}
		
		parameters += TEXT("&count="); parameters += DataTraits<T>::IntToCharArray(count);
		parameters += TEXT("&offset="); parameters += DataTraits<T>::IntToCharArray(offset);
		
		parameters += TEXT("&lid=") + listId;
		parameters += TEXT("&order=");
		
		if (listOrder == ListOrder_Name)
			parameters += TEXT("name");
		else
			parameters += TEXT("hints");
			
		try
		{
			SendAPIRequest(TEXT("friends.get"), parameters);
			return m_responseParser->ParseProfiles();
		}
		catch(...)
		{
			throw;
		}
	}
	
	template<DataType T> const vector<Status<T> > *VKClient<T>::GetFriendStatuses(const String *userIds, size_t usersCount)
	{
		if (userIds == NULL || usersCount == 0)
			return NULL;
			
		String parameters(TEXT("&uids="));
		
		for (size_t i = 0; i < usersCount - 1; i++)
		{
			parameters += userIds[i];
			parameters += TEXT(",");
		}
		
		parameters += userIds[usersCount - 1];
		
		try
		{
			SendAPIRequest(TEXT("friends.areFriends"), parameters);
			return m_responseParser->ParseStatuses();
		}
		catch(...)
		{
			throw;
		}
	}
	
	template<DataType T> const vector<typename VKClient<T>::String> *VKClient<T>::GetGroupMembers(const String &groupId, size_t count, size_t offset, UsersSortType sortType)
	{
		if (count > 1000)
			count = 1000;
			
		String parameters(TEXT("&gid="));
		
		parameters += groupId;
		parameters += TEXT("&count="); parameters += DataTraits<T>::IntToCharArray(count);
		parameters += TEXT("&offset="); parameters += DataTraits<T>::IntToCharArray(offset);
		parameters += TEXT("&sort=");
		
		switch(sortType)
		{
			case UsersSortType_Id_Ascending:
				parameters += TEXT("id_asc");
				break;
				
			case UsersSortType_Id_Descending:
				parameters += TEXT("id_desc");
				break;
				
			case UsersSortType_Time_Ascending:
				parameters += TEXT("time_asc");
				break;
				
			case UsersSortType_Time_Descending:
				parameters += TEXT("time_desc");
				break;
		}
		
		try
		{
			SendAPIRequest(TEXT("groups.getMembers"), parameters);
			return m_responseParser->ParseStringArray();
		}
		catch(...)
		{
			throw;
		}
	}
	
	template<DataType T> const vector<Group<T> > *VKClient<T>::GetGroups(const String &userId, GroupType groupType, size_t offset, size_t count)
	{
		String parameters(TEXT("&uid="));
		parameters += userId;
		parameters += TEXT("&extended=1");
		parameters += TEXT("&filter=");
		parameters += TEXT("&fields=city,counters,country,end_date,description,members_count,place,start_date,wiki_page");
		
		switch(groupType)
		{
			case GroupType_Admin:
				parameters += TEXT("admin");
				break;
				
			case GroupType_Event:
				parameters += TEXT("events");
				break;
				
			case GroupType_Group:
				parameters += TEXT("groups");
				break;
				
			case GroupType_Public:
				parameters += TEXT("publics");
				break;
				
			case GroupType_All:
				break;
		}
		
		if (count > 1000)
			count = 1000;
		
		parameters += TEXT("&count="); parameters += DataTraits<T>::IntToCharArray(count);
		parameters += TEXT("&offset="); parameters += DataTraits<T>::IntToCharArray(offset);
		
		try
		{
			SendAPIRequest(TEXT("groups.get"), parameters);
			return m_responseParser->ParseGroups();
		}
		catch(...)
		{
			throw;
		}
	}
	
	template<DataType T> const vector<Group<T> > *VKClient<T>::GetGroupsById(const String *userIds, size_t usersCount)
	{
		if (userIds == NULL || usersCount < 1)
			return NULL;
		
		if (usersCount > 500)
			usersCount = 500;
		
		String parameters(TEXT("&gids="));
		
		for (size_t i = 0; i < usersCount - 1; i++)
		{
			parameters += userIds[i];
			parameters += TEXT(",");
		}
		
		parameters += userIds[usersCount - 1];
	
		try
		{
			SendAPIRequest(TEXT("groups.getById"), parameters);
			return m_responseParser->ParseGroups();
		}
		catch(...)
		{
			throw;
		}
	}
	
	/**
	 * \brief Возвращает идентификатор текущего пользователя.
	 * \return Идентификатор текущего пользователя.
	 */
	template<DataType T> const typename VKClient<T>::String &VKClient<T>::GetId() const
	{
		return m_id;
	}
	
	template<DataType T> const typename VKClient<T>::String &VKClient<T>::GetInstallationLink() const
	{
		return m_installationLink;
	}
	
	template<DataType T> const vector<typename VKClient<T>::String> *VKClient<T>::GetLikedUsers(LikeType likeType, const String &ownerId, const String &itemId, const String &pageURL, bool allUsers, bool friendsOnly, size_t count, size_t offset)
	{
		String like_type, parameters;
		
		switch(likeType)
		{
			case LikeType_Audio:
				like_type = TEXT("audio");
				break;
			case LikeType_Comment:
				like_type = TEXT("comment");
				break;
			case LikeType_Note:
				like_type = TEXT("note");
				break;
			case LikeType_Photo:
				like_type = TEXT("photo");
				break;
			case LikeType_Post:
				like_type = TEXT("post");
				break;
			case LikeType_Sitepage:
				like_type = TEXT("sitepage");
				break;
			case LikeType_Video:
				like_type = TEXT("video");
				break;
		}
		
		parameters += TEXT("&type=") + like_type;
		parameters += TEXT("&owner_id=");
		
		if (likeType == LikeType_Sitepage)
			parameters += m_applicationId;
		else
			parameters += ownerId;
			
		parameters += TEXT("&item_id=") + itemId;
		parameters += TEXT("&page_url=") + pageURL;
		parameters += TEXT("&filter=");
		
		if (allUsers)
			parameters += TEXT("likes");
		else
			parameters += TEXT("copies");
		
		parameters += TEXT("&friends_only=");
		
		if (friendsOnly)
			parameters += TEXT("1");
		
		parameters += TEXT("&count="); parameters += DataTraits<T>::IntToCharArray(count);
		parameters += TEXT("&offset="); parameters += DataTraits<T>::IntToCharArray(offset);
		
		try
		{
			SendAPIRequest(TEXT("likes.getList"), parameters);
			return m_responseParser->ParseStringArray();
		}
		catch(...)
		{
			throw;
		}
	}
	
	template<DataType T> const vector<typename VKClient<T>::String> *VKClient<T>::GetMutualFriends(const String &targetUserId, const String &sourceUserId = TEXT(""))
	{
		String parameters(TEXT("&target_uid="));
		parameters += targetUserId;
		parameters += TEXT("&source_uid=") + sourceUserId;
		
		try
		{
			SendAPIRequest(TEXT("friends.getMutual"), parameters);
			return m_responseParser->ParseStringArray();
		}
		catch(...)
		{
			throw;
		}
	}
	
	template<DataType T> const NewsFeed<T> *VKClient<T>::GetNewsFeed(const String *userIds, size_t usersCount, const String *groupIds, size_t groupsCount,
		NewsType newsType, time_t startTime, time_t endTime, size_t offset, const String &from, size_t count, size_t maxPhotos)
	{
		if (count > 100)
			count = 100;
			
		String parameters(TEXT("&source_ids="));
		
		if (userIds != NULL && usersCount > 0)
		{
			for (size_t i = 0; i < usersCount - 1; i++)
			{
				parameters += TEXT("u"); parameters += userIds[i];
				parameters += TEXT(",");
			}
			
			parameters =+ TEXT("u"); parameters += userIds[usersCount - 1];
		}
		
		if (groupIds != NULL && groupsCount > 0)
		{
			for (size_t i = 0; i < groupsCount - 1; i++)
			{
				parameters += TEXT("g"); parameters += groupIds[i];
				parameters += TEXT(",");
			}
			
			parameters =+ TEXT("g"); parameters += groupIds[groupsCount - 1];
		}
		
		parameters += TEXT("&filters=");
		
		if ((newsType & NewsType_Friend) == NewsType_Friend)
			parameters += TEXT("friend,");
		
		if ((newsType & NewsType_Note) == NewsType_Note)
			parameters += TEXT("note,");
		
		if ((newsType & NewsType_Photo) == NewsType_Photo)
			parameters += TEXT("photo,");
			
		if ((newsType & NewsType_PhotoTag) == NewsType_PhotoTag)
			parameters += TEXT("photo_tag,");
			
		if ((newsType & NewsType_Post) == NewsType_Post)
			parameters += TEXT("post,");
			
		//Удаляем последнюю запятую
		parameters.erase(parameters.size() - 1, 1);
		
		parameters += TEXT("&start_time="); parameters += Converter<T>::UnixTimeToCharArray(startTime);
		parameters += TEXT("&end_time="); parameters += Converter<T>::UnixTimeToCharArray(endTime);
		parameters += TEXT("&offset="); parameters += DataTraits<T>::IntToCharArray(offset);
		parameters += TEXT("&from="); parameters += from;
		parameters += TEXT("&count="); parameters += DataTraits<T>::IntToCharArray(count);
		parameters += TEXT("&max_photos="); parameters += DataTraits<T>::IntToCharArray(maxPhotos);
		
		try
		{
			SendAPIRequest(TEXT("newsfeed.get"), parameters);
			return m_responseParser->ParseNewsFeed();
		}
		catch(...)
		{
			throw;
		}
	}
	
	template<DataType T> const vector<typename VKClient<T>::String> *VKClient<T>::GetOnlineFriends(const String &uid = TEXT(""))
	{
		try
		{
			SendAPIRequest(TEXT("friends.getOnline"), TEXT("&uid=") + uid);
			return m_responseParser->ParseStringArray();
		}
		catch(...)
		{
			throw;
		}
	}
	
	/**
	 * \brief Возвращает ссылку текущего пользователя на страницу ВКонтакте.
	 * \return Ссылка на страницу текущего пользователя.
	 */
	template<DataType T> const typename VKClient<T>::String &VKClient<T>::GetPageLink() const
	{
		return m_pageLink;
	}
	
	template<DataType T> const typename VKClient<T>::String *VKClient<T>::GetPhotoUploadServer(const String &objectId, bool isGroup)
	{
		String parameters(TEXT("&"));
		
		if (isGroup)
			parameters += TEXT("gid=");
		else
			parameters += TEXT("aid=");
			
		parameters += objectId;
		
		try
		{
			SendAPIRequest(TEXT("photos.getUploadServer"), parameters);
			return m_responseParser->ParseString("upload_url");
		}
		catch(...)
		{
			throw;
		}
	}
	
	template<DataType T> const typename VKClient<T>::String *VKClient<T>::GetPhotoWallUploadServer(const String &objectId, bool isGroup)
	{
		String parameters(TEXT("&"));
		
		if (isGroup)
			parameters += TEXT("gid=");
		else
			parameters += TEXT("uid=");
			
		parameters += objectId;
		
		try
		{
			SendAPIRequest(TEXT("photos.getWallUploadServer"), parameters);
			return m_responseParser->ParseString("upload_url");
		}
		catch(...)
		{
			throw;
		}
	}
	
	template<DataType T> const vector <Photo<T> > *VKClient<T>::GetPhotos(const String &objectId, bool isGroup, const String &albumId, const String *photoIds, size_t photosCount,
		size_t limit, size_t offset, time_t feed, NewsType newsType)
	{
		String parameters(TEXT("&"));
		
		if (isGroup)
			parameters += TEXT("gid=");
		else
			parameters += TEXT("uid=");
			
		parameters += objectId;
		
		parameters += TEXT("&aid="); parameters += albumId;
		parameters += TEXT("&pids=");
		
		if (photoIds != NULL && photosCount > 0)
		{
			for(size_t i = 0; i < photosCount - 1; i++)
			{
				parameters += photoIds[i];
				parameters += TEXT(",");
			}
			
			parameters += photoIds[photosCount - 1];
		}
		
		parameters += TEXT("&extended=1");
		parameters += TEXT("&limit=");
		
		if (limit > 0)
			parameters += DataTraits<T>::IntToCharArray(limit);
			
		parameters += TEXT("&offset="); parameters += DataTraits<T>::IntToCharArray(offset);
		parameters += TEXT("&feed="); parameters += Converter<T>::UnixTimeToCharArray(feed);
		parameters += TEXT("&feed_type=");
		
		if (newsType == NewsType_PhotoTag)
			parameters += TEXT("photo_tag");
		else
			parameters += TEXT("photo");
			
		try
		{
			SendAPIRequest(TEXT("photos.get"), parameters);
			return m_responseParser->ParsePhotos();
		}
		catch(...)
		{
			throw;
		}
	}
	
	template<DataType T> const vector<Photo<T> > *VKClient<T>::GetPhotosById(const String *objectIds, const String *photoIds, size_t photosCount)
	{
		String parameters(TEXT("&photos="));
		
		if (objectIds != NULL && photoIds != NULL && photosCount > 0)
		{
			for(size_t i = 0; i < photosCount - 1; i++)
			{
				parameters += objectIds[i];
				parameters += TEXT("_");
				parameters += photoIds[i];
				parameters += TEXT(",");
			}
			
			parameters += objectIds[photosCount - 1];
			parameters += TEXT("_");
			parameters += photoIds[photosCount - 1];
		}
		
		parameters += TEXT("&extended=1");
		
		try
		{
			SendAPIRequest(TEXT("photos.getById"), parameters);
			return m_responseParser->ParsePhotos();
		}
		catch(...)
		{
			throw;
		}
	}
	
	//TODO: Задокументировать, что метод возвращает пустые данные
	template<DataType T> const vector<Photo<T> > *VKClient<T>::GetProfilePhotos(const String &objectId, bool isGroup, size_t limit, size_t offset)
	{
		String parameters(TEXT("&"));
		
		if (isGroup)
			parameters += TEXT("gid=");
		else
			parameters += TEXT("uid=");
		
		parameters += TEXT("&limit=0");
		if (limit > 0)
		{	
			parameters += TEXT("&limit=");
			parameters += DataTraits<T>::IntToCharArray(limit);
		}
		
		parameters += TEXT("&offset="); parameters += DataTraits<T>::IntToCharArray(offset);
		
		try
		{
			SendAPIRequest(TEXT("photos.getProfile"), parameters);
			return m_responseParser->ParsePhotos();
		}
		catch(...)
		{
			throw;
		}
	}
	
	template<DataType T> const typename VKClient<T>::String *VKClient<T>::GetProfileUploadServer()
	{
		try
		{
			SendAPIRequest(TEXT("photos.getProfileUploadServer"), TEXT(""));
			return m_responseParser->ParseString("upload_url");
		}
		catch(...)
		{
			throw;
		}
	}
	
	template<DataType T> time_t VKClient<T>::GetServerTime()
	{
		try
		{
			SendAPIRequest(TEXT("getServerTime"));
			m_stringHandler.Reset(m_responseParser->ParseString(NULL));
			time_t serverTime = Converter<T>::CharArrayToUnixTime(m_stringHandler.Get()->c_str());
			return serverTime;
		}
		catch(...)
		{
			throw;
		}
	}
	
	/** \brief Информирует, установил ли пользователь текущее приложение.
	 * \param userId Идентификатор пользователя.
	 * \return true - пользователь установил приложение, false - не установил.
	 */
	template<DataType T> bool VKClient<T>::IsAppUser(const String &userId)
	{
		try
		{
			if (userId.empty())
				SendAPIRequest(TEXT("isAppUser"), TEXT("&uid=") + m_id);
			else
				SendAPIRequest(TEXT("isAppUser"), TEXT("&uid=") + userId);
				
			return m_responseParser->ParseBoolean();
		}
		catch (...)
		{
			throw;
		}
	}
	
	template<DataType T> bool VKClient<T>::IsGroupMember(const String &groupId, const String &userId)
	{
		String parameters(TEXT("&gid="));
		parameters += groupId;
		parameters += (TEXT("&uid=")); parameters += userId;
		parameters += TEXT("&extended=1");
		
		try
		{
			SendAPIRequest(TEXT("groups.isMember"), parameters);
			return m_responseParser->ParseBoolean();
		}
		catch(...)
		{
			throw;
		}
	}
	
	/**
	 * \brief Получает информацию о том, указана ли автоматическая установка приложения.
	 * \details Автоматическая установка приложения подразумевает собой скрытую операцию
	 * добавления приложения к пользователю с нужными правами без его согласия.
	 * \return true - автоматическая установка приложения разрешена, false - запрещена.
	 */
	template<DataType T> bool VKClient<T>::IsInstallAllowed() const
	{
		return m_autoInstall;
	}
	
	/**
	 * \brief Авторизует пользователя в соцсети.
	 * \details Выполняет авторизацию в соцсети <a href="vk.com">ВКонтакте</a>, после чего автоматически добавляет приложение и выдает необходимые разрешения.
	 * \param email Логин (или e-mail) пользователя.
	 * \param pass Пароль пользователя.
	 * \param appAccess Уровень доступа приложения.
	 * \param captchaKey Ключ для <a href="http://ru.wikipedia.org/wiki/CAPTCHA">CAPTCHA</a>.
	 * \return Результат авторизации.
	 */
	template<DataType T> bool VKClient<T>::LogIn(const String &email, const String &pass, ApplicationAccess appAccess, const String &captchaKey)
	{
		try
		{
			m_responseStream->str(TEXT(""));
			m_connection->Get(TEXT("http://vk.com/"));
		}
		catch (ConnectionFailException &exc)
		{
			throw exc;
		}
		
		String response = m_responseStream->str();
		String ip_h = response.substr(response.find(TEXT("ip_h: ")) + 7, 18);
		String postBuf = TEXT("act=login"\
		                 "&q=1"\
		                 "&al_frame=1"\
		                 "&expire=");
		
		if (!captchaKey.empty())
		{
			postBuf += TEXT("&captcha_sid=") + m_captchaSid;
			postBuf += TEXT("&captcha_key=") + captchaKey;
		}
		
		postBuf.append(TEXT("&from_host=vk.com"
		               "&ip_h=") + ip_h);
		postBuf.append(TEXT("&email=") + email);
		postBuf.append(TEXT("&pass=") + pass);
		
		IStringStream inputStream(postBuf);
		m_connection->SetRequestStream(&inputStream);
		
		try
		{
			m_responseStream->str(TEXT(""));
			m_connection->Post(TEXT("http://login.vk.com/?act=login"));
		}
		catch (ConnectionFailException &exc)
		{
			throw exc;
		}
		
		response = m_responseStream->str();
		size_t successLogin = response.find(TEXT("parent.onLoginDone"));
		
		//Если дали успех - заходим на страничку и извлекаем доменное имя
		if (successLogin != String::npos)
		{
			successLogin += 20;
			int endOfPageLink = response.find(TEXT("</script>")) - 4;
			m_pageLink = TEXT("http://vk.com") + response.substr(successLogin, endOfPageLink - successLogin);

			try
			{
				m_responseStream->str(TEXT(""));
				m_connection->Get(m_pageLink);

				/* Проверяем, не заблокирована ли наша страница.
				 * Для этого достаточно проверить, не перенаправили ли нас
				 * на /login?act=blocked.
				 */
				String redirectedUrl = m_connection->GetConnectionInfo().GetLastUsedURL();

				if (redirectedUrl.find(TEXT("act=blocked")) != String::npos)
					return false;
			}
			catch (ConnectionFailException &exc)
			{
				throw exc;
			}

			response = m_responseStream->str();
			size_t idStartIndex = response.find(TEXT("id: ")) + 4;
			size_t idEndIndex = response.find(TEXT("intnat: ")) - 4;
			m_id = response.substr(idStartIndex, idEndIndex - idStartIndex);
			size_t hashStartIndex = response.find(TEXT("hash")) + 13;
			m_hash = response.substr(hashStartIndex, 18);
			ActivateDesktopApp(m_applicationId, appAccess);
			
			return true;
		}
		size_t captchaNeed = response.find(TEXT("parent.onLoginCaptcha"));

		if (captchaNeed != String::npos)
		{
			//TODO: Если требуется капча, извлечь sid, url - выбросить исключение
		
			return false;
		}
		
		return false;
	}
	
	/**
	 * \brief Авторизует пользователя в соцсети.
	 * \details Выполняет авторизацию в соцсети <a href="vk.com">ВКонтакте</a>, после чего автоматически добавляет приложение и выдает необходимые разрешения.
	 * \param email Логин (или e-mail) пользователя.
	 * \param pass Пароль пользователя.
	 * \param appId Идентификатор приложения. Создать свое приложение вы можете <a href="http://vk.com/editapp?act=create">здесь</a>.
	 * \param appAccess Уровень доступа приложения.
	 * \param captchaKey Ключ для <a href="http://ru.wikipedia.org/wiki/CAPTCHA">CAPTCHA</a>.
	 * \return Результат запроса.
	 */
	template<DataType T> bool VKClient<T>::LogIn(const String &email, const String &pass, const String &appId, ApplicationAccess appAccess, const String &captchaKey)
	{
		SetApplicationId(appId);
		return LogIn(email, pass, appAccess, captchaKey);
	}
	
	/**
	 * \brief Деавторизует пользователя в соцсети.
	 * \return Результат запроса.
	 */
	template<DataType T> void VKClient<T>::LogOut()
	{
		try
		{
			m_responseStream->str(TEXT(""));
			m_connection->Get(TEXT("https://login.vk.com/?act=logout&hash=") + m_hash + TEXT("&from_host=vk.com&from_protocol=http"));
		}
		catch (ConnectionFailException &exc)
		{
			throw exc;
		}
	}
	
	template<DataType T> bool VKClient<T>::MakeAlbumCover(const String &photoId, const String &albumId, const String &ownerId)
	{
		String parameters(TEXT("&pid=") + photoId + 
			TEXT("&aid=") + albumId +
			TEXT("&oid=") + ownerId);
			
		try
		{
			SendAPIRequest(TEXT("photos.makeCover"), parameters);
			return m_responseParser->ParseBoolean();
		}
		catch(...)
		{
			throw;
		}
	}
	
	template<DataType T> bool VKClient<T>::MovePhoto(const String &photoId, const String &targetAlbumId, const String &ownerId)
	{
		String parameters(TEXT("&pid=") + photoId +
			TEXT("&target_aid=") + targetAlbumId +
			TEXT("&oid=") + ownerId);
			
		try
		{
			SendAPIRequest(TEXT("photos.move"), parameters);
			return m_responseParser->ParseBoolean();
		}
		catch(...)
		{
			throw;
		}
	}
	
	template<DataType T> const typename VKClient<T>::String *VKClient<T>::NotesAdd(const String &title, const String &text,
		AccessPrivacy privacy, AccessPrivacy commentPrivacy)
	{
		String parameters(TEXT("&title="));
		
		m_stringHandler.Reset(Encoder<T>::EscapeString(title));
		parameters += *m_stringHandler;
		
		parameters += TEXT("&text=");
		m_stringHandler.Reset(Encoder<T>::EscapeString(text));
		parameters += *m_stringHandler;
		
		parameters += TEXT("&privacy=");
		parameters += DataTraits<T>::IntToCharArray(static_cast<int>(privacy));
		
		parameters += TEXT("&comment_privacy=");
		parameters += DataTraits<T>::IntToCharArray(static_cast<int>(commentPrivacy));
		
		try
		{
			SendAPIRequest(TEXT("notes.add"), parameters);
			return m_responseParser->ParseString("nid");
		}
		catch(...)
		{
			throw;
		}
	}
	
	template<DataType T> bool VKClient<T>::NotesDelete(const String &noteId)
	{
		try
		{
			SendAPIRequest(TEXT("notes.delete"), TEXT("&nid=") + noteId);
			return m_responseParser->ParseBoolean();
		}
		catch(...)
		{
			throw;
		}
	}
	
	template<DataType T> bool VKClient<T>::NotesEdit(const String &noteId, const String &title, const String &text,
		AccessPrivacy privacy, AccessPrivacy commentPrivacy)
	{
		String parameters(TEXT("&nid=") + noteId);
		
		parameters += TEXT("&title=");
		m_stringHandler.Reset(Encoder<T>::EscapeString(title));
		parameters += *m_stringHandler;
		parameters += TEXT("&text=");
		m_stringHandler.Reset(Encoder<T>::EscapeString(text));
		parameters += *m_stringHandler;
		parameters += TEXT("&privacy=");
		parameters += DataTraits<T>::IntToCharArray(static_cast<int>(privacy));
		parameters += TEXT("&comment_privacy=");
		parameters += DataTraits<T>::IntToCharArray(static_cast<int>(commentPrivacy));
		
		try
		{
			SendAPIRequest(TEXT("notes.edit"), parameters);
			return m_responseParser->ParseBoolean();
		}
		catch(...)
		{
			throw;
		}
	}
	
	template<DataType T> const vector<Note<T> > *VKClient<T>::NotesGet(const String *noteIds, size_t notesCount, const String &userId,
			NotesSortType sortType, size_t count, size_t offset)
	{
		if (noteIds != NULL && notesCount > 0)
		{
			if (count > 100)
				count = 100;
			
			String parameters(TEXT("&uid=") + userId);
			parameters += TEXT("&nids=");
			
			for(size_t i = 0; i < notesCount; i++)
			{
				parameters += noteIds[i];
				
				if (i < notesCount - 1)
					parameters += TEXT(",");
			}
			
			parameters += TEXT("&sort=");
			parameters  += DataTraits<T>::IntToCharArray(static_cast<int>(sortType));
			parameters += TEXT("&offset="); parameters += DataTraits<T>::IntToCharArray(offset);
			parameters += TEXT("&count="); parameters += DataTraits<T>::IntToCharArray(count);
			
			try
			{
				SendAPIRequest(TEXT("notes.get"), parameters);
				return m_responseParser->ParseNotes();
			}
			catch(...)
			{
				throw;
			}
		}
		
		return NULL;
	}
	
	template<DataType T> const Note<T> *VKClient<T>::NotesGetById(const String &noteId, const String &ownerId,
		bool needWiki)
	{
		String parameters(TEXT("&nid=") + noteId +
			TEXT("&owner_id=") + ownerId);
			
		if (needWiki)
			parameters += TEXT("&need_wiki=1");
		else
			parameters += TEXT("&need_wiki=0");
			
		try
		{
			SendAPIRequest(TEXT("notes.getById"), parameters);
			return m_responseParser->ParseNote(NULL);
		}
		catch(...)
		{
			throw;
		}
	}
	
	template<DataType T> const vector<Comment<T> > *VKClient<T>::NotesGetComments(const String &noteId,
		const String &ownerId, CommentsSortType sortType, size_t count, size_t offset)
	{
		if (count > 100)
			count = 100;
			
		String parameters(TEXT("&nid=") + noteId +
			TEXT("&owner_id=") + ownerId);
			
		parameters += TEXT("&sort=");
		parameters += DataTraits<T>::IntToCharArray(static_cast<int>(sortType));
		parameters += TEXT("&count="); parameters += DataTraits<T>::IntToCharArray(count);
		parameters += TEXT("&offset="); parameters += DataTraits<T>::IntToCharArray(offset);
		
		try
		{
			SendAPIRequest(TEXT("notes.getComments"), parameters);
			return m_responseParser->ParseComments();
		}
		catch(...)
		{
			throw;
		}
	}
	
	template<DataType T> const vector<Note<T> > *VKClient<T>::NotesGetFriendNotes(size_t count, size_t offset)
	{
		if (count > 100)
			count = 100;
		
		String parameters(TEXT("&"));
		parameters += TEXT("count="); parameters += DataTraits<T>::IntToCharArray(count);
		parameters += TEXT("&offset="); parameters += DataTraits<T>::IntToCharArray(offset);
		
		try
		{
			SendAPIRequest(TEXT("notes.getFriendsNotes"), parameters);
			return m_responseParser->ParseNotes();
		}
		catch(...)
		{
			throw;
		}
	}
	
	template<DataType T> bool VKClient<T>::ReorderAlbums(const String &albumId, const String &beforeAlbumId, const String &afterAlbumId, const String &ownerId)
	{
		String parameters(TEXT("&aid=") + albumId +
			TEXT("&before=") + beforeAlbumId +
			TEXT("&after=") + afterAlbumId +
			TEXT("&oid") + ownerId);
			
		try
		{
			SendAPIRequest(TEXT("photos.reorderAlbums"), parameters);
			return m_responseParser->ParseBoolean();
		}
		catch(...)
		{
			throw;
		}
	}
	
	template<DataType T> bool VKClient<T>::ReorderPhotos(const String &photoId, const String &beforePhotoId, const String &afterPhotoId, const String &ownerId)
	{
		String parameters(TEXT("&pid=") + photoId +
			TEXT("&before=") + beforePhotoId +
			TEXT("&after=") + afterPhotoId +
			TEXT("&oid") + ownerId);
			
		try
		{
			SendAPIRequest(TEXT("photos.reorderPhotos"), parameters);
			return m_responseParser->ParseBoolean();
		}
		catch(...)
		{
			throw;
		}
	}
	
	template<DataType T> const vector<Photo<T> > *VKClient<T>::SavePhotos(const String &objectId, bool isGroup,
		const String &server, const String &photosList, const String &hash, const String &caption)
	{
		String parameters(TEXT("&"));
		
		if (isGroup)
			parameters += TEXT("gid=");
		else
			parameters += TEXT("aid=");
			
		parameters += objectId;
		parameters += TEXT("&server="); parameters += server;
		parameters += TEXT("&photos_list="); parameters += photosList;
		parameters += TEXT("&hash="); parameters += hash;
		parameters += TEXT("&caption="); parameters += caption;
		
		try
		{
			SendAPIRequest(TEXT("photos.save"), parameters);
			return m_responseParser->ParsePhotos();
		}
		catch(...)
		{
			throw;
		}
	}
	
	template<DataType T> void VKClient<T>::SaveProfilePhoto(const String &server, const String &photo, const String &hash)
	{
		try
		{
			String parameters(TEXT("&server=") + server +
				TEXT("&photo=") + photo +
				TEXT("&hash=") + hash);
			SendAPIRequest(TEXT("photos.saveProfilePhoto"), parameters);
		}
		catch(...)
		{
			throw;
		}
	}
	
	template<DataType T> const Photo<T> *VKClient<T>::SaveWallPhoto(const String &server, const String &photo, const String &hash,
		const String &objectId, bool isGroup)
	{
		String parameters(TEXT("&server=") + server +
			TEXT("&photo=") + photo +
			TEXT("&hash=") + hash +
			TEXT("&"));

		if (isGroup)
			parameters += TEXT("gid=");
		else
			parameters += TEXT("uid=");
			
		parameters += objectId;
		
		try
		{
			SendAPIRequest(TEXT("photos.saveWallPhoto"), parameters);
			return m_responseParser->ParsePhoto(NULL);
		}
		catch(...)
		{
			throw;
		}
	}
	
	template<DataType T> const vector<Group<T> > *VKClient<T>::SearchGroups(const String &query, size_t count, size_t offset)
	{
		String parameters(TEXT("&q=")); parameters += query;
		parameters += TEXT("&count="); parameters += DataTraits<T>::IntToCharArray(count);
		parameters += TEXT("&offset="); parameters += DataTraits<T>::IntToCharArray(offset);
		
		try
		{
			SendAPIRequest(TEXT("groups.search"), parameters);
			return m_responseParser->ParseGroups();
		}
		catch(...)
		{
			throw;
		}
	}
	
	template<DataType T> const vector<News<T> > *VKClient<T>::SearchNews(const String &query, size_t count, size_t offset, time_t startTime,
		time_t endTime, const String &startId)
	{
		if (count > 100)
			count = 100;

		String parameters(TEXT("&q=")); parameters += query;
		parameters += TEXT("&count="); parameters += DataTraits<T>::IntToCharArray(count);
		parameters += TEXT("&offset="); parameters += DataTraits<T>::IntToCharArray(offset);
		
		m_memoryHandler.Reset(Converter<T>::UnixTimeToCharArray(startTime));
		parameters += TEXT("&start_time="); parameters += m_memoryHandler.Get();
		
		m_memoryHandler.Reset(Converter<T>::UnixTimeToCharArray(endTime));
		parameters += TEXT("&end_time="); parameters += m_memoryHandler.Get();
		
		parameters += TEXT("&start_id="); parameters += startId;
		parameters += TEXT("&extended=1");
		
		try
		{
			SendAPIRequest(TEXT("newsfeed.search"), parameters);
			return m_responseParser->ParseNews(NULL);
		}
		catch(...)
		{
			throw;
		}
	}
	
	template<DataType T> const vector<Profile<T> > *VKClient<T>::SearchUsers(const String &query, size_t count, size_t offset)
	{
		if (count > 1000)
			count = 1000;
		
		String fields(TEXT("uid,first_name,last_name,nickname,screen_name,sex,bdate,city,country,timezone,photo,photo_medium,photo_big,has_mobile,rate,contacts,education,online"));
		
		try
		{
			SendAPIRequest(TEXT("users.search"),
				TEXT("&q=") + query +
				TEXT("&fields=") + fields +
				TEXT("&count=") + DataTraits<T>::IntToCharArray(count) +
				TEXT("&offset=") + DataTraits<T>::IntToCharArray(offset));
			
			return m_responseParser->ParseProfiles();
		}
		catch(...)
		{
			throw;
		}
	}
	
	/**
	 * \brief Отправляет запрос на сервер API.
	 * \param methodName Название метода API, которое будет вызвано.
	 * \param params Параметры вызываемого из API метода.
	 * \return Результат запроса.
	 */
	template<DataType T> void VKClient<T>::SendAPIRequest(const String &methodName, const String &params)
	{
		String requestURL = TEXT("https://") + m_apiServer +
			TEXT("/method/") + methodName + TEXT(".xml?") +
			TEXT("access_token=") + m_accessToken;
		
		if (!params.empty())
			requestURL += params;
		
		try
		{
			m_responseStream->str(TEXT(""));
			m_connection->Get(requestURL);
		
			String xmlResult = m_responseStream->str();

			m_responseParser->SetBuffer(xmlResult);
			m_responseParser->ProcessAPIError();
		}
		catch(...)
		{
			throw;
		}
	}
	
	/**
	 * \brief Устанавливает адрес сервера API.
	 * \param serverAddress Адрес сервера.
	 */
	template<DataType T> void VKClient<T>::SetAPIServer(const String &serverAddress)
	{
		m_apiServer = serverAddress;
	}
	
	/**
	 * \brief Устанавливает идентификатор приложения.
	 * \param appId Идентификатор приложения.
	 */
	template<DataType T> void VKClient<T>::SetApplicationId(const String &appId)
	{
		m_applicationId = appId;
	}
	
	/**
	 * \brief Указывает соединение, которое будет использоваться клиентом.
	 * \param connection Соединение, которое будет использоваться клиентом.
	 */
	template<DataType T> void VKClient<T>::SetConnection(HTTPConnection<T> *connection)
	{
		m_connection = connection;
	}
	
	template<DataType T> const typename VKClient<T>::String *VKClient<T>::VideoAdd(const String &videoId,
		const String &ownerId)
	{
		String parameters(TEXT("&vid=") + videoId +
			TEXT("&oid=") + ownerId);
			
		try
		{
			SendAPIRequest(TEXT("video.add"), parameters);
			return m_responseParser->ParseString(NULL);
		}
		catch(...)
		{
			throw;
		}
	}
	
	template<DataType T> const typename VKClient<T>::String *VKClient<T>::VideoCreateComment(const String &videoId,
		const String &message, const String &ownerId)
	{
		String parameters(TEXT("&vid=") + videoId +
			TEXT("&owner_id=") + ownerId +
			TEXT("&message=") + message);
			
		try
		{
			SendAPIRequest(TEXT("video.createComment"), parameters);
			return m_responseParser->ParseString(NULL);
		}
		catch(...)
		{
			throw;
		}
	}
	
	template<DataType T> bool VKClient<T>::VideoDelete(const String &videoId, const String &ownerId)
	{
		String parameters(TEXT("&vid=") + videoId +
			TEXT("&oid=") + ownerId);
			
		try
		{
			SendAPIRequest(TEXT("video.delete"), parameters);
			return m_responseParser->ParseBoolean();
		}
		catch(...)
		{
			throw;
		}
	}
	
	template<DataType T> bool VKClient<T>::VideoDeleteComment(const String &commentId, const String &ownerId)
	{
		String parameters(TEXT("&owner_id=") + ownerId +
			TEXT("&cid=") + commentId);
			
		try
		{
			SendAPIRequest(TEXT("video.deleteComment"), parameters);
			return m_responseParser->ParseBoolean();
		}
		catch(...)
		{
			throw;
		}
	}
	
	template<DataType T> bool VKClient<T>::VideoEdit(const String &videoId, const String &ownerId, const String &newName, const String &newDescription,
		AccessPrivacy newViewPrivacy, AccessPrivacy newCommentPrivacy)
	{
		String parameters(TEXT("&vid=") + videoId +
			TEXT("&oid=") + ownerId +
			TEXT("&name=") + newName +
			TEXT("&desc=") + newDescription);
			
		
		parameters += TEXT("&privacy_view=");
		parameters += DataTraits<T>::IntToCharArray(static_cast<int>(newViewPrivacy));
		parameters += TEXT("&privacy_comment=");
		parameters += DataTraits<T>::IntToCharArray(static_cast<int>(newCommentPrivacy));
		
		try
		{
			SendAPIRequest(TEXT("video.edit"), parameters);
			return m_responseParser->ParseBoolean();
		}
		catch(...)
		{
			throw;
		}
	}
	
	template<DataType T> bool VKClient<T>::VideoEditComment(const String &commentId, const String &message, const String &ownerId)
	{
		String parameters(TEXT("&cid=") + commentId +
			TEXT("&message=") + message +
			TEXT("&owner_id=") + ownerId);
			
		try
		{
			SendAPIRequest(TEXT("video.editComment"), parameters);
			return m_responseParser->ParseBoolean();
		}
		catch(...)
		{
			throw;
		}
	}
	
	/** \brief Получает баланс текущего пользователя на счету приложения.
	 * \return Баланс пользователя.
	 */
	template<DataType T> float VKClient<T>::GetUserBalance()
	{
		try
		{
			SendAPIRequest(TEXT("getUserBalance"), TEXT(""));
		}
		catch(...)
		{
			throw;
		}
		
		size_t balance = m_responseParser->ParseNesetUInt();
		float userBalance = balance * 0.01;
		
		return userBalance;
	}	
	
	template<DataType T> ApplicationAccess VKClient<T>::GetUserRights(const String &userId)
	{
		try
		{
			if (userId.empty())
				SendAPIRequest(TEXT("getUserSettings"), TEXT("&uid=") + m_id);
			else
				SendAPIRequest(TEXT("getUserSettings"), TEXT("&uid=") + userId);
				
			return (ApplicationAccess)m_responseParser->ParseNestedUInt();
		}
		catch(...)
		{
			throw;
		}
	}
	
	/**
	 * \brief Получает расширенную информацию о пользователях.
	 * \param userIds Массив идентификаторов пользователей, информация которых будет запрошена.
	 * Максимальное количество 1000 пользователей.
	 * \param userCount Количество пользователей, информация которых будет запрошена.
	 * Максимальное количество 1000 пользователей.
	 * \param nameCase Падеж, в котором будут склоняться имя и фамилия каждого пользователя.
	 * По умолчанию - именительный.
	 * \return Вектор профилей, который будет заполнен информацией о профилях..
	 */
	template<DataType T> const vector<Profile<T> > *VKClient<T>::GetUsers(const String *userIds, size_t userCount, NameCase nameCase)
	{
		String uids;
		
		if (userIds == NULL || userCount < 1)
			uids = m_id;
		else
		{
			if (userCount > 1000)
				userCount = 1000;
			
			for (size_t i = 0; i < userCount - 1; i++)
				uids += userIds[i] + TEXT(",");
			
			uids += userIds[userCount - 1];
		}
		
		// TODO: Дать возможность выбора требуемых полей
		String fields = TEXT("uid,first_name,last_name,nickname,"\
		                "screen_name,sex,birthdate,city,country,"\
		                "timezone,photo,photo_medium,photo_big,has_mobile,"\
		                "rate,contacts,education,online,counters");
		String name_case;
		
		switch (nameCase)
		{
			case NameCase_Nominative:
				name_case = TEXT("nom");
				break;
			case NameCase_Genitive:
				name_case = TEXT("gen");
				break;
			case NameCase_Dative:
				name_case = TEXT("dat");
				break;
			case NameCase_Accusative:
				name_case = TEXT("acc");
				break;
			case NameCase_Instrumental:
				name_case = TEXT("ins");
				break;
			case NameCase_Ablative:
				name_case = TEXT("abl");
				break;
		}
		
		try
		{
			SendAPIRequest(TEXT("users.get"),
				TEXT("&uids=") + uids +
				TEXT("&fields=") + fields +
				TEXT("&name_case=") + name_case);
				
			return m_responseParser->ParseProfiles();
		}
		catch(...)
		{
			throw;
		}
	}
	
	template<DataType T> const vector<Post<T> > *VKClient<T>::GetWall(const String &ownerId, size_t offset, size_t count, WallPostFilter filter)
	{
		String parameters(TEXT("&owner_id=") + ownerId);
		
		if (offset > 0)
		{
			parameters += TEXT("&offset="); parameters += DataTraits<T>::IntToCharArray(offset);
		}
		
		if (count > 0)
		{
			parameters += TEXT("&count="); parameters += DataTraits<T>::IntToCharArray(count);
		}
		
		parameters += TEXT("&filter=");
		
		switch(filter)
		{
			case WallPostFilter_All:
				parameters += TEXT("all");
				break;
				
			case WallPostFilter_Others:
				parameters += TEXT("others");
				break;
				
			case WallPostFilter_Owner:
				parameters += TEXT("owner");
				break;
		}
		
		parameters += TEXT("&extended=0");
		
		try
		{
			SendAPIRequest(TEXT("wall.get"), parameters);
			return m_responseParser->ParsePosts();
		}
		catch(...)
		{
			throw;
		}
	}
	
	template<DataType T> const vector<Comment<T> > *VKClient<T>::GetWallComments(const String &ownerId, const String &postId,
		CommentsSortType commentsSortType, size_t offset, size_t count, size_t previewLength)
	{
		String parameters(TEXT("&owner_id=") + ownerId +
			TEXT("&post_id=") + postId +
			TEXT("&sort="));

		if (commentsSortType == CommentsSortType_Ascending)
			parameters += TEXT("asc");
		else
			parameters += TEXT("desc");
			
		parameters += TEXT("&need_likes=1");
		parameters += TEXT("&offset="); parameters += DataTraits<T>::IntToCharArray(offset);
		parameters += TEXT("&count="); parameters += DataTraits<T>::IntToCharArray(count);
		parameters += TEXT("&preview_length="); parameters += DataTraits<T>::IntToCharArray(previewLength);
		
		try
		{
			SendAPIRequest(TEXT("wall.getComments"), parameters);
			return m_responseParser->ParseComments();
		}
		catch(...)
		{
			throw;
		}
	}
	
	template<DataType T> const vector<Video<T> > *VKClient<T>::VideoGet(const String &objectId, bool isGroup,
		const String &albumId, VideoWidth videoWidth, size_t count, size_t offset)
	{
		if (count > 200)
			count = 200;
		
		String parameters(TEXT("&"));
		
		if (isGroup)
			parameters += TEXT("gid=");
		else
			parameters += TEXT("uid=");
			
		parameters += objectId;
		parameters += TEXT("&aid="); parameters += albumId;
		parameters += TEXT("&width=");
		
		switch (videoWidth)
		{
			case VideoWidth_130:
				parameters += TEXT("130");
				break;
				
			case VideoWidth_160:
				parameters += TEXT("160");
				break;
				
			case VideoWidth_320:
				parameters += TEXT("320");
				break;
		}
		
		parameters += TEXT("&offset="); parameters += DataTraits<T>::IntToCharArray(offset);
		parameters += TEXT("&count="); parameters += DataTraits<T>::IntToCharArray(count);
		
		try
		{
			SendAPIRequest(TEXT("video.get"), parameters);
			return m_responseParser->ParseVideos();
		}
		catch(...)
		{
			throw;
		}
	}
	
	template<DataType T> const vector<Comment<T> > *VKClient<T>::VideoGetComments(const String &videoId, const String &ownerId, size_t offset, size_t count,
		CommentsSortType sortType)
	{
		String parameters(TEXT("&vid=") + videoId +
			TEXT("&owner_id=") + ownerId);

		parameters += TEXT("&offset="); parameters += DataTraits<T>::IntToCharArray(offset);
		parameters += TEXT("&count="); parameters += DataTraits<T>::IntToCharArray(count);
		
		if (sortType == CommentsSortType_Ascending)
			parameters += TEXT("&sort=asc");
		else
			parameters += TEXT("&sort=desc");
		
		try
		{
			SendAPIRequest(TEXT("video.getComments"), parameters);
			return m_responseParser->ParseComments();
		}
		catch(...)
		{
			throw;
		}
	}
	
	template<DataType T> const vector<VideoTag<T> > *VKClient<T>::VideoGetTags(const String &videoId, const String &ownerId)
	{
		try
		{
			SendAPIRequest(TEXT("video.getTags"), TEXT("&owner_id=") + ownerId +
				TEXT("&vid=") + videoId);
			return m_responseParser->ParseVideoTags();
		}
		catch(...)
		{
			throw;
		}
	}
	
	template<DataType T> const vector<Video<T> > *VKClient<T>::VideoGetUserVideos(const String &userId, size_t offset,
		size_t count)
	{
		String parameters(TEXT("&uid=") + userId);
		parameters += TEXT("&offset="); parameters += DataTraits<T>::IntToCharArray(offset);
		parameters += TEXT("&count="); parameters += DataTraits<T>::IntToCharArray(count);
		
		try
		{
			SendAPIRequest(TEXT("video.getUserVideos"), parameters);
			return m_responseParser->ParseVideos();
		}
		catch(...)
		{
			throw;
		}
	}
	
	//TODO: задокументировать, что метод не срабатывает. Возможно, эта функиональность ВКонтакте отменена.
	template<DataType T> const typename VKClient<T>::String *VKClient<T>::VideoPutTag(const String &videoId, const String &userId,
		const String &ownerId)
	{
		try
		{
			SendAPIRequest(TEXT("video.putTag"), TEXT("&owner_id=") + ownerId +
				TEXT("&vid=") + videoId +
				TEXT("&uid=") + userId);
			return m_responseParser->ParseString(NULL);
		}
		catch(...)
		{
			throw;
		}
	}
	
	template<DataType T> bool VKClient<T>::VideoRemoveTag(const String &videoId, const String &tagId, const String &ownerId)
	{
		try
		{
			SendAPIRequest(TEXT("video.removeTag"), TEXT("&owner_id=") + ownerId +
				TEXT("&vid=") + videoId +
				TEXT("&tag_id=") + tagId);
			return m_responseParser->ParseBoolean();
		}
		catch(...)
		{
			throw;
		}
	}
	
	template<DataType T> const typename VKClient<T>::String *VKClient<T>::VideoSave(const String &name, const String &description,
		const String &groupId, AccessPrivacy viewPrivacy, AccessPrivacy commentPrivacy)
	{
		String parameters(TEXT("&name="));
		
		m_stringHandler.Reset(Encoder<T>::EscapeString(name));
		parameters += *m_stringHandler;
		
		m_stringHandler.Reset(Encoder<T>::EscapeString(description));
		parameters += TEXT("&description="); parameters += *m_stringHandler;
		
		parameters += TEXT("&gid="); parameters += groupId;
		parameters += TEXT("&privacy_view=");
		parameters += DataTraits<T>::IntToCharArray(static_cast<int>(viewPrivacy));
		
		parameters += TEXT("&privacy_comment=");
		parameters += DataTraits<T>::IntToCharArray(static_cast<int>(commentPrivacy));
		
		try
		{
			SendAPIRequest(TEXT("video.save"), parameters);
			return m_responseParser->ParseString("upload_url");
		}
		catch(...)
		{
			throw;
		}
	}
	
	template<DataType T> const vector<Video<T> > *VKClient<T>::VideoSearch(const String &query, VideoSortType sortType, bool hdQuality,
		size_t count, size_t offset)
	{
		if (count > 200)
			count = 200;
		
		String parameters(TEXT("&q=") + query);
		parameters += TEXT("&sort=");
		parameters += DataTraits<T>::IntToCharArray(static_cast<int>(sortType));
		
		if (!hdQuality)
			parameters += TEXT("&hd=0");
		else
			parameters += TEXT("&hd=1");
			
		parameters += TEXT("&offset="); parameters += DataTraits<T>::IntToCharArray(offset);
		parameters += TEXT("&count="); parameters += DataTraits<T>::IntToCharArray(count);
		
		try
		{
			SendAPIRequest(TEXT("video.search"), parameters);
			return m_responseParser->ParseVideos();
		}
		catch(...)
		{
			throw;
		}
	}
	
	template<DataType T> const vector<Post<T> > *VKClient<T>::WallGetById(const String *ownerIds, const String *postIds, size_t postsCount)
	{
		String parameters(TEXT("&posts="));
		
		if (ownerIds != NULL && postIds != NULL && postsCount > 0)
		{
			for(size_t i = 0; i < postsCount - 1; i++)
			{
				parameters += ownerIds[i];
				parameters += TEXT("_");
				parameters += postIds[i];
				parameters += TEXT(",");
			}
			
			parameters += ownerIds[postsCount - 1];
			parameters += TEXT("_");
			parameters += postIds[postsCount - 1];
			
			try
			{
				SendAPIRequest(TEXT("wall.getById"), parameters);
				return m_responseParser->ParsePosts();
			}
			catch(...)
			{
				throw;
			}
		}
		
		return new vector<Post<T> >();
	}
	
	template<DataType T> const typename VKClient<T>::String *VKClient<T>::WallPost(const String &ownerId, const String &message,
		const Media<T> *attachments, size_t attachmentsCount, const Place<T> &place, Service services, bool fromGroup,
		bool isSigned, bool friendsOnly)
	{
		String parameters(TEXT("&owner_id=") + ownerId +
			TEXT("&message=") + message);
		
		//Прикрепляем аттачменты в формате <type><owner_id>_<media_id>url
		if (attachments != NULL && attachmentsCount > 0)
		{
			parameters += TEXT("&attachments=");
			
			for(size_t i = 0; i < attachmentsCount; i++)
			{
				MediaType attachmentType = attachments[i].GetType();
				
				switch (attachmentType)
				{
					case MediaType_Audio:
						parameters += TEXT("audio");
						break;
						
					case MediaType_Document:
						parameters += TEXT("doc");
						break;
						
					case MediaType_Photo:
						parameters += TEXT("photo");
						break;
						
					case MediaType_Video:
						parameters += TEXT("video");
						break;
				}
				
				parameters += attachments[i].GetOwnerId();
				parameters += TEXT("_");
				parameters += attachments[i].GetId();
				
				if (i < attachmentsCount - 1)
					parameters += TEXT(",");
				
				if (!attachments[i].GetUrl().empty())
					parameters += attachments[i].GetUrl();
			}
		}
		
		//Прикрепляем место, если его ИД не пустой
		if (!place.GetId().empty())
		{
			parameters += TEXT("&lat="); parameters += place.GetLatitude();
			parameters += TEXT("&long="); parameters += place.GetLongitude();
			parameters += TEXT("&place_id="); parameters += place.GetId();
		}
		
		if (services != Service_None)
		{
			String servs;
			
			if ((services & Service_Facebook) == Service_Facebook)
				servs += TEXT("facebook");
				
			if ((services & Service_Twitter) == Service_Twitter)
			{
				if (!servs.empty())
					servs += TEXT(",");
					
				servs += TEXT("twitter");
			}
			
			parameters += TEXT("&services="); parameters += servs;
		}
		
		parameters += TEXT("&from_group=");
		
		if (fromGroup)
			parameters += TEXT("1");
		else
			parameters += TEXT("0");
			
		parameters += TEXT("&signed=");
		
		if (isSigned)
			parameters += TEXT("1");
		else
			parameters += TEXT("0");
			
		parameters += TEXT("&friends_only=");
		
		if (friendsOnly)
			parameters += TEXT("1");
		else
			parameters += TEXT("0");
			
		try
		{
			SendAPIRequest(TEXT("wall.post"), parameters);
			return m_responseParser->ParseString("post_id");
		}
		catch(...)
		{
			throw;
		}
	}
}
