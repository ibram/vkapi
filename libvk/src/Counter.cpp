#include "Counter.h"
using namespace sonapi;
using namespace std;

UserCounter::UserCounter()
{
	m_albumsCount = m_audiosCount = m_followersCount = m_friendsCount = m_mutualFriendsCount = m_notesCount = 0;
	m_onlineFriendsCount = m_photosWithUserCount = m_subscriptionsCount = m_videosCount = m_videosWithUserCount = 0;
}

size_t UserCounter::GetAlbumsCount() const
{
	return m_albumsCount;
}

size_t UserCounter::GetAudiosCount() const
{
	return m_audiosCount;
}

size_t UserCounter::GetFollowersCount() const
{
	return m_followersCount;
}

size_t UserCounter::GetFriendsCount() const
{
	return m_friendsCount;
}

size_t UserCounter::GetMutualFriendsCount() const
{
	return m_mutualFriendsCount;
}

size_t UserCounter::GetNotesCount() const
{
	return m_notesCount;
}

size_t UserCounter::GetOnlineFriendsCount() const
{
	return m_onlineFriendsCount;
}

size_t UserCounter::GetPhotosWithUserCount() const
{
	return m_photosWithUserCount;
}

size_t UserCounter::GetSubscriptionsCount() const
{
	return m_subscriptionsCount;
}

size_t UserCounter::GetVideosCount() const
{
	return m_videosCount;
}

size_t UserCounter::GetVideosWithUserCount() const
{
	return m_videosWithUserCount;
}

void UserCounter::SetAlbumsCount(size_t albumsCount)
{
	m_albumsCount = albumsCount;
}

void UserCounter::SetAudiosCount(size_t audiosCount)
{
	m_audiosCount = audiosCount;
}

void UserCounter::SetFollowersCount(size_t followersCount)
{
	m_followersCount = followersCount;
}

void UserCounter::SetFriendsCount(size_t friendsCount)
{
	m_friendsCount = friendsCount;
}

void UserCounter::SetMutualFriendsCount(size_t mutualFriendsCount)
{
	m_mutualFriendsCount = mutualFriendsCount;
}

void UserCounter::SetNotesCount(size_t notesCount)
{
	m_notesCount = notesCount;
}

void UserCounter::SetOnlineFriendsCount(size_t onlineFriendsCount)
{
	m_onlineFriendsCount = onlineFriendsCount;
}

void UserCounter::SetPhotosWithUserCount(size_t photosWithUserCount)
{
	m_photosWithUserCount = photosWithUserCount;
}

void UserCounter::SetSubscriptionsCount(size_t subscriptionsCount)
{
	m_subscriptionsCount = subscriptionsCount;
}

void UserCounter::SetVideosCount(size_t videosCount)
{
	m_videosCount = videosCount;
}

void UserCounter::SetVideosWithUserCount(size_t videosWithUserCount)
{
	m_videosWithUserCount = videosWithUserCount;
}
