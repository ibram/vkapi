#include "GroupCounter.h"
using namespace sonapi;
using namespace std;

GroupCounter::GroupCounter()
{
	m_albumsCount = m_audiosCount = m_docsCount = 0;
	m_photosCount = m_topicsCount = m_videosCount = 0;
}

size_t GroupCounter::GetAlbumsCount() const
{
	return m_albumsCount;
}

size_t GroupCounter::GetAudiosCount() const
{
	return m_audiosCount;
}

size_t GroupCounter::GetDocsCount() const
{
	return m_docsCount;
}

size_t GroupCounter::GetPhotosCount() const
{
	return m_photosCount;
}

size_t GroupCounter::GetTopicsCount() const
{
	return m_topicsCount;
}

size_t GroupCounter::GetVideosCount() const
{
	return m_videosCount;
}

void GroupCounter::SetAlbumsCount(size_t albumsCount)
{
	m_albumsCount = albumsCount;
}

void GroupCounter::SetAudiosCount(size_t audiosCount)
{
	m_audiosCount = audiosCount;
}

void GroupCounter::SetDocsCount(size_t docsCount)
{
	m_docsCount = docsCount;
}

void GroupCounter::SetPhotosCount(size_t photosCount)
{
	m_photosCount = photosCount;
}

void GroupCounter::SetTopicsCount(size_t topicsCount)
{
	m_topicsCount = topicsCount;
}

void GroupCounter::SetVideosCount(size_t videosCount)
{
	m_videosCount = videosCount;
}