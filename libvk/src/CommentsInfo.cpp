#include "CommentsInfo.h"
using namespace sonapi;

CommentsInfo::CommentsInfo()
{
	m_count = 0;
	m_canPost = 0;
}

bool CommentsInfo::CanPost() const
{
	return m_canPost;
}

size_t CommentsInfo::GetCount() const
{
	return m_count;
}

void CommentsInfo::SetCanPost(bool canPost)
{
	m_canPost = canPost;
}

void CommentsInfo::SetCount(size_t count)
{
	m_count = count;
}