#include "LikesInfo.h"
using namespace sonapi;

LikesInfo::LikesInfo()
{
	m_canLike = m_canPublish = m_userLikes = false;
	m_count = 0;
}

bool LikesInfo::CanLike() const
{
	return m_canLike;
}

bool LikesInfo::CanPublish() const
{
	return m_canPublish;
}

size_t LikesInfo::GetCount() const
{
	return m_count;
}

bool LikesInfo::IsUserLikes() const
{
	return m_userLikes;
}

void LikesInfo::SetCanLike(bool canLike)
{
	m_canLike = canLike;
}

void LikesInfo::SetCanPublish(bool canPublish)
{
	m_canPublish = canPublish;
}

void LikesInfo::SetCount(size_t count)
{
	m_count = count;
}

void LikesInfo::SetUserLikes(bool userLikes)
{
	m_userLikes = userLikes;
}