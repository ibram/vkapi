#include "CaptchaNeedException.h"
using namespace sonapi;

CaptchaNeedException::CaptchaNeedException(int errorCode, const char *errorMessage):
	BaseException(errorCode, errorMessage)
{
	m_captchaSid = m_captchaUrl = NULL;
}

CaptchaNeedException::~CaptchaNeedException() throw()
{
	delete[] m_captchaSid;
	delete[] m_captchaUrl;
}

/**
 * \brief Возвращает sid <a href = "http://ru.wikipedia.org/wiki/CAPTCHA"> CAPTCHA </a>.
 * \return sid <a href="http://ru.wikipedia.org/wiki/CAPTCHA">CAPTCHA</a>.
 */
const char *CaptchaNeedException::GetCaptchaSid() const
{
	return m_captchaSid;
}

/**
 * \brief Возвращает ссылку на <a href="http://ru.wikipedia.org/wiki/CAPTCHA">CAPTCHA</a>.
 * \return Ссылка на <a href="http://ru.wikipedia.org/wiki/CAPTCHA">CAPTCHA</a>.
 */
const char *CaptchaNeedException::GetCaptchaURL() const
{
	return m_captchaUrl;
}

void CaptchaNeedException::SetData(const char *captchaSid, const char *captchaUrl)
{
	delete[] m_captchaSid;
	delete[] m_captchaUrl;
	m_captchaSid = new char[strlen(captchaSid) + 1];
	m_captchaUrl = new char[strlen(captchaUrl) + 1];
	strncpy(m_captchaSid, captchaSid, strlen(captchaSid) + 1);
	strncpy(m_captchaUrl, captchaUrl, strlen(captchaUrl) + 1);
}
