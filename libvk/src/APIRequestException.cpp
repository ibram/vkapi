#include "APIRequestFailException.h"
using namespace sonapi;

APIRequestFailException::APIRequestFailException(int errorCode, const char *errorMessage):
	BaseException(errorCode, errorMessage)
{

}