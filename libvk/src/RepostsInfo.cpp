#include "RepostsInfo.h"
using namespace sonapi;

RepostsInfo::RepostsInfo()
{
	m_count = 0;
	m_userReposted = false;
}

size_t RepostsInfo::GetCount() const
{
	return m_count;
}

bool RepostsInfo::UserReposted() const
{
	return m_userReposted;
}

void RepostsInfo::SetCount(size_t count)
{
	m_count = count;
}

void RepostsInfo::SetUserReposted(bool userReposted)
{
	m_userReposted = userReposted;
}
