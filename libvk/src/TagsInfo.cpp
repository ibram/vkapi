#include "TagsInfo.h"
using namespace sonapi;

size_t TagsInfo::GetCount() const
{
	return m_count;
}

void TagsInfo::SetCount(size_t count)
{
	m_count = count;
}