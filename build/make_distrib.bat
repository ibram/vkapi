@echo off
title Distributive Maker

call setup.bat

echo Creating dir...
mkdir ..\distrib

echo Copying files...
copy /V /Y ..\lib\*.dll /B ..\distrib\ /B
copy /V /Y ..\bin\*.exe /B ..\distrib\ /B

copy /V /Y "%LIBCURL_PATH%\lib\release\libcurl.dll" /B ..\distrib\ /B
copy /V /Y "%OPENSSL_PATH%\libeay32.dll" /B ..\distrib\ /B
copy /V /Y "%OPENSSL_PATH%\ssleay32.dll" /B ..\distrib\ /B
copy /V /Y "%ZLIB_PATH%\lib\zlib.dll" /B ..\distrib\ /B